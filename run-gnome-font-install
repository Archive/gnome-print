# Hey Emacs, this is -*- perl -*-

# This will be run once on the installation machine.  Therefore we will not
# try to predict the perl path.
# -----------------------------------------------------------------------------

die "Usage: $0 installer datadir srcdir\n" unless @ARGV == 3;

my $installer = $ARGV[0];
my $datadir = $ARGV[1];
my $srcdir = $ARGV[2];

die "Error: installer not executable.\n" unless -f $installer && -x $installer;
die "Error: datadir is not a directory.\n" unless -d $datadir;
# die "Error: srcdir is not a directory.\n" unless -d $srcdir;

my @default_paths =
    ('/usr/share/ghostscript/fonts',
     '/usr/lib/ghostscript/fonts',
     '/usr/share/fonts/default/Type1',
     '/usr/share/fonts/default/ghostscript',
     '/usr/local/share/ghostscript/fonts',
     '/usr/local/lib/ghostscript/fonts',
     '/usr/local/share/fonts/default/Type1',
     '/usr/local/share/fonts/default/ghostscript',
     );

# -----------------------------------------------------------------------------

my $targetdir = "$datadir/fonts";
my $targetfile = "$targetdir/fontmap3";

# We might be running under a restricted path; try to compensate.
$ENV{'PATH'} .= ":/bin:/usr/bin:/usr/local/bin";

# Ask ghostscript for a list of font directories.
my @gs_fontpath = ();
{
    local (*FIL);
    if (open (*FIL, "gs -h 2>/dev/null |")) {
	while (<FIL>) {
	    if (/^Search path:$/i ... /^\S/) {
		next if /^\S/;

		s/^\s+//;   # Initial white-space
		s/\s+$//;   # Terminal white-space including \n.
		s/\s+:$//;  # " :" at end of line.

		# Absolute paths only.
		push @gs_fontpath, (grep { m|^/|; } (split (/\s:\s/)));
	    }
	}
	close (*FIL);

	if (@gs_fontpath) {
	    print "Info: ghostscript was found and has fonts in these directories:\n";
	    foreach (@gs_fontpath) {
		print "Info:   $_\n";
	    }
	} else {
	    print STDERR "Error: ghostscript was found, but appeared to have no font\n";
	    print STDERR "Error: information.  Please file a bug report (see bugs.gnome.org)\n";
	    print STDERR "Error: and include the verbatim output of \"gs -h\".  Thanks.\n";
	    print STDERR "Warning: I'll try to limp on...\n";
	}
    } else {
	print STDERR "Warning: could not locate gs (aka ghostscript).\n";
	print STDERR "Warning: I'll try to limp on...\n";
    }
}

# Add in out defaults if they exist.
{
    my %seen = ();
    my $dir;

    foreach $dir (@gs_fontpath) { $seen{$dir} = 1; }

    foreach $dir (@default_paths) {
	next if $seen{$dir};
	next unless -d $dir;

	print "Info: adding default directory $dir to font path.\n";
	push @gs_fontpath, $dir;
	$seen{$dir} = 1;
    }	    
}

my @options = (
#     '--system',
#     '--scan',
#     '--no-copy',
#     "--afm-path=$datadir/fonts/afms",
#     "--pfb-path=$datadir/fonts/pfbs",
     "--debug",
     "--recursive",
     "--target=$targetfile",
     );
{
    my $dir;
    foreach $dir (@gs_fontpath) {
#	push @options, "--pfb-assignment=ghostscript,$dir";
#	push @options, "--assignment=ghostscript,$dir";
	push @options, $dir;
    }
}
push @options, "--aliases=$srcdir/fonts/urw.aliases";

print "Info: running \"$installer ", join (' ', @options), "\".\n";
my $exitcode = system ($installer, @options);
die "Error: failed.\n" if $exitcode;

chmod 0644 => $targetfile;
if (!-r $targetfile) {
    die "Error: something went very wrong.\n";
}

#!/usr/bin/perl

# NOTE: deprecated - use gnome-font-install instead

# This is a somewhat hackish script to install symlinks to fonts
# into the ~/.gnome/fonts directory.

# I will want to replace this with a cleaner solution later.

# Usage: fontinst.pl tries to automatically install the GhostScript
# and X11 fonts.
# fontinst.pl <pathname> tries to install the font

# fontinst.pl only bothers with matched .pf[ab] and .afm sets.

sub install_from_file {
    my ($target, $fn) = @_;

    my $target_fn = $fn;
    $target_fn =~ s/^.*\///;
    $target_fn = $target.'/'.$target_fn;
    if (!-s $target_fn) {
	symlink $fn, $target_fn or die "Can\'t install $fn: $!\n";
	print "Installed $target_fn\n";
    }
}

sub ensure_target_dir {
    my $home = $ENV{'HOME'};
    my $d = "$home/.gnome";
    if (!-d $d) {
	mkdir $d, 0777 or die "Errror creating $d: $!\n";
    }
    $d .= '/fonts';
    if (!-d $d) {
	mkdir $d, 0777 or die "Errror creating $d: $!\n";
    }
    return $d;
}

sub get_font_files {
    my ($path) = @_;
    my (@files);

    if (-d $path) {
	opendir DIR, $path;
	@files = map { "$path/$_" } grep { /\.(afm|pf[ab])$/i } readdir DIR;
	closedir DIR;
    } else {
	if ($path =~ /\.[^\/]+$/) {
	    $path =~ s/\.[^\/]+$//;
	}
	my @suflist = ('.pfb', '.PFB', '.pfa', '.PFA');
	foreach my $suf (@suflist) {
	    my $fn = $path.$suf;
	    print "Checking $fn\n";
	    if (-e $fn) {
		push @files, $fn;
	    }
	}
    }
    return @files;
}

sub install_from_files {
    my ($target, @path) = @_;
    my %afm, %pfb;

    my (@afms) = grep { /\.afm$/i } @path;
    foreach my $fn (@path) {
	my $base = $fn;
	$base =~ s/^.*\///;
	my $base2 = $base;
	$base2 =~ s/\.[^\.\/]+//;
	if ($fn =~ /\.afm$/i) {
	    $afm{$base2} = $fn;
	} elsif ($fn =~ /.pf[ab]$/i) {
	    $pfb{$base2} = $fn;
	}
    }
    foreach my $base (keys %afm) {
	if ($pfb{$base}) {
	    install_from_file ($target, $afm{$base});
	    install_from_file ($target, $pfb{$base});
	} else {
	    die "Can\'t find .pfb file for $base\n";
	}
    }
}

sub main {
    my (@path) = @_;

    if (!@path) {
	@path = ('/usr/share/ghostscript/fonts',
		 '/usr/lib/texmf/texmf/fonts/afm/adobe/utopia',
		 '/usr/lib/texmf/texmf/fonts/type1/adobe/utopia',
		 '/usr/lib/texmf/texmf/fonts/afm/bitstrea/charter',
		 '/usr/lib/texmf/texmf/fonts/type1/bitstrea/charter');
    }
    my $d = ensure_target_dir ();
    @path = map { get_font_files $_ } @path;
    install_from_files ($d, @path);
}

main (@ARGV);

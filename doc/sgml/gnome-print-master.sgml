<refentry id="gnome-print-gnome-print-master" revision="24 Dec 2000">
<refmeta>
<refentrytitle>gnome-print-master</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GNOME-PRINT Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>gnome-print-master</refname><refpurpose></refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>



struct      <link linkend="GnomePrintMaster">GnomePrintMaster</link>;
struct      <link linkend="GnomePrintMasterClass">GnomePrintMasterClass</link>;
<link linkend="GnomePrintMaster">GnomePrintMaster</link>* <link linkend="gnome-print-master-new">gnome_print_master_new</link>    (void);
<link linkend="GnomePrintMaster">GnomePrintMaster</link>* <link linkend="gnome-print-master-new-from-dialog">gnome_print_master_new_from_dialog</link>
                                            (<link linkend="GnomePrintDialog">GnomePrintDialog</link> *dialog);
<link linkend="GnomePrintContext">GnomePrintContext</link>* <link linkend="gnome-print-master-get-context">gnome_print_master_get_context</link>
                                            (<link linkend="GnomePrintMaster">GnomePrintMaster</link> *gpm);
void        <link linkend="gnome-print-master-set-paper">gnome_print_master_set_paper</link>    (<link linkend="GnomePrintMaster">GnomePrintMaster</link> *gpm,
                                             const <link linkend="GnomePaper">GnomePaper</link> *paper);
const <link linkend="GnomePaper">GnomePaper</link>* <link linkend="gnome-print-master-get-paper">gnome_print_master_get_paper</link>
                                            (const <link linkend="GnomePrintMaster">GnomePrintMaster</link> *gpm);
void        <link linkend="gnome-print-master-set-printer">gnome_print_master_set_printer</link>  (<link linkend="GnomePrintMaster">GnomePrintMaster</link> *gpm,
                                             <link linkend="GnomePrinter">GnomePrinter</link> *printer);
void        <link linkend="gnome-print-master-set-copies">gnome_print_master_set_copies</link>   (<link linkend="GnomePrintMaster">GnomePrintMaster</link> *gpm,
                                             int copies,
                                             <link linkend="gboolean">gboolean</link> iscollate);
void        <link linkend="gnome-print-master-close">gnome_print_master_close</link>        (<link linkend="GnomePrintMaster">GnomePrintMaster</link> *gpm);
int         <link linkend="gnome-print-master-get-pages">gnome_print_master_get_pages</link>    (<link linkend="GnomePrintMaster">GnomePrintMaster</link> *gpm);
int         <link linkend="gnome-print-master-print">gnome_print_master_print</link>        (<link linkend="GnomePrintMaster">GnomePrintMaster</link> *gpm);

</synopsis>
</refsynopsisdiv>





<refsect1>
<title>Description</title>
<para>

</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GnomePrintMaster">struct GnomePrintMaster</title>
<programlisting>struct GnomePrintMaster {
	GtkObject object;

	void *priv;		/* private data */

	GnomePrintContext *context; /* metafile context */

	/* # copies info */
	gint copies;
	gboolean iscollate;

	/* paper */
	const GnomePaper *paper;

	/* printer, if required */
	GnomePrinter *printer;
};
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GnomePrintMasterClass">struct GnomePrintMasterClass</title>
<programlisting>struct GnomePrintMasterClass;</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="gnome-print-master-new">gnome_print_master_new ()</title>
<programlisting><link linkend="GnomePrintMaster">GnomePrintMaster</link>* gnome_print_master_new    (void);</programlisting>
<para>
Create a new GnomePrintMaster.  All values are initialised
to sensible defaults.</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> A new GnomePrintMaster.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-master-new-from-dialog">gnome_print_master_new_from_dialog ()</title>
<programlisting><link linkend="GnomePrintMaster">GnomePrintMaster</link>* gnome_print_master_new_from_dialog
                                            (<link linkend="GnomePrintDialog">GnomePrintDialog</link> *dialog);</programlisting>
<para>
Create a new GnomePrintMaster based on the values in the
<literal>GnomePrintDialog</literal>.  Range values are initialised to sensible
defaults.  Other values are initialised from the given dialog.</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>dialog</parameter>&nbsp;:</entry>
<entry> <literal>A</literal> GnomePrintDialog
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> A new GnomePrintMaster.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-master-get-context">gnome_print_master_get_context ()</title>
<programlisting><link linkend="GnomePrintContext">GnomePrintContext</link>* gnome_print_master_get_context
                                            (<link linkend="GnomePrintMaster">GnomePrintMaster</link> *gpm);</programlisting>
<para>
Retrieve the GnomePrintContext which applications
print to.</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>gpm</parameter>&nbsp;:</entry>
<entry> An initialised GnomePrintMaster.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> The printing context.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-master-set-paper">gnome_print_master_set_paper ()</title>
<programlisting>void        gnome_print_master_set_paper    (<link linkend="GnomePrintMaster">GnomePrintMaster</link> *gpm,
                                             const <link linkend="GnomePaper">GnomePaper</link> *paper);</programlisting>
<para>
Set the paper size for the print.</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>gpm</parameter>&nbsp;:</entry>
<entry> An initialised GnomePrintMaster.
</entry></row>
<row><entry align="right"><parameter>paper</parameter>&nbsp;:</entry>
<entry> Paper size required.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-master-get-paper">gnome_print_master_get_paper ()</title>
<programlisting>const <link linkend="GnomePaper">GnomePaper</link>* gnome_print_master_get_paper
                                            (const <link linkend="GnomePrintMaster">GnomePrintMaster</link> *gpm);</programlisting>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>gpm</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-master-set-printer">gnome_print_master_set_printer ()</title>
<programlisting>void        gnome_print_master_set_printer  (<link linkend="GnomePrintMaster">GnomePrintMaster</link> *gpm,
                                             <link linkend="GnomePrinter">GnomePrinter</link> *printer);</programlisting>
<para>
Sets physical printer used to print to.  If this is not set,
then a dialogue will be presented on each print.
</para>
<para>
This functions takes ownership of the printer reference.</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>gpm</parameter>&nbsp;:</entry>
<entry> An initialised GnomePrintMaster.
</entry></row>
<row><entry align="right"><parameter>printer</parameter>&nbsp;:</entry>
<entry> Printer device to use.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-master-set-copies">gnome_print_master_set_copies ()</title>
<programlisting>void        gnome_print_master_set_copies   (<link linkend="GnomePrintMaster">GnomePrintMaster</link> *gpm,
                                             int copies,
                                             <link linkend="gboolean">gboolean</link> iscollate);</programlisting>
<para>
Set the number of copies to print, and whether pages are collated.
If <parameter>iscollate</parameter> is set, then a multiple copy print to a physical
printer will order pages as "1,2,3,...1,2,3,..." otherwise they
will be ordered "1,1,...,2,2...".</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>gpm</parameter>&nbsp;:</entry>
<entry> A GnomePrintContext.
</entry></row>
<row><entry align="right"><parameter>copies</parameter>&nbsp;:</entry>
<entry> Number of copies of each page to print.
</entry></row>
<row><entry align="right"><parameter>iscollate</parameter>&nbsp;:</entry>
<entry> Whether page copies are collated.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-master-close">gnome_print_master_close ()</title>
<programlisting>void        gnome_print_master_close        (<link linkend="GnomePrintMaster">GnomePrintMaster</link> *gpm);</programlisting>
<para>
Closes the GnomePrintMaster <parameter>gpm</parameter>, ready for printing
or previewing.</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>gpm</parameter>&nbsp;:</entry>
<entry> A GnomePrintMaster which has had printing performed
on it.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-master-get-pages">gnome_print_master_get_pages ()</title>
<programlisting>int         gnome_print_master_get_pages    (<link linkend="GnomePrintMaster">GnomePrintMaster</link> *gpm);</programlisting>
<para>
Find the number of pages stored in a completed printout.</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>gpm</parameter>&nbsp;:</entry>
<entry> An initialised and closed GnomePrintMaster.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> If <parameter>gpm</parameter> has not been closed using
<link linkend="gnome-print-master-close">gnome_print_master_close</link>(), then 0, otherwise the number
of pages created by the application.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-master-print">gnome_print_master_print ()</title>
<programlisting>int         gnome_print_master_print        (<link linkend="GnomePrintMaster">GnomePrintMaster</link> *gpm);</programlisting>
<para>
Print the pages stored in the GnomePrintMaster to
the phyisical printing device.
</para>
<para>
If no printer has been set, then a dialogue is presented,
asking the user for the printer to print to.</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>gpm</parameter>&nbsp;:</entry>
<entry> A completed GnomePrintMaster.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> Returns -1 on error.
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>




</refentry>

<refentry id="gnome-print-gnome-print-encode-private" revision="24 Dec 2000">
<refmeta>
<refentrytitle>gnome-print-encode-private</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GNOME-PRINT Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>gnome-print-encode-private</refname><refpurpose></refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>



int         <link linkend="gnome-print-encode-blank">gnome_print_encode_blank</link>        (const <link linkend="guchar">guchar</link> *in,
                                             <link linkend="gint">gint</link> in_size);
int         <link linkend="gnome-print-encode-rlc">gnome_print_encode_rlc</link>          (const <link linkend="guchar">guchar</link> *in,
                                             <link linkend="guchar">guchar</link> *out,
                                             <link linkend="gint">gint</link> in_size);
int         <link linkend="gnome-print-encode-tiff">gnome_print_encode_tiff</link>         (const <link linkend="guchar">guchar</link> *in,
                                             <link linkend="guchar">guchar</link> *out,
                                             <link linkend="gint">gint</link> in_size);
int         <link linkend="gnome-print-encode-drow">gnome_print_encode_drow</link>         (const <link linkend="guchar">guchar</link> *in,
                                             <link linkend="guchar">guchar</link> *out,
                                             <link linkend="gint">gint</link> in_size,
                                             <link linkend="guchar">guchar</link> *seed);
int         <link linkend="gnome-print-encode-hex">gnome_print_encode_hex</link>          (const <link linkend="guchar">guchar</link> *in,
                                             <link linkend="guchar">guchar</link> *out,
                                             <link linkend="gint">gint</link> in_size);
int         <link linkend="gnome-print-decode-hex">gnome_print_decode_hex</link>          (const <link linkend="guchar">guchar</link> *in,
                                             <link linkend="guchar">guchar</link> *out,
                                             <link linkend="gint">gint</link> in_size);
int         <link linkend="gnome-print-encode-ascii85">gnome_print_encode_ascii85</link>      (const <link linkend="guchar">guchar</link> *in,
                                             <link linkend="guchar">guchar</link> *out,
                                             <link linkend="gint">gint</link> in_size);
int         <link linkend="gnome-print-decode-ascii85">gnome_print_decode_ascii85</link>      (const <link linkend="guchar">guchar</link> *in,
                                             <link linkend="guchar">guchar</link> *out,
                                             <link linkend="gint">gint</link> in_size);
int         <link linkend="gnome-print-encode-deflate">gnome_print_encode_deflate</link>      (const <link linkend="guchar">guchar</link> *in,
                                             <link linkend="guchar">guchar</link> *out,
                                             <link linkend="gint">gint</link> in_size,
                                             <link linkend="gint">gint</link> out_size);
int         <link linkend="gnome-print-encode-rlc-wcs">gnome_print_encode_rlc_wcs</link>      (<link linkend="gint">gint</link> size);
int         <link linkend="gnome-print-encode-tiff-wcs">gnome_print_encode_tiff_wcs</link>     (<link linkend="gint">gint</link> size);
int         <link linkend="gnome-print-encode-drow-wcs">gnome_print_encode_drow_wcs</link>     (<link linkend="gint">gint</link> size);
int         <link linkend="gnome-print-encode-hex-wcs">gnome_print_encode_hex_wcs</link>      (<link linkend="gint">gint</link> size);
int         <link linkend="gnome-print-decode-hex-wcs">gnome_print_decode_hex_wcs</link>      (<link linkend="gint">gint</link> size);
int         <link linkend="gnome-print-encode-ascii85-wcs">gnome_print_encode_ascii85_wcs</link>  (<link linkend="gint">gint</link> size);
int         <link linkend="gnome-print-decode-ascii85-wcs">gnome_print_decode_ascii85_wcs</link>  (<link linkend="gint">gint</link> size);
int         <link linkend="gnome-print-encode-deflate-wcs">gnome_print_encode_deflate_wcs</link>  (<link linkend="gint">gint</link> size);
int         <link linkend="gnome-print-encode-ascii85-">gnome_print_encode_ascii85_</link>     (const <link linkend="guchar">guchar</link> *in,
                                             <link linkend="guchar">guchar</link> *out,
                                             <link linkend="gint">gint</link> in_size);
void        <link linkend="gnome-print-encode-timer-start">gnome_print_encode_timer_start</link>  (void);
void        <link linkend="gnome-print-encode-timer-end">gnome_print_encode_timer_end</link>    (void);
</synopsis>
</refsynopsisdiv>





<refsect1>
<title>Description</title>
<para>

</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="gnome-print-encode-blank">gnome_print_encode_blank ()</title>
<programlisting>int         gnome_print_encode_blank        (const <link linkend="guchar">guchar</link> *in,
                                             <link linkend="gint">gint</link> in_size);</programlisting>
<para>
Scan the row and determine if it is a row with no data</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>in</parameter>&nbsp;:</entry>
<entry> the row to analiyze
</entry></row>
<row><entry align="right"><parameter>in_size</parameter>&nbsp;:</entry>
<entry> the size of the row
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> TRUE if it contains only 0's FALSE otherwise
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-encode-rlc">gnome_print_encode_rlc ()</title>
<programlisting>int         gnome_print_encode_rlc          (const <link linkend="guchar">guchar</link> *in,
                                             <link linkend="guchar">guchar</link> *out,
                                             <link linkend="gint">gint</link> in_size);</programlisting>
<para>
Implements RunLengthCoding encodeion</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>in</parameter>&nbsp;:</entry>
<entry> the buffer to encode
</entry></row>
<row><entry align="right"><parameter>out</parameter>&nbsp;:</entry>
<entry> the encodeed buffer
</entry></row>
<row><entry align="right"><parameter>in_size</parameter>&nbsp;:</entry>
<entry> size of the in buffer
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> size of the encodeed buffer
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-encode-tiff">gnome_print_encode_tiff ()</title>
<programlisting>int         gnome_print_encode_tiff         (const <link linkend="guchar">guchar</link> *in,
                                             <link linkend="guchar">guchar</link> *out,
                                             <link linkend="gint">gint</link> in_size);</programlisting>
<para>
Reference: [1] Page15-17.
(it should be useful for other Printer Languages too).</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>in</parameter>&nbsp;:</entry>
<entry> the array containing the buffer of bytes that will be converted
</entry></row>
<row><entry align="right"><parameter>out</parameter>&nbsp;:</entry>
<entry> the output array in TIFF "Packbits"
</entry></row>
<row><entry align="right"><parameter>in_size</parameter>&nbsp;:</entry>
<entry> the size of the input buffer
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>: the size of the TIFF encodeed string
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-encode-drow">gnome_print_encode_drow ()</title>
<programlisting>int         gnome_print_encode_drow         (const <link linkend="guchar">guchar</link> *in,
                                             <link linkend="guchar">guchar</link> *out,
                                             <link linkend="gint">gint</link> in_size,
                                             <link linkend="guchar">guchar</link> *seed);</programlisting>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>in</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>out</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>in_size</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>seed</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-encode-hex">gnome_print_encode_hex ()</title>
<programlisting>int         gnome_print_encode_hex          (const <link linkend="guchar">guchar</link> *in,
                                             <link linkend="guchar">guchar</link> *out,
                                             <link linkend="gint">gint</link> in_size);</programlisting>
<para>
Implements hex encoding.</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>in</parameter>&nbsp;:</entry>
<entry> a pointer to the buffer to encode
</entry></row>
<row><entry align="right"><parameter>out</parameter>&nbsp;:</entry>
<entry> a pointer to the prev. allocated out buffer
</entry></row>
<row><entry align="right"><parameter>in_size</parameter>&nbsp;:</entry>
<entry> the size of the input buffer
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> size of the encoded buffer
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-decode-hex">gnome_print_decode_hex ()</title>
<programlisting>int         gnome_print_decode_hex          (const <link linkend="guchar">guchar</link> *in,
                                             <link linkend="guchar">guchar</link> *out,
                                             <link linkend="gint">gint</link> in_size);</programlisting>
<para>
Implements hex decoding</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>in</parameter>&nbsp;:</entry>
<entry> a pointer to the buffer to encode
</entry></row>
<row><entry align="right"><parameter>out</parameter>&nbsp;:</entry>
<entry> a pointer to the prev. allocated out buffer
</entry></row>
<row><entry align="right"><parameter>in_size</parameter>&nbsp;:</entry>
<entry> the size of the input buffer
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> size of the decoded buffer
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-encode-ascii85">gnome_print_encode_ascii85 ()</title>
<programlisting>int         gnome_print_encode_ascii85      (const <link linkend="guchar">guchar</link> *in,
                                             <link linkend="guchar">guchar</link> *out,
                                             <link linkend="gint">gint</link> in_size);</programlisting>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>in</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>out</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>in_size</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-decode-ascii85">gnome_print_decode_ascii85 ()</title>
<programlisting>int         gnome_print_decode_ascii85      (const <link linkend="guchar">guchar</link> *in,
                                             <link linkend="guchar">guchar</link> *out,
                                             <link linkend="gint">gint</link> in_size);</programlisting>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>in</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>out</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>in_size</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-encode-deflate">gnome_print_encode_deflate ()</title>
<programlisting>int         gnome_print_encode_deflate      (const <link linkend="guchar">guchar</link> *in,
                                             <link linkend="guchar">guchar</link> *out,
                                             <link linkend="gint">gint</link> in_size,
                                             <link linkend="gint">gint</link> out_size);</programlisting>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>in</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>out</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>in_size</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>out_size</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-encode-rlc-wcs">gnome_print_encode_rlc_wcs ()</title>
<programlisting>int         gnome_print_encode_rlc_wcs      (<link linkend="gint">gint</link> size);</programlisting>
<para>
returns the bytes that the apps should alocate
for the "out" buffer of a rlc encodeion</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>size</parameter>&nbsp;:</entry>
<entry> the size of the "in" buffer
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the size of a rlc buffer in a WCS 
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-encode-tiff-wcs">gnome_print_encode_tiff_wcs ()</title>
<programlisting>int         gnome_print_encode_tiff_wcs     (<link linkend="gint">gint</link> size);</programlisting>
<para>
returns the bytes that the apps should alocate
for the "out" buffer of a tiff encodeion</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>size</parameter>&nbsp;:</entry>
<entry> the size of the "in" buffer
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the size of a tiff buffer in a WCS 
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-encode-drow-wcs">gnome_print_encode_drow_wcs ()</title>
<programlisting>int         gnome_print_encode_drow_wcs     (<link linkend="gint">gint</link> size);</programlisting>
<para>
returns the bytes that the apps should alocate
for the "out" buffer of a drow encodeion</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>size</parameter>&nbsp;:</entry>
<entry> the size of the "in" buffer
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the size of a drow buffer in a WCS 
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-encode-hex-wcs">gnome_print_encode_hex_wcs ()</title>
<programlisting>int         gnome_print_encode_hex_wcs      (<link linkend="gint">gint</link> size);</programlisting>
<para>
returns the bytes that the apps should alocate
for the "out" buffer of a hex encodede stream</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>size</parameter>&nbsp;:</entry>
<entry> the size of the "in" buffer
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the size of a ascii85 buffer in a WCS 
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-decode-hex-wcs">gnome_print_decode_hex_wcs ()</title>
<programlisting>int         gnome_print_decode_hex_wcs      (<link linkend="gint">gint</link> size);</programlisting>
<para>
returns the bytes that the apps should alocate
for the "out" buffer of a hex decoded stream</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>size</parameter>&nbsp;:</entry>
<entry> the size of the "in" buffer
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the size of a decoded buffer in a WCS 
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-encode-ascii85-wcs">gnome_print_encode_ascii85_wcs ()</title>
<programlisting>int         gnome_print_encode_ascii85_wcs  (<link linkend="gint">gint</link> size);</programlisting>
<para>
returns the bytes that the apps should alocate
for the "out" buffer of a ascii85 encoding</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>size</parameter>&nbsp;:</entry>
<entry> the size of the "in" buffer
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> the size of a ascii85 buffer in a WCS 
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-decode-ascii85-wcs">gnome_print_decode_ascii85_wcs ()</title>
<programlisting>int         gnome_print_decode_ascii85_wcs  (<link linkend="gint">gint</link> size);</programlisting>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>size</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-encode-deflate-wcs">gnome_print_encode_deflate_wcs ()</title>
<programlisting>int         gnome_print_encode_deflate_wcs  (<link linkend="gint">gint</link> size);</programlisting>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>size</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-encode-ascii85-">gnome_print_encode_ascii85_ ()</title>
<programlisting>int         gnome_print_encode_ascii85_     (const <link linkend="guchar">guchar</link> *in,
                                             <link linkend="guchar">guchar</link> *out,
                                             <link linkend="gint">gint</link> in_size);</programlisting>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>in</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>out</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>in_size</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-encode-timer-start">gnome_print_encode_timer_start ()</title>
<programlisting>void        gnome_print_encode_timer_start  (void);</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="gnome-print-encode-timer-end">gnome_print_encode_timer_end ()</title>
<programlisting>void        gnome_print_encode_timer_end    (void);</programlisting>
<para>

</para></refsect2>

</refsect1>




</refentry>

<refentry id="gnome-print-gnome-print-copies" revision="24 Dec 2000">
<refmeta>
<refentrytitle>gnome-print-copies</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GNOME-PRINT Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>gnome-print-copies</refname><refpurpose></refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>
<synopsis>



struct      <link linkend="GnomePrintCopies">GnomePrintCopies</link>;
struct      <link linkend="GnomePrintCopiesClass">GnomePrintCopiesClass</link>;
<link linkend="GtkWidget">GtkWidget</link>*  <link linkend="gnome-print-copies-new">gnome_print_copies_new</link>          (void);
void        <link linkend="gnome-print-copies-bind-editable-enters">gnome_print_copies_bind_editable_enters</link>
                                            (<link linkend="GnomePrintCopies">GnomePrintCopies</link> *gpc,
                                             <link linkend="GnomeDialog">GnomeDialog</link> *dialog);
void        <link linkend="gnome-print-copies-bind-accel-group">gnome_print_copies_bind_accel_group</link>
                                            (<link linkend="GnomePrintCopies">GnomePrintCopies</link> *gpc,
                                             <link linkend="GtkWindow">GtkWindow</link> *window);
void        <link linkend="gnome-print-copies-set-copies">gnome_print_copies_set_copies</link>   (<link linkend="GnomePrintCopies">GnomePrintCopies</link> *gpc,
                                             <link linkend="gint">gint</link> copies,
                                             <link linkend="gboolean">gboolean</link> collate);
<link linkend="gint">gint</link>        <link linkend="gnome-print-copies-get-copies">gnome_print_copies_get_copies</link>   (<link linkend="GnomePrintCopies">GnomePrintCopies</link> *gpc);
<link linkend="gboolean">gboolean</link>    <link linkend="gnome-print-copies-get-collate">gnome_print_copies_get_collate</link>  (<link linkend="GnomePrintCopies">GnomePrintCopies</link> *gpc);

</synopsis>
</refsynopsisdiv>





<refsect1>
<title>Description</title>
<para>

</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="GnomePrintCopies">struct GnomePrintCopies</title>
<programlisting>struct GnomePrintCopies;</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="GnomePrintCopiesClass">struct GnomePrintCopiesClass</title>
<programlisting>struct GnomePrintCopiesClass;</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="gnome-print-copies-new">gnome_print_copies_new ()</title>
<programlisting><link linkend="GtkWidget">GtkWidget</link>*  gnome_print_copies_new          (void);</programlisting>
<para>
Create a new GnomePrintCopies widget.</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> A new GnomePrintCopies widget.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-copies-bind-editable-enters">gnome_print_copies_bind_editable_enters ()</title>
<programlisting>void        gnome_print_copies_bind_editable_enters
                                            (<link linkend="GnomePrintCopies">GnomePrintCopies</link> *gpc,
                                             <link linkend="GnomeDialog">GnomeDialog</link> *dialog);</programlisting>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>gpc</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>dialog</parameter>&nbsp;:</entry>
<entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-copies-bind-accel-group">gnome_print_copies_bind_accel_group ()</title>
<programlisting>void        gnome_print_copies_bind_accel_group
                                            (<link linkend="GnomePrintCopies">GnomePrintCopies</link> *gpc,
                                             <link linkend="GtkWindow">GtkWindow</link> *window);</programlisting>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>gpc</parameter>&nbsp;:</entry>
<entry>
</entry></row>
<row><entry align="right"><parameter>window</parameter>&nbsp;:</entry>
<entry>


</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-copies-set-copies">gnome_print_copies_set_copies ()</title>
<programlisting>void        gnome_print_copies_set_copies   (<link linkend="GnomePrintCopies">GnomePrintCopies</link> *gpc,
                                             <link linkend="gint">gint</link> copies,
                                             <link linkend="gboolean">gboolean</link> collate);</programlisting>
<para>
Set the number of copies and collation sequence to be displayed.</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>gpc</parameter>&nbsp;:</entry>
<entry> An initialised GnomePrintCopies widget.
</entry></row>
<row><entry align="right"><parameter>copies</parameter>&nbsp;:</entry>
<entry> New number of copies.
</entry></row>
<row><entry align="right"><parameter>collate</parameter>&nbsp;:</entry>
<entry> New collation status.
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-copies-get-copies">gnome_print_copies_get_copies ()</title>
<programlisting><link linkend="gint">gint</link>        gnome_print_copies_get_copies   (<link linkend="GnomePrintCopies">GnomePrintCopies</link> *gpc);</programlisting>
<para>
Retrieve the number of copies set</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>gpc</parameter>&nbsp;:</entry>
<entry> An initialised GnomePrintCopies widget.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> Number of copies set
</entry></row>
</tbody></tgroup></informaltable></refsect2>
<refsect2>
<title><anchor id="gnome-print-copies-get-collate">gnome_print_copies_get_collate ()</title>
<programlisting><link linkend="gboolean">gboolean</link>    gnome_print_copies_get_collate  (<link linkend="GnomePrintCopies">GnomePrintCopies</link> *gpc);</programlisting>
<para>
Retrieve the collation status</para>
<para>

</para><informaltable pgwide=1 frame="none" role="params">
<tgroup cols="2">
<colspec colwidth="2*">
<colspec colwidth="8*">
<tbody>
<row><entry align="right"><parameter>gpc</parameter>&nbsp;:</entry>
<entry> An initialised GnomePrintCopies widget.
</entry></row>
<row><entry align="right"><emphasis>Returns</emphasis> :</entry><entry> Collation status
</entry></row>
</tbody></tgroup></informaltable></refsect2>

</refsect1>




</refentry>

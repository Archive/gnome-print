#ifndef __GPA_OPTIONS_H__
#define __GPA_OPTIONS_H__

/*
 * Copyright (C) 2000-2001 Ximian, Inc. and Jose M Celorio
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *  Chema Celorio <chema@celorio.com>
 *  Lauris Kaplinski <lauris@ximian.com>
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include "gpa-node.h"

typedef enum {
	GPA_OPTIONS_TYPE_PICKONE,
	GPA_OPTIONS_TYPE_PICKMANY,
	GPA_OPTIONS_TYPE_BOOLEAN,
	GPA_OPTIONS_TYPE_NUMERIC,
	GPA_OPTIONS_TYPE_DOUBLE,
	GPA_OPTIONS_TYPE_ERROR,
} GpaOptionsType;

typedef enum {
	GPA_GROUP_PAPER,
	GPA_GROUP_QUALITY,
	GPA_GROUP_INSTALLED,
	GPA_GROUP_SUBGROUP,
	GPA_GROUP_COMPRESSION,
	GPA_GROUP_PS,
	GPA_GROUP_ERROR,
} GpaOptionsGroup;

typedef enum {
	GPA_CONTENT_GENERIC,
	GPA_CONTENT_PAPER_SIZE,
	GPA_CONTENT_PAPER_MEDIA,
	GPA_CONTENT_PAPER_SOURCE,
	GPA_CONTENT_PAPER_ORIENTATION,
	GPA_CONTENT_RESOLUTION,
	GPA_CONTENT_RESOLUTION_MODE,
	GPA_CONTENT_ERROR
} GpaContent;

typedef enum {
	GPA_PICKONE_COMBO,
	GPA_PICKONE_RADIO,
	GPA_PICKONE_ERROR,
} GpaPickoneType;

#define GPA_TYPE_OPTIONS (gpa_options_get_type ())
#define GPA_OPTIONS(obj) (GTK_CHECK_CAST ((obj), GPA_TYPE_OPTIONS, GpaOptions))
#define GPA_OPTIONS_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GPA_TYPE_OPTIONS, GPAOptionsClass))
#define GPA_IS_OPTIONS(obj) (GTK_CHECK_TYPE ((obj), GPA_TYPE_OPTIONS))
#define GPA_IS_OPTIONS_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GPA_TYPE_OPTIONS))

typedef struct _GPAOptions GpaOptions;
typedef struct _GPAOptionsClass GPAOptionsClass;

GtkType gpa_options_get_type (void);

END_GNOME_DECLS

#endif /* __GPA_OPTIONS_H__ */




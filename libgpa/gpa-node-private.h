#ifndef _GPA_NODE_PRIVATE_H_
#define _GPA_NODE_PRIVATE_H_

/*
 * GPANode
 *
 * Opaque handle to gnome-print configuration tree
 *
 * Authors:
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include <gtk/gtkobject.h>
#include <libgnomeprint/gpa-node.h>

#define GPA_MODIFIED_FLAG (1 << 4)

#define GPA_TYPE_NODE (gpa_node_get_type ())
#define GPA_NODE_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GPA_TYPE_NODE, GPANodeClass))
#define GPA_IS_NODE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GPA_TYPE_NODE))

typedef struct _GPANodeClass GPANodeClass;

struct _GPANode {
	GtkObject object;
	GPANode *parent;
	GPANode *next;
	guchar *id;
};

struct _GPANodeClass {
	GtkObjectClass object_class;

	GPANode * (* duplicate) (GPANode *node);
	gboolean (* verify) (GPANode *node);

	guchar * (* get_value) (GPANode *node);
	gboolean (* set_value) (GPANode *node, const guchar *value);

	GPANode * (* get_child) (GPANode *node, GPANode *ref);

	GPANode * (* lookup) (GPANode *node, const guchar *path);

	/* Signal that node or its children have been modified */
	void (* modified) (GPANode *node);
};

#define GPA_NODE_ID(n) (GPA_NODE (n)->id)

GtkType gpa_node_get_type (void);

GPANode *gpa_node_new (GtkType type, const guchar *id);
GPANode *gpa_node_construct (GPANode *node, const guchar *id);

GPANode *gpa_node_duplicate (GPANode *node);
gboolean gpa_node_verify (GPANode *node);

GPANode *gpa_node_lookup (GPANode *node, const guchar *path);

/* Request that "modified" signal will be run from idle loop */
void gpa_node_request_modified (GPANode *node);
void gpa_node_emit_modified (GPANode *node);

END_GNOME_DECLS

#endif


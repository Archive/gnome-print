/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#ifndef __GNOME_PRINT_ADMIN_STRUCTS_H__
#define __GNOME_PRINT_ADMIN_STRUCTS_H__

typedef struct _GpaPpdInfo      GpaPpdInfo;
typedef struct _GpaBackend      GpaBackend;

#endif /* __GNOME_PRINT_ADMIN_STRUCTS_H__ */

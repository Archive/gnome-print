#define __GPA_REFERENCE_C__

/*                                                            
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Chema Celorio <chema@celorio.com>
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2000-2001 Ximian, Inc. and Jose M. Celorio
 *
 */

#include <string.h>
#include <gtk/gtksignal.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-util.h>

#include "gpa-reference.h"

/* GPAReference */

enum {SET_VALUE, LAST_SIGNAL};

static void gpa_reference_class_init (GPAReferenceClass *klass);
static void gpa_reference_init (GPAReference *reference);

static void gpa_reference_destroy (GtkObject *object);

static GPANode *gpa_reference_duplicate (GPANode *node);
static gboolean gpa_reference_verify (GPANode *node);
static guchar *gpa_reference_get_value (GPANode *node);
static gboolean gpa_reference_set_value (GPANode *node, const guchar *value);
static GPANode *gpa_reference_get_child (GPANode *node, GPANode *ref);
static GPANode *gpa_reference_lookup (GPANode *node, const guchar *path);
static void gpa_reference_modified (GPANode *node);

static void gpa_reference_reference_modified (GPANode *node, gpointer data);

static GPANodeClass *reference_parent_class = NULL;
static guint reference_signals[LAST_SIGNAL] = {0};

GtkType
gpa_reference_get_type (void) {
	static GtkType reference_type = 0;
	if (!reference_type) {
		GtkTypeInfo reference_info = {
			"GPAReference",
			sizeof (GPAReference),
			sizeof (GPAReferenceClass),
			(GtkClassInitFunc)  gpa_reference_class_init,
			(GtkObjectInitFunc) gpa_reference_init,
			NULL, NULL, NULL
		};
		reference_type = gtk_type_unique (GPA_TYPE_NODE, &reference_info);
	}
	return reference_type;
}

static void
gpa_reference_class_init (GPAReferenceClass *klass)
{
	GtkObjectClass *object_class;
	GPANodeClass *node_class;

	object_class = (GtkObjectClass *) klass;
	node_class = (GPANodeClass *) klass;

	reference_parent_class = gtk_type_class (GPA_TYPE_NODE);

	reference_signals[SET_VALUE] = gtk_signal_new ("set_value",
						       GTK_RUN_LAST,
						       ((GtkObjectClass *) reference_parent_class)->type,
						       GTK_SIGNAL_OFFSET (GPAReferenceClass, set_value),
						       gtk_marshal_BOOL__POINTER,
						       GTK_TYPE_BOOL, 1,
						       GTK_TYPE_POINTER);
	gtk_object_class_add_signals ((GtkObjectClass *) reference_parent_class, reference_signals, LAST_SIGNAL);

	object_class->destroy = gpa_reference_destroy;

	node_class->duplicate = gpa_reference_duplicate;
	node_class->verify = gpa_reference_verify;
	node_class->get_value = gpa_reference_get_value;
	node_class->set_value = gpa_reference_set_value;
	node_class->get_child = gpa_reference_get_child;
	node_class->lookup = gpa_reference_lookup;
	node_class->modified = gpa_reference_modified;
}

static void
gpa_reference_init (GPAReference *reference)
{
	reference->ref = NULL;
}

static void
gpa_reference_destroy (GtkObject *object)
{
	GPAReference *reference;

	reference = GPA_REFERENCE (object);

	if (reference->ref) {
		gtk_signal_disconnect_by_data (GTK_OBJECT (reference->ref), reference);
		gpa_node_unref (reference->ref);
		reference->ref = NULL;
	}

	if (((GtkObjectClass *) reference_parent_class)->destroy)
		(* ((GtkObjectClass *) reference_parent_class)->destroy) (object);
}

static GPANode *
gpa_reference_duplicate (GPANode *node)
{
	GPAReference *reference, *new;

	reference = GPA_REFERENCE (node);

	new = gtk_type_new (GPA_TYPE_REFERENCE);

	new->ref = reference->ref;
	if (new->ref) {
		gpa_node_ref (new->ref);
		gtk_signal_connect (GTK_OBJECT (new->ref), "modified",
				    GTK_SIGNAL_FUNC (gpa_reference_reference_modified), new);
	}

	return GPA_NODE (new);
}

static gboolean
gpa_reference_verify (GPANode *node)
{
	GPAReference *reference;

	reference = GPA_REFERENCE (node);

	if (!reference->ref) return FALSE;
	if (!gpa_node_verify (reference->ref)) return FALSE;

	return TRUE;
}

static guchar *
gpa_reference_get_value (GPANode *node)
{
	GPAReference *reference;

	reference = GPA_REFERENCE (node);

	if (reference->ref) {
		return gpa_node_get_value (reference->ref);
	}

	return NULL;
}

static gboolean
gpa_reference_set_value (GPANode *node, const guchar *value)
{
	GPAReference *reference;
	gboolean ret;

	reference = GPA_REFERENCE (node);

	ret = FALSE;
	gtk_signal_emit (GTK_OBJECT (node), reference_signals[SET_VALUE], value, &ret);

	return ret;
}

static GPANode *
gpa_reference_get_child (GPANode *node, GPANode *ref)
{
	GPAReference *reference;

	reference = GPA_REFERENCE (node);

	if (reference->ref) {
		return gpa_node_get_child (reference->ref, ref);
	}

	return NULL;
}

static GPANode *
gpa_reference_lookup (GPANode *node, const guchar *path)
{
	GPAReference *reference;

	reference = GPA_REFERENCE (node);

	if (reference->ref) {
		return gpa_node_lookup (reference->ref, path);
	}

	return NULL;
}

static void
gpa_reference_modified (GPANode *node)
{
	GPAReference *reference;

	reference = GPA_REFERENCE (node);

	if (reference->ref && (GTK_OBJECT_FLAGS (reference->ref) & GPA_MODIFIED_FLAG)) {
		gpa_node_emit_modified (reference->ref);
	}
}

static void
gpa_reference_reference_modified (GPANode *node, gpointer data)
{
	gpa_node_request_modified (GPA_NODE (data));
}

GPANode *
gpa_reference_new (GPANode *ref)
{
	GPAReference *reference;

	g_return_val_if_fail (ref != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (ref), NULL);

	reference = gtk_type_new (GPA_TYPE_REFERENCE);

	reference->ref = ref;
	gpa_node_ref (reference->ref);
	gtk_signal_connect (GTK_OBJECT (reference->ref), "modified",
			    GTK_SIGNAL_FUNC (gpa_reference_reference_modified), reference);


	return GPA_NODE (reference);
}

GPANode *
gpa_reference_new_empty (void)
{
	GPAReference *reference;

	reference = gtk_type_new (GPA_TYPE_REFERENCE);

	return GPA_NODE (reference);
}

gboolean
gpa_reference_set_reference (GPAReference *reference, GPANode *ref)
{
	g_return_val_if_fail (reference != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_REFERENCE (reference), FALSE);
	g_return_val_if_fail (!ref || GPA_IS_NODE (ref), FALSE);

	if (reference->ref) {
		gtk_signal_disconnect_by_data (GTK_OBJECT (reference->ref), reference);
		gpa_node_unref (reference->ref);
		reference->ref = NULL;
	}

	if (ref) {
		reference->ref = ref;
		gpa_node_ref (reference->ref);
		gtk_signal_connect (GTK_OBJECT (reference->ref), "modified",
				    GTK_SIGNAL_FUNC (gpa_reference_reference_modified), reference);
	}

	gpa_node_request_modified (GPA_NODE (reference));

	return TRUE;
}



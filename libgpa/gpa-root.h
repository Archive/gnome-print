#ifndef _GPA_ROOT_H_
#define _GPA_ROOT_H_

/*
 * GPARoot
 *
 * Opaque root object to gnome-print configuration tree
 *
 * Authors:
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#define GPA_TYPE_ROOT (gpa_root_get_type ())
#define GPA_ROOT(obj) (GTK_CHECK_CAST ((obj), GPA_TYPE_ROOT, GPARoot))
#define GPA_ROOT_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GPA_TYPE_ROOT, GPARootClass))
#define GPA_IS_ROOT(obj) (GTK_CHECK_TYPE ((obj), GPA_TYPE_ROOT))
#define GPA_IS_ROOT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GPA_TYPE_ROOT))

#include "gpa-list.h"
#include "gpa-node-private.h"

typedef struct _GPARoot GPARoot;
typedef struct _GPARootClass GPARootClass;

struct _GPARoot {
	GPANode node;
	GPAList *vendors;
	GPAList *printers;
};

struct _GPARootClass {
	GPANodeClass node_class;
};

GtkType gpa_root_get_type (void);

GPARoot *gpa_root_get (void);
GPARoot *gpa_root (void);

#define GPA (gpa_root ())

END_GNOME_DECLS

#endif


/* These are copied from gnome-print. To satisfy linking requriements for libgpa */
#include <glib.h>

#define GPC_MAX_CHARS_PER_LINE 80 /* Needs to be a multiple of 2 ! */

/* FIXME: Implement as a macro ? maybe */
static inline gint libgpa_hex_2_dec (guchar upper, guchar lower)
{
  if (upper > '9')
    upper -= 39;
  if (lower > '9')
    lower -= 39;
               
  return ((upper - '0')*16) + lower - '0';
}

int
libgpa_decode_hex_wcs(int size){
  return ((size/2)+((size/2)/GPC_MAX_CHARS_PER_LINE)+4);
}

int
libgpa_decode_hex (const guchar *in, guchar *out, gint in_size)
{
  gint p1; /* Points to where we are writing into the out buffer */
  gint p2; /* Points to where we are reading from the in buffer */

  p1 = 0;
  p2 = 0;
  for (p2=0; p2 < in_size; p2+=2)
    {
      if ((in [p2] == ' ') ||
	  (in [p2] == '\t') ||
	  (in [p2] == '\n'))
	continue;

      out [p1++] = hex_2_dec (in[p2], in [p2+1]);
    }

  return p1;
}


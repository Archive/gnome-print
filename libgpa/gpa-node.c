#define _GPA_NODE_C_

/*
 * GPANode
 *
 * Opaque handle to gnome-print configuration tree
 *
 * Authors:
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 */

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <locale.h>
#include <gtk/gtkmain.h>
#include <gtk/gtksignal.h>
#include "gpa-config.h"
#include "gpa-node-private.h"

/* GPANode */

enum {MODIFIED, LAST_SIGNAL};

static void gpa_node_class_init (GPANodeClass *klass);
static void gpa_node_init (GPANode *node);

static void gpa_node_destroy (GtkObject *object);

static gint gpa_node_modified_idle_hook (GPANode *node);

static GtkObjectClass *node_parent_class;
static guint node_signals[LAST_SIGNAL] = {0};

GtkType
gpa_node_get_type (void)
{
	static GtkType node_type = 0;
	if (!node_type) {
		GtkTypeInfo node_info = {
			"GPANode",
			sizeof (GPANode),
			sizeof (GPANodeClass),
			(GtkClassInitFunc) gpa_node_class_init,
			(GtkObjectInitFunc) gpa_node_init,
			NULL, NULL, NULL
		};
		node_type = gtk_type_unique (gtk_object_get_type (), &node_info);
	}
	return node_type;
}

static void
gpa_node_class_init (GPANodeClass *klass)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass*) klass;

	node_parent_class = gtk_type_class (gtk_object_get_type ());

	node_signals[MODIFIED] = gtk_signal_new ("modified",
						 GTK_RUN_FIRST,
						 ((GtkObjectClass *) node_parent_class)->type,
						 GTK_SIGNAL_OFFSET (GPANodeClass, modified),
						 gtk_marshal_NONE__NONE,
						 GTK_TYPE_NONE, 0);
	gtk_object_class_add_signals ((GtkObjectClass *) node_parent_class, node_signals, LAST_SIGNAL);

	object_class->destroy = gpa_node_destroy;
}

static void
gpa_node_init (GPANode *node)
{
	node->parent = NULL;
	node->next = NULL;
	node->id = NULL;
}

static void
gpa_node_destroy (GtkObject *object)
{
	GPANode *node;
	guint id;

	node = (GPANode *) object;

	g_assert (node->parent == NULL);
	g_assert (node->next == NULL);

	id = GPOINTER_TO_INT (gtk_object_get_data (object, "idle_id"));
	if (id != 0) {
		gtk_idle_remove (id);
		gtk_object_remove_data (GTK_OBJECT (object), "idle_id");
	}

	if (node->id) {
		g_free (node->id);
		node->id = NULL;
	}

	if (((GtkObjectClass *) node_parent_class)->destroy)
		(* ((GtkObjectClass *) node_parent_class)->destroy) (object);
}

/* Methods */

GPANode *
gpa_node_new (GtkType type, const guchar *id)
{
	GPANode *node;

	g_return_val_if_fail (gtk_type_is_a (type, GPA_TYPE_NODE), NULL);

	node = gtk_type_new (type);

	if (id) node->id = g_strdup (id);

	return node;
}

GPANode *
gpa_node_construct (GPANode *node, const guchar *id)
{
	g_return_val_if_fail (node != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (node), NULL);
	g_return_val_if_fail (node->id == NULL, NULL);

	if (id) node->id = g_strdup (id);

	return node;
}

GPANode *
gpa_node_duplicate (GPANode *node)
{
	g_return_val_if_fail (node != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (node), NULL);

	if (((GPANodeClass *) (((GtkObject *) node)->klass))->duplicate)
		return (* ((GPANodeClass *) (((GtkObject *) node)->klass))->duplicate) (node);

	return NULL;
}

gboolean
gpa_node_verify (GPANode *node)
{
	gboolean ret;

	g_return_val_if_fail (node != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_NODE (node), FALSE);

	ret = TRUE;

	if (((GPANodeClass *) (((GtkObject *) node)->klass))->verify)
		ret = (* ((GPANodeClass *) (((GtkObject *) node)->klass))->verify) (node);

	return TRUE;
}

guchar *
gpa_node_get_value (GPANode *node)
{
	g_return_val_if_fail (node != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (node), NULL);

	if (((GPANodeClass *) (((GtkObject *) node)->klass))->get_value)
		return (* ((GPANodeClass *) (((GtkObject *) node)->klass))->get_value) (node);

	return NULL;
}

gboolean
gpa_node_set_value (GPANode *node, const guchar *value)
{
	gboolean ret;

	g_return_val_if_fail (node != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_NODE (node), FALSE);

	ret = FALSE;

	if (((GPANodeClass *) (((GtkObject *) node)->klass))->set_value)
		ret = (* ((GPANodeClass *) (((GtkObject *) node)->klass))->set_value) (node, value);

	if (ret) {
		gpa_node_request_modified (node);
	}

	return FALSE;
}

/* NB! ref->parent == node is not invariant due to references */

GPANode *
gpa_node_get_child (GPANode *node, GPANode *ref)
{
	g_return_val_if_fail (node != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (node), NULL);
	g_return_val_if_fail ((ref == NULL) || GPA_IS_NODE (ref), NULL);

	if (((GPANodeClass *) (((GtkObject *) node)->klass))->get_child)
		return (* ((GPANodeClass *) (((GtkObject *) node)->klass))->get_child) (node, ref);

	return NULL;
}

GPANode *
gpa_node_lookup (GPANode *node, const guchar *path)
{
	g_return_val_if_fail (node != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (node), NULL);
	g_return_val_if_fail (path != NULL, NULL);
	g_return_val_if_fail (!*path || isalnum (*path), NULL);

	if (!*path) {
		gpa_node_ref (node);
		return node;
	}

	if (((GPANodeClass *) (((GtkObject *) node)->klass))->lookup)
		return (* ((GPANodeClass *) (((GtkObject *) node)->klass))->lookup) (node, path);

	return NULL;
}

/* Signal stuff */

void
gpa_node_request_modified (GPANode *node)
{
	g_return_if_fail (node != NULL);
	g_return_if_fail (GPA_IS_NODE (node));

	if (!(GTK_OBJECT_FLAGS (node) & GPA_MODIFIED_FLAG)) {
		GTK_OBJECT_SET_FLAGS (node, GPA_MODIFIED_FLAG);
		if (node->parent) {
			gpa_node_request_modified (node->parent);
		} else {
			guint id;
			id = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (node), "idle_id"));
			if (id == 0) {
				id = gtk_idle_add ((GtkFunction) gpa_node_modified_idle_hook, node);
				gtk_object_set_data (GTK_OBJECT (node), "idle_id", GUINT_TO_POINTER (id));
			}
		}
	}
}

void
gpa_node_emit_modified (GPANode *node)
{
	gpa_node_ref (node);

	GTK_OBJECT_UNSET_FLAGS (node, GPA_MODIFIED_FLAG);

	gtk_signal_emit (GTK_OBJECT (node), node_signals[MODIFIED]);

	gpa_node_unref (node);
}

static gint
gpa_node_modified_idle_hook (GPANode *node)
{
	gtk_object_set_data (GTK_OBJECT (node), "idle_id", GUINT_TO_POINTER (0));

	gpa_node_emit_modified (node);

	return FALSE;
}

/* Public methods */

void
gpa_node_ref (GPANode *node)
{
	g_return_if_fail (node != NULL);
	g_return_if_fail (GPA_IS_NODE (node));

	gtk_object_ref (GTK_OBJECT (node));
}

void
gpa_node_unref (GPANode *node)
{
	g_return_if_fail (node != NULL);
	g_return_if_fail (GPA_IS_NODE (node));

	gtk_object_ref (GTK_OBJECT (node));
}

/* Gtk+ replacement to simplify migration */
GPANode *
gpa_node_check_cast (gpointer node)
{
	return (GTK_CHECK_CAST ((node), GPA_TYPE_NODE, GPANode));
}

gboolean
gpa_node_check_type (gpointer node)
{
	return (GTK_CHECK_TYPE ((node), GPA_TYPE_NODE));
}

guchar *
gpa_node_id (GPANode *node)
{
	g_return_val_if_fail (node != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (node), NULL);

	if (node->id) return g_strdup (node->id);

	return NULL;
}

/* These return referenced node or NULL */

GPANode *
gpa_node_get_parent (GPANode *node)
{
	g_return_val_if_fail (node != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (node), NULL);

	if (node->parent) gpa_node_ref (node->parent);

	return node->parent;
}

GPANode *
gpa_node_get_path_node (GPANode *node, const guchar *path)
{
	g_return_val_if_fail (node != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (node), NULL);
	g_return_val_if_fail (path != NULL, NULL);
	g_return_val_if_fail (!*path || isalnum (*path), NULL);

	return gpa_node_lookup (node, path);
}

/* Basic value manipulation */

guchar *
gpa_node_get_path_value (GPANode *node, const guchar *path)
{
	GPANode *ref;

	g_return_val_if_fail (node != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (node), NULL);
	g_return_val_if_fail (path != NULL, NULL);
	g_return_val_if_fail (!*path || isalnum (*path), NULL);

	ref = gpa_node_lookup (node, path);

	if (ref) {
		guchar *value;
		value = gpa_node_get_value (ref);
		gpa_node_unref (ref);
		return value;
	}

	return NULL;
}

gboolean
gpa_node_set_path_value (GPANode *node, const guchar *path, const guchar *value)
{
	GPANode *ref;

	g_return_val_if_fail (node != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_NODE (node), FALSE);
	g_return_val_if_fail (path != NULL, FALSE);
	g_return_val_if_fail (!*path || isalnum (*path), FALSE);

	ref = gpa_node_lookup (node, path);

	if (ref) {
		gboolean ret;
		ret = gpa_node_set_value (ref, value);
		gpa_node_unref (ref);
		return ret;
	}

	return FALSE;
}

/* Convenience stuff */
gboolean
gpa_node_get_bool_path_value (GPANode *node, const guchar *path, gint *value)
{
	guchar *v;
	
	g_return_val_if_fail (node != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_NODE (node), FALSE);
	g_return_val_if_fail (path != NULL, FALSE);
	g_return_val_if_fail (!*path || isalnum (*path), FALSE);
	g_return_val_if_fail (value != NULL, FALSE);

	v = gpa_node_get_path_value (node, path);

	if (v != NULL) {
		if (!strcasecmp (v, "true") || !strcasecmp (v, "yes") || !strcasecmp (v, "y") || (atoi (v) > 0)) {
			*value = TRUE;
			return TRUE;
		}
		*value = FALSE;
		g_free (v);
		return TRUE;
	}
	
	return FALSE;
}

gboolean
gpa_node_get_int_path_value (GPANode *node, const guchar *path, gint *value)
{
	guchar *v;
	
	g_return_val_if_fail (node != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_NODE (node), FALSE);
	g_return_val_if_fail (path != NULL, FALSE);
	g_return_val_if_fail (!*path || isalnum (*path), FALSE);
	g_return_val_if_fail (value != NULL, FALSE);

	v = gpa_node_get_path_value (node, path);

	if (v != NULL) {
		*value = atoi (v);
		g_free (v);
		return TRUE;
	}
	
	return FALSE;
}

gboolean
gpa_node_get_double_path_value (GPANode *node, const guchar *path, gdouble *value)
{
	guchar *v;
	
	g_return_val_if_fail (node != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_NODE (node), FALSE);
	g_return_val_if_fail (path != NULL, FALSE);
	g_return_val_if_fail (!*path || isalnum (*path), FALSE);
	g_return_val_if_fail (value != NULL, FALSE);

	v = gpa_node_get_path_value (node, path);

	if (v != NULL) {
		gchar *loc;
		loc = setlocale (LC_NUMERIC, NULL);
		setlocale (LC_NUMERIC, "C");
		*value = atof (v);
		g_free (v);
		setlocale (LC_NUMERIC, loc);
		return TRUE;
	}
	
	return FALSE;
}

gboolean
gpa_node_set_bool_path_value (GPANode *node, const guchar *path, gint value)
{
	g_return_val_if_fail (node != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_NODE (node), FALSE);
	g_return_val_if_fail (path != NULL, FALSE);
	g_return_val_if_fail (!*path || isalnum (*path), FALSE);

	return gpa_node_set_path_value (node, path, (value) ? "true" : "false");
}

gboolean
gpa_node_set_int_path_value (GPANode *node, const guchar *path, gint value)
{
	guchar c[64];
	
	g_return_val_if_fail (node != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_NODE (node), FALSE);
	g_return_val_if_fail (path != NULL, FALSE);
	g_return_val_if_fail (!*path || isalnum (*path), FALSE);

	g_snprintf (c, 64, "%d", value);

	return gpa_node_set_path_value (node, path, c);
}

gboolean
gpa_node_set_double_path_value (GPANode *node, const guchar *path, gdouble value)
{
	guchar c[64];
	gchar *loc;
	
	g_return_val_if_fail (node != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_NODE (node), FALSE);
	g_return_val_if_fail (path != NULL, FALSE);
	g_return_val_if_fail (!*path || isalnum (*path), FALSE);

	loc = setlocale (LC_NUMERIC, NULL);
	setlocale (LC_NUMERIC, "C");
	g_snprintf (c, 64, "%g", value);
	setlocale (LC_NUMERIC, loc);

	return gpa_node_set_path_value (node, path, c);
}

GPANode *
gpa_defaults (void)
{
	GPANode *config;

	config = gpa_config_new ();

	return config;
}




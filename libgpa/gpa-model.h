#ifndef __GPA_MODEL_H__
#define __GPA_MODEL_H__

/*                                                            
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Chema Celorio <chema@celorio.com>
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2000-2001 Ximian, Inc. and Jose M. Celorio
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include <tree.h>

#include "gpa-list.h"

#define GPA_TYPE_MODEL (gpa_model_get_type ())
#define GPA_MODEL(obj) (GTK_CHECK_CAST ((obj), GPA_TYPE_MODEL, GPAModel))
#define GPA_MODEL_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GPA_TYPE_MODEL, GPAModelClass))
#define GPA_IS_MODEL(obj) (GTK_CHECK_TYPE ((obj), GPA_TYPE_MODEL))
#define GPA_IS_MODEL_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GPA_TYPE_MODEL))

typedef struct _GPAModel GPAModel;
typedef struct _GPAModelClass GPAModelClass;

/* GPAModel */

struct _GPAModel {
	GPANode node;
	
	guint loaded : 1;

	GPANode *name;
	GPANode *vendor;
	GPAList *options;
};

struct _GPAModelClass {
	GPANodeClass node_class;
};

GtkType gpa_model_get_type (void);

GPANode *gpa_model_new_from_info_tree (xmlNodePtr tree);
GPANode *gpa_model_new_from_tree (xmlNodePtr tree);

GPAList *gpa_model_list_new_from_info_tree (xmlNodePtr tree);

GPANode *gpa_model_get_by_id (const guchar *id);

gboolean gpa_model_load (GPAModel *model);

#define GPA_MODEL_ENSURE_LOADED(m) ((m) && GPA_IS_MODEL(m) && (GPA_MODEL(m)->loaded || gpa_model_load (GPA_MODEL(m))))

#if 0
/* GPAModelList */

#define GPA_MODEL_ID(m) (GPA_ITEM (m)->id)
#define GPA_MODEL_VENDOR(m) (GPA_NODE (m)->parent ? GPA_NODE (m)->parent->parent ? GPA_VENDOR (GPA_NODE (m)->parent->parent) : NULL : NULL)

/* Constructors */

GPAModel *gpa_model_new (const gchar *id, const gchar *name);
GPAModel *gpa_model_new_from_info_tree (xmlNodePtr tree, XmlParseContext *context);

/* Basic "GpaModel" object operations */

gboolean gpa_model_verify_with_settings (const GPAModel *model, GpaSettings *settings);
gboolean gpa_model_save (GPAModel *model);

/* Access to the struct */
GpaOptions * gpa_model_get_options_from_id (const GPAModel *model, const gchar *id);

/* Utility functions */
gboolean gpa_model_add_missing_default_settings_and_values (GPAModel *model);

GpaBackend *gpa_model_get_default_backend (GPAModel *model);
#endif

END_GNOME_DECLS

#endif /* __GPA_MODEL_H__ */

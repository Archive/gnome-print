#define __GPA_LIST_C__

/*                                                            
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 */

#include <string.h>
#include <gtk/gtksignal.h>
#include "gpa-utils.h"
#include "gpa-reference.h"
#include "gpa-list.h"

/* GPAList */

static void gpa_list_class_init (GPAListClass *klass);
static void gpa_list_init (GPAList *list);

static void gpa_list_destroy (GtkObject *object);

static GPANode *gpa_list_get_child (GPANode *node, GPANode *ref);
static GPANode *gpa_list_lookup (GPANode *node, const guchar *path);
static void gpa_list_modified (GPANode *node);

static gboolean gpa_list_def_set_value (GPAReference *reference, const guchar *value, gpointer data);

static GPANodeClass *list_parent_class = NULL;

GtkType
gpa_list_get_type (void) {
	static GtkType list_type = 0;
	if (!list_type) {
		GtkTypeInfo list_info = {
			"GPAList",
			sizeof (GPAList),
			sizeof (GPAListClass),
			(GtkClassInitFunc)  gpa_list_class_init,
			(GtkObjectInitFunc) gpa_list_init,
			NULL, NULL, NULL
		};
		list_type = gtk_type_unique (GPA_TYPE_NODE, &list_info);
	}
	return list_type;
}

static void
gpa_list_class_init (GPAListClass *klass)
{
	GtkObjectClass *object_class;
	GPANodeClass *node_class;

	object_class = (GtkObjectClass *) klass;
	node_class = (GPANodeClass *) klass;

	list_parent_class = gtk_type_class (GPA_TYPE_NODE);

	object_class->destroy = gpa_list_destroy;

	node_class->get_child = gpa_list_get_child;
	node_class->lookup = gpa_list_lookup;

	node_class->modified = gpa_list_modified;
}

static void
gpa_list_init (GPAList *list)
{
	list->childtype = GPA_TYPE_NODE;
	list->children = NULL;
	list->has_def = FALSE;
	list->def = NULL;
}

static void
gpa_list_destroy (GtkObject *object)
{
	GPAList *list;

	list = GPA_LIST (object);

	if (list->def) {
		list->def = gpa_node_detach_unref (GPA_NODE (list), GPA_NODE (list->def));
		list->def = NULL;
	}

	while (list->children) {
		GPANode *child;
		child = list->children;
		list->children = child->next;
		child->next = NULL;
		child->parent = NULL;
		gpa_node_unref (child);
	}

	if (((GtkObjectClass *) list_parent_class)->destroy)
		(* ((GtkObjectClass *) list_parent_class)->destroy) (object);
}

static GPANode *
gpa_list_get_child (GPANode *node, GPANode *ref)
{
	GPAList *gpl;

	gpl = GPA_LIST (node);

	if (!ref) {
		if (gpl->children) gpa_node_ref (gpl->children);
		return gpl->children;
	}

	if (ref->next) gpa_node_ref (ref->next);

	return ref->next;
}

static GPANode *
gpa_list_lookup (GPANode *node, const guchar *path)
{
	GPAList *list;

	list = GPA_LIST (node);

	if (list->has_def) {
		GPANode *child;
		child = NULL;
		if (gpa_node_lookup_ref (&child, GPA_NODE (list->def), path, "Default")) return child;
	}

	return NULL;
}

static void
gpa_list_modified (GPANode *node)
{
	GPAList *gpl;
	GPANode *child;

	gpl = GPA_LIST (node);

	child = gpl->children;
	while (child) {
		GPANode *next;
		next = child->next;
		if (GTK_OBJECT_FLAGS (child) & GPA_MODIFIED_FLAG) {
			gpa_node_ref (child);
			gpa_node_emit_modified (child);
			gpa_node_unref (child);
		}
		child = next;
	}

	if (gpl->has_def && gpl->def) {
		if (GTK_OBJECT_FLAGS (gpl->def) & GPA_MODIFIED_FLAG) {
			gpa_node_ref (GPA_NODE (gpl->def));
			gpa_node_emit_modified (GPA_NODE (gpl->def));
			gpa_node_unref (GPA_NODE (gpl->def));
		}
	}
}

GPAList *
gpa_list_construct (GPAList *gpl, GtkType childtype, gboolean has_def)
{
	g_return_val_if_fail (gpl != NULL, NULL);
	g_return_val_if_fail (GPA_IS_LIST (gpl), NULL);
	g_return_val_if_fail (gtk_type_is_a (childtype, GPA_TYPE_NODE), NULL);

	gpl->childtype = childtype;
	gpl->has_def = has_def;
	if (has_def) {
		gpl->def = gpa_reference_new_empty ();
		gtk_signal_connect (GTK_OBJECT (gpl->def), "set_value",
				    GTK_SIGNAL_FUNC (gpa_list_def_set_value), gpl);
	}

	return gpl;
}

gboolean
gpa_list_set_default (GPAList *list, GPANode *def)
{
	gboolean ret;

	g_return_val_if_fail (list != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_LIST (list), FALSE);
	g_return_val_if_fail (list->has_def, FALSE);
	g_return_val_if_fail (list->def != NULL, FALSE);
	g_return_val_if_fail (!def || GPA_IS_NODE (def), FALSE);
	g_return_val_if_fail (!def || GPA_NODE_ID (def), FALSE);
	g_return_val_if_fail (!def || def->parent == (GPANode *) list, FALSE);

	ret = gpa_node_set_value (GPA_NODE (list->def), GPA_NODE_ID (def));

	return ret;
}

static gboolean
gpa_list_def_set_value (GPAReference *reference, const guchar *value, gpointer data)
{
	GPAList *gpl;
	GPANode *child;

	gpl = GPA_LIST (data);

	for (child = gpl->children; child != NULL; child = child->next) {
		if (!strcmp (value, GPA_NODE_ID (child))) {
			gboolean ret;
			ret = gpa_reference_set_reference (GPA_REFERENCE (gpl->def), child);
			return TRUE;
		}
	}

	return FALSE;
}

GPANode *
gpa_list_new (GtkType childtype, gboolean has_def)
{
	GPAList *gpl;

	g_return_val_if_fail (gtk_type_is_a (childtype, GPA_TYPE_NODE), NULL);

	gpl = gtk_type_new (GPA_TYPE_LIST);

	gpa_list_construct (gpl, childtype, has_def);

	return GPA_NODE (gpl);
}

gboolean
gpa_list_add_child (GPAList *list, GPANode *child, GPANode *ref)
{
	g_return_val_if_fail (list != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_LIST (list), FALSE);
	g_return_val_if_fail (child != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_NODE (child), FALSE);
	g_return_val_if_fail (child->parent == NULL, FALSE);
	g_return_val_if_fail (child->next == NULL, FALSE);
	g_return_val_if_fail (!ref || GPA_IS_NODE (ref), FALSE);
	g_return_val_if_fail (!ref || ref->parent == GPA_NODE (list), FALSE);

	if (!ref) {
		child->next = list->children;
		list->children = child;
	} else {
		child->next = ref->next;
		ref->next = child;
	}

	child->parent = GPA_NODE (list);
	gpa_node_ref (child);

	gpa_node_request_modified (GPA_NODE (list));

	return TRUE;
}

#if 0
gboolean
gpa_list_test_item_by_id (GPAList *list, const gchar *id)
{
	GPAItem *item;

	g_return_val_if_fail (list != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_LIST (list), FALSE);
	g_return_val_if_fail (id != NULL, FALSE);
	g_return_val_if_fail (*id != '\0', FALSE);

	for (item = list->items; item != NULL; item = item->next) {
		if (item->id && !strcmp (item->id, id)) return TRUE;
	}

	return FALSE;
}

GPANode *
gpa_list_get_item_by_id (GPAList *list, const gchar *id)
{
	GPAItem *item;

	g_return_val_if_fail (list != NULL, NULL);
	g_return_val_if_fail (GPA_IS_LIST (list), NULL);
	g_return_val_if_fail (id != NULL, NULL);
	g_return_val_if_fail (*id != '\0', NULL);

	for (item = list->items; item != NULL; item = item->next) {
		if (item->id && !strcmp (item->id, id)) {
			gpa_node_ref (GPA_NODE (item));
			return GPA_NODE (item);
		}
	}

	return NULL;
}
#endif


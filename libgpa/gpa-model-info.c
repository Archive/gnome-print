/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2000 Jose M Celorio
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *  Chema Celorio <chema@celorio.com>
 *
 */

#include "gpa-defs.h"

#include <string.h>
#include <glib.h>

#include <gtk/gtkwidget.h>
#include <libgnome/gnome-util.h>

#include <dirent.h> /* For the DIR structure stuff */

#include "gpa-private.h"
#include "gpa-model-private.h"
#include "gpa-model-info.h"
#include "gpa-model-info-private.h"

#if 0
GpaModelInfo *
gpa_model_info_new (const gchar *name, const gchar *id, const GPAVendor *vendor)
{
	GpaModelInfo *model_info;

	debug (FALSE, "");

	model_info = g_new (GpaModelInfo, 1);

	model_info->name   = g_strdup (name);
	model_info->id     = g_strdup (id);
	model_info->vendor = vendor;

	/* FIXME: Should we ref vendor ? */
	
	return model_info;
}
#endif

#if 0
/* Access to the struct */
const gchar *
gpa_model_info_get_name (GpaModelInfo *mi)
{
	g_return_val_if_fail (GPA_IS_MODEL_INFO (mi), NULL);

	return mi->name;
}

gchar *
gpa_model_info_dup_name (GpaModelInfo *mi)
{
	g_return_val_if_fail (GPA_IS_MODEL_INFO (mi), NULL);

	return g_strdup (mi->name);
}

const gchar *
gpa_model_info_get_id (GpaModelInfo *mi)
{
	g_return_val_if_fail (GPA_IS_MODEL_INFO (mi), NULL);

	return mi->id;
}


const GPAVendor *
gpa_model_info_get_vendor (GpaModelInfo *mi)
{
	g_return_val_if_fail (GPA_IS_MODEL_INFO (mi), NULL);

	return mi->vendor;
}
#endif

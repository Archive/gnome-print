#define __GPA_UTILS_C__

/*                                                            
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Chema Celorio <chema@celorio.com>
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2000-2001 Ximian, Inc. and Jose M. Celorio
 *
 */

#include <string.h>
#include <unistd.h>
#include <time.h>
#include "gpa-node-private.h"
#include "gpa-utils.h"

guchar *
gpa_id_new (const guchar *key)
{
	static gint counter = 0;
	guchar *id;

	/* fixme: This is not very intelligent (Lauris) */
	id = g_strdup_printf ("%s-%.12d%.6d%.6d", key, (gint) time (NULL), getpid (), counter++);

	return id;
}

GPANode *
gpa_node_attach (GPANode *parent, GPANode *child)
{
	g_return_val_if_fail (parent != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (parent), NULL);
	g_return_val_if_fail (child != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (child), NULL);
	g_return_val_if_fail (child->parent == NULL, NULL);
	g_return_val_if_fail (child->next == NULL, NULL);

	child->parent = parent;

	return child;
}

GPANode *
gpa_node_attach_ref (GPANode *parent, GPANode *child)
{
	g_return_val_if_fail (parent != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (parent), NULL);
	g_return_val_if_fail (child != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (child), NULL);
	g_return_val_if_fail (child->parent == NULL, NULL);
	g_return_val_if_fail (child->next == NULL, NULL);

	gpa_node_ref (child);

	child->parent = parent;

	return child;
}

GPANode *
gpa_node_detach (GPANode *parent, GPANode *child)
{
	g_return_val_if_fail (parent != NULL, child);
	g_return_val_if_fail (GPA_IS_NODE (parent), child);
	g_return_val_if_fail (child != NULL, child);
	g_return_val_if_fail (GPA_IS_NODE (child), child);
	g_return_val_if_fail (child->parent == parent, child);

	child->parent = NULL;
	child->next = NULL;

	return NULL;
}

GPANode *
gpa_node_detach_unref (GPANode *parent, GPANode *child)
{
	g_return_val_if_fail (parent != NULL, child);
	g_return_val_if_fail (GPA_IS_NODE (parent), child);
	g_return_val_if_fail (child != NULL, child);
	g_return_val_if_fail (GPA_IS_NODE (child), child);
	g_return_val_if_fail (child->parent == parent, child);

	child->parent = NULL;
	child->next = NULL;

	gpa_node_unref (child);

	return NULL;
}

GPANode *
gpa_node_detach_next (GPANode *parent, GPANode *child)
{
	GPANode *next;

	g_return_val_if_fail (parent != NULL, child);
	g_return_val_if_fail (GPA_IS_NODE (parent), child);
	g_return_val_if_fail (child != NULL, child);
	g_return_val_if_fail (GPA_IS_NODE (child), child);
	g_return_val_if_fail (child->parent == parent, child);

	next = child->next;
	child->parent = NULL;
	child->next = NULL;

	return next;
}

GPANode *
gpa_node_detach_unref_next (GPANode *parent, GPANode *child)
{
	GPANode *next;

	g_return_val_if_fail (parent != NULL, child);
	g_return_val_if_fail (GPA_IS_NODE (parent), child);
	g_return_val_if_fail (child != NULL, child);
	g_return_val_if_fail (GPA_IS_NODE (child), child);
	g_return_val_if_fail (child->parent == parent, child);

	next = child->next;
	child->parent = NULL;
	child->next = NULL;

	gpa_node_unref (child);

	return next;
}

gboolean
gpa_node_lookup_ref (GPANode **child, GPANode *node, const guchar *path, const guchar *key)
{
	gint len;

	g_return_val_if_fail (*child == NULL, TRUE);
	g_return_val_if_fail (node != NULL, TRUE);
	g_return_val_if_fail (GPA_IS_NODE (node), TRUE);
	g_return_val_if_fail (path != NULL, TRUE);
	g_return_val_if_fail (*path != '\0', TRUE);
	g_return_val_if_fail (key != NULL, TRUE);
	g_return_val_if_fail (*key != '\0', TRUE);

	len = strlen (key);

	if (!strncmp (path, key, len)) {
		if (!path[len]) {
			gpa_node_ref (node);
			*child = node;
			return TRUE;
		} else {
			if (path[len] != '.') return FALSE;
			*child = gpa_node_lookup (node, path + len + 1);
			return TRUE;
		}
	}

	return FALSE;
}

#if 0
static void
gpa_hash_item_copy (gpointer key_in, gpointer value_in, gpointer data)
{
	GHashTable *new_values;
	gchar *key;
	gchar *value;
	gchar *new_key;
	gchar *new_value;

	key   = (gchar *) key_in;
	value = (gchar *) value_in;
	new_values = (GHashTable *) data;

	new_key   = g_strdup (key);
	new_value = g_strdup (value);

	g_hash_table_insert (new_values, new_key, new_value);

}	

GHashTable*
gpa_hash_copy (GHashTable *values)
{
	GHashTable *new_values;

	new_values = g_hash_table_new (g_str_hash, g_str_equal);

	g_hash_table_foreach (values, gpa_hash_item_copy, new_values);

	return new_values;
}

static gboolean
gpa_hash_item_free (gpointer key_in, gpointer value_in, gpointer data)
{
	gchar *key;
	gchar *value;

	key   = (gchar *) key_in;
	value = (gchar *) value_in;

	g_return_val_if_fail (key != NULL, FALSE);
	g_return_val_if_fail (value != NULL, FALSE);

	g_free (key);
	g_free (value);
	
	key   = NULL;
	value = NULL;

	return TRUE;
}	

gboolean 
gpa_hash_free (GHashTable *values)
{
	if (values == NULL)
		return TRUE;

	g_hash_table_foreach_remove (values, gpa_hash_item_free, NULL);

	g_hash_table_destroy (values);

	return TRUE;
}


static void
gpa_hash_item_write (gpointer key_in, gpointer value_in, gpointer data)
{
	xmlNodePtr item;
	xmlNodePtr node;
	gchar *key;
	gchar *value;

	key   = (gchar *) key_in;
	value = (gchar *) value_in;
	node  = (xmlNodePtr) data;

	item = xmlNewChild (node, NULL, key, value);

	/*
	gpa_xml_set_value (item, GPA_TAG_NAME, key);
	gpa_xml_set_value (item, GPA_TAG_CONTENT, value);
	*/

}	

xmlNodePtr
gpa_hash_write (XmlParseContext *context, GHashTable *values, const gchar *name)
{
	xmlNodePtr node;

	g_return_val_if_fail (context != NULL, NULL);
	g_return_val_if_fail (values  != NULL, NULL);
	g_return_val_if_fail (name    != NULL, NULL);
		
	node = xmlNewDocNode (context->doc, context->ns, name, NULL);

	g_hash_table_foreach (values, gpa_hash_item_write, node);
	
	return node;
}

gboolean
gpa_hash_item_set (GHashTable *hash, const gchar *key, const gchar *content)
{
	g_return_val_if_fail (key     != NULL, FALSE);
	g_return_val_if_fail (content != NULL, FALSE);

	if (hash == NULL) {
		g_warning ("Hash value not found. Trying to set :\"%s\" Key:\"%s\"",
			   content, key);
		return FALSE;
	}

	/* FIXME: We need to free the string inside the hash */
	g_hash_table_remove (hash, key);
	g_hash_table_insert (hash, g_strdup (key), g_strdup (content));
	
	return TRUE;
}


const gchar *
gpa_hash_item_get (GHashTable *hash, const gchar *key)
{
	gchar *content;
	
	g_return_val_if_fail (key != NULL,    FALSE);

	if (hash == NULL) {
		g_warning ("Hash value not found. Key:\"%s\"",
			   key);
		return FALSE;
	}

	content = g_hash_table_lookup (hash, key);

	return content;
}

typedef struct {
	gboolean success;
	gint size;
	const GpaKnownNodes *nodes;
	const gchar *section;
} known_nodes_structure;

static void
gpa_hash_verify_item (gpointer key_in, gpointer value_in, gpointer data)
{
	known_nodes_structure *nodes_struct;
	gboolean success;
	gchar *key;
	gchar *value;
	gint size;
	gint n;

	debug (FALSE, "");

	key   = (gchar *) key_in;
	value = (gchar *) value_in;
	nodes_struct = (known_nodes_structure *) data;
	
	size    = nodes_struct->size;
	success = nodes_struct->success;

	for (n = 0; n < size; n++) {
		if (nodes_struct->nodes[n].name == NULL)
			break;
		if (strcmp (key, nodes_struct->nodes[n].name) == 0)
			break;
	}

	if (n == size) {
		gpa_error ("Invalid Value \"%s\", inside \"%s\"", key, nodes_struct->section);
		nodes_struct->success = FALSE;
	}
}

gboolean
gpa_hash_verify (GHashTable *hash,
		 const GpaKnownNodes *nodes,
		 gboolean xtra_flag,
		 const gchar *section)
{
	known_nodes_structure *nodes_struct;
	gboolean success = TRUE;
	gint size = 0;
	gint n;

	debug (FALSE, "");

	g_return_val_if_fail (hash != NULL, FALSE);

	/* First, verify that the values that are in the hash are Valid */
	while (nodes[size++].name != NULL);
	size--;
	
	nodes_struct = g_new (known_nodes_structure, 1);
	nodes_struct->success = TRUE;
	nodes_struct->nodes = nodes;
	nodes_struct->size  = size;
	nodes_struct->section = section;
	
	g_hash_table_foreach (hash, gpa_hash_verify_item, nodes_struct);
	
	success = nodes_struct->success;
	
	g_free (nodes_struct);

	/* Second, verify that the required values are in the hash */
	for (n = 0; n < size; n++) {
		if (!nodes[n].required)
			continue;
		if (nodes[n].xtra_flag || !xtra_flag) {
			if (g_hash_table_lookup (hash, nodes[n].name) ==NULL) {
				gpa_error ("Could not find the required value \"%s\" in the "
					   "\"%s\" hash\n",
					   nodes[n].name, section);
				return FALSE;
			}
		}
	}


	return success;
}


gchar *
gpa_utils_convert_date_to_string (time_t clock)
{
	struct tm *now;
	gchar *date;

	now = localtime (&clock);
		
	date = g_strdup_printf ("%04d%02d%02d%02d%02d%02d",
				now->tm_year + 1900,
				now->tm_mon + 1,
				now->tm_mday,
				now->tm_hour,
				now->tm_min,
				now->tm_sec);

	return date;
}
#endif


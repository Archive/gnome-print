#ifndef __GPA_REFERENCE_H__
#define __GPA_REFERENCE_H__

/*                                                            
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Chema Celorio <chema@celorio.com>
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2000-2001 Ximian, Inc. and Jose M. Celorio
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include "gpa-node-private.h"

#define GPA_TYPE_REFERENCE (gpa_reference_get_type ())
#define GPA_REFERENCE(obj) (GTK_CHECK_CAST ((obj), GPA_TYPE_REFERENCE, GPAReference))
#define GPA_REFERENCE_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GPA_TYPE_REFERENCE, GPAReferenceClass))
#define GPA_IS_REFERENCE(obj) (GTK_CHECK_TYPE ((obj), GPA_TYPE_REFERENCE))
#define GPA_IS_REFERENCE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GPA_TYPE_REFERENCE))

typedef struct _GPAReference GPAReference;
typedef struct _GPAReferenceClass GPAReferenceClass;

/* GPAReference */

struct _GPAReference {
	GPANode node;
	GPANode *ref;
};

struct _GPAReferenceClass {
	GPANodeClass node_class;
	/* fixme: Either find better name or move signalling to GPANodeClass */
	gboolean (* set_value) (GPAReference *reference, const guchar *value);
};

#define GPA_REFERENCE_REFERENCE(r) ((r) ? GPA_REFERENCE (r)->ref : NULL)

GtkType gpa_reference_get_type (void);

GPANode *gpa_reference_new (GPANode *ref);
GPANode *gpa_reference_new_empty (void);

gboolean gpa_reference_set_reference (GPAReference *reference, GPANode *ref);

END_GNOME_DECLS

#endif /* __GPA_REFERENCE_H__ */


#ifndef __GPA_LIST_H__
#define __GPA_LIST_H__

/*                                                            
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include "gpa-node-private.h"

/* GPAList */

#define GPA_TYPE_LIST (gpa_list_get_type ())
#define GPA_LIST(obj) (GTK_CHECK_CAST ((obj), GPA_TYPE_LIST, GPAList))
#define GPA_LIST_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GPA_TYPE_LIST, GpaListClass))
#define GPA_IS_LIST(obj) (GTK_CHECK_TYPE ((obj), GPA_TYPE_LIST))
#define GPA_IS_LIST_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GPA_TYPE_LIST))

typedef struct _GPAList GPAList;
typedef struct _GPAListClass GPAListClass;

struct _GPAList {
	GPANode node;
	GtkType childtype;
	GPANode *children;
	guint has_def : 1;
	GPANode *def;
};

struct _GPAListClass {
	GPANodeClass node_class;
};

GtkType gpa_list_get_type (void);

GPAList *gpa_list_construct (GPAList *list, GtkType childtype, gboolean has_default);

gboolean gpa_list_set_default (GPAList *list, GPANode *def);

/* Deprecated ... probably */
GPANode *gpa_list_new (GtkType childtype, gboolean has_def);
/* Deprecated ... probably */
gboolean gpa_list_add_child (GPAList *list, GPANode *child, GPANode *ref);

#if 0
/* fixme: I am not sure about these */
gboolean gpa_list_test_item_by_id (GPAList *list, const gchar *id);
GPANode *gpa_list_get_item_by_id (GPAList *list, const gchar *id);
#endif

END_GNOME_DECLS

#endif

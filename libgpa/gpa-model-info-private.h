#ifndef __GNOME_PRINT_ADMIN_MODEL_INFO_PRIVATE_H__
#define __GNOME_PRINT_ADMIN_MODEL_INFO_PRIVATE_H__

BEGIN_GNOME_DECLS

#if 0
#include "xml-utils.h"
#include "gpa-vendor.h"
#include "gpa-model.h"
#include "gpa-model-info.h"

GPAList *gpa_load_model_info_list_from_tree (XmlParseContext *context, xmlNodePtr tree, GPAVendor *vendor);
#endif

END_GNOME_DECLS

#endif /* __GNOME_PRINT_ADMIN_MODEL_INFO_PRIVATE_H__ */


#ifndef _GPA_CONFIG_H_
#define _GPA_CONFIG_H_

/*                                                            
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#define GPA_TYPE_CONFIG (gpa_config_get_type ())
#define GPA_CONFIG(obj) (GTK_CHECK_CAST ((obj), GPA_TYPE_CONFIG, GPAConfig))
#define GPA_CONFIG_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GPA_TYPE_CONFIG, GPAConfigClass))
#define GPA_IS_CONFIG(obj) (GTK_CHECK_TYPE ((obj), GPA_TYPE_CONFIG))
#define GPA_IS_CONFIG_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GPA_TYPE_CONFIG))

#include "gpa-list.h"

typedef struct _GPAConfig GPAConfig;
typedef struct _GPAConfigClass GPAConfigClass;

struct _GPAConfig {
	GPANode node;
	GPANode *globals;
	GPANode *printer;
	GPANode *settings;
};

struct _GPAConfigClass {
	GPANodeClass node_class;
};

GtkType gpa_config_get_type (void);

GPANode *gpa_config_new (void);

END_GNOME_DECLS

#endif

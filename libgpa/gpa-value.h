#ifndef __GPA_VALUE_H__
#define __GPA_VALUE_H__

/*                                                            
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include "tree.h"
#include "gpa-node-private.h"

/* GPAValue */

#define GPA_TYPE_VALUE (gpa_value_get_type ())
#define GPA_VALUE(obj) (GTK_CHECK_CAST ((obj), GPA_TYPE_VALUE, GPAValue))
#define GPA_VALUE_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GPA_TYPE_VALUE, GPAValueClass))
#define GPA_IS_VALUE(obj) (GTK_CHECK_TYPE ((obj), GPA_TYPE_VALUE))
#define GPA_IS_VALUE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GPA_TYPE_VALUE))

typedef struct _GPAValue GPAValue;
typedef struct _GPAValueClass GPAValueClass;

struct _GPAValue {
	GPANode node;
	guchar *value;
};

struct _GPAValueClass {
	GPANodeClass node_class;
};

#define GPA_VALUE_VALUE(v) ((v) ? GPA_VALUE (v)->value : NULL)

GtkType gpa_value_get_type (void);

GPANode *gpa_value_new (const guchar *id, const guchar *content);

GPANode *gpa_value_new_from_tree (const guchar *id, xmlNodePtr tree);

gboolean gpa_value_set_value_forced (GPAValue *value, const guchar *val);

END_GNOME_DECLS

#endif

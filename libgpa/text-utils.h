#ifndef __GPA_TEXT_UTILS_H__
#define __GPA_TEXT_UTILS_H__

/*
 * Copyright (C) 2000-2001 Jose M Celorio and Ximian Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *  Chema Celorio <chema@celorio.com>
 *  Lauris Kaplinski <lauris@ximian.com>
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include <glib.h>

gboolean gpa_path_is (const guchar *path, const guchar *prefix, gint length);

gint tu_get_pos_of_last_delimiter (const gchar *buffer, gchar delimiter);

gint     gpa_tu_replace_all (const gchar *search_text,
				 const gchar *replace_text,
				 gchar **buffer, gboolean case_sensitive);

GList * gpa_tu_get_list_of_sufixes (const gchar *search_for,
					 const gchar *buffer,
					 gchar separator);
void gpa_tu_list_of_sufixes_free (GList *list);


gboolean gpa_tu_divide (const gchar *buffer,
			    const gchar* start_tag,
			    const gchar *end_tag,
			    gchar **pre, gchar **body, gchar **post);

gint gpa_tu_search (const gchar *buffer,
			const gchar *search_for,
			gboolean case_sensitive);

gint gpa_tu_search_real (const gchar *buffer, gint buffer_length,
					    const gchar *search_text, gint search_text_length,
					    gboolean case_sensitive);

guchar* gpa_tu_read_file (const gchar* file_name, gint *size_,
						gint initial_size, gint grow_size_);

void gpa_tu_xml_clean (gchar **code,
			   gint extra_clean);

gboolean gpa_tu_remove_trailing_spaces (gchar **buffer_);


gint gpa_tu_strnchr (const guchar *buffer, guchar c);


/* Tokens */
gboolean gpa_tu_token_next (const guchar *buffer, gint buffer_size, gint *offset);

gboolean gpa_tu_token_next_till_newline (const guchar *buffer,
							  gint buffer_size,
							  gint *offset);
gchar *  gpa_tu_token_next_dup_till_newline (const guchar *buffer,
								  gint buffer_size,
								  gint *offset);
gchar *  gpa_tu_token_next_dup (const guchar *buffer, gint buffer_length, gint *offset);

gchar *  gpa_tu_token_previous_dup (const guchar *buffer, gint buffer_length, gint *offset);

gboolean gpa_tu_token_next_verify (const guchar *buffer, gint buffer_length,
						 gint *offset, const gchar *label);

gchar *  gpa_tu_token_next_dup_till (const guchar *buffer, gint buffer_size,
						   gint *offset, guchar till_me);
gboolean gpa_tu_token_next_till (const guchar *buffer, gint buffer_size,
					    gint *offset, guchar till_me);
	
gboolean gpa_tu_token_next_search (const guchar *buffer, gint buffer_length, gint *offset,
						 const gchar *search_text, gboolean case_sensitive);


gboolean gpa_tu_string_contains_newline (const gchar *token);

END_GNOME_DECLS

#endif /* __GPA_TEXT_UTILS_H__ */




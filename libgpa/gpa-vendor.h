#ifndef __GPA_VENDOR_H__
#define __GPA_VENDOR_H__

/*                                                            
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Chema Celorio <chema@celorio.com>
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2000-2001 Ximian, Inc. and Jose M. Celorio
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include <tree.h>

#include "gpa-list.h"

#define GPA_TYPE_VENDOR (gpa_vendor_get_type ())
#define GPA_VENDOR(obj) (GTK_CHECK_CAST ((obj), GPA_TYPE_VENDOR, GPAVendor))
#define GPA_VENDOR_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GPA_TYPE_VENDOR, GPAVendorClass))
#define GPA_IS_VENDOR(obj) (GTK_CHECK_TYPE ((obj), GPA_TYPE_VENDOR))
#define GPA_IS_VENDOR_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GPA_TYPE_VENDOR))

typedef struct _GPAVendor GPAVendor;
typedef struct _GPAVendorClass GPAVendorClass;

/* GPAVendor */

struct _GPAVendor {
	GPANode node;
	GPANode *name;
	GPANode *url;
	GPAList *models;
};

struct _GPAVendorClass {
	GPANodeClass node_class;
};

#define GPA_VENDOR_ID(v) (GPA_ITEM (v)->id)

GtkType gpa_vendor_get_type (void);

GPANode *gpa_vendor_new_from_tree (xmlNodePtr tree);

GPANode *gpa_vendor_get_by_id (const guchar *id);

/* GPAVendorList */

GPAList *gpa_vendor_list_load (void);

#if 0
GPAVendor *gpa_vendor_list_get_vendor_by_id (GPAList *vendors, const gchar *id);
/* Helpers */
GPAVendor * gpa_vendor_get_by_name (const gchar *name);
GPAModel *gpa_vendor_get_model_by_id (GPAVendor *vendor, const gchar *id);
#endif

END_GNOME_DECLS

#endif /* __GPA_VENDOR_H__ */


#ifndef __GPA_OPTIONS_PRIVATE_H__
#define __GPA_OPTIONS_PRIVATE_H__

/*
 * Copyright (C) 2000-2001 Ximian, Inc. and Jose M Celorio
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *  Chema Celorio <chema@celorio.com>
 *  Lauris Kaplinski <lauris@ximian.com>
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include "xml-utils.h"

#include "gpa-element.h"
#include "gpa-option.h"
#include "gpa-options.h"

struct _GPAOptions {
	GPAElement element;

	gchar *name;  /* Name of the Options. Like : "Paper Size" */

	/* We use GPAItem id (Lauris)
	 *Id. Like "PaperSize". The id is the internal tag
	 * used. Since the name can be translated */

	GList *code_fragments; /* List of GpaCodeFragments structs */

	gboolean defined;

	GpaOptionsGroup group;
	GpaContent      content;
	GpaOptionsType  options_type;

	GpaOption *parent;
	GList *children; /* List of "Option" elements */

	gchar *frame; /* The Frame that this options belong to in the config dialog
		       * it is a pointer to a string that lives inside a g_hash so
		       * it should not be freed. The string can be shared by other
		       * options that have the same frame name */
};

struct _GPAOptionsClass {
	GPAElementClass element_class;
};

/* fixme: Temporary hack (Lauris) */
GList *gpa_options_parse_default (GpaOptions *options, GList *list);
GList *gpa_options_list_parse_default (GPAList *optionslist, GList *list);

/* XML Writing */
xmlNodePtr gpa_options_write_paths_from_model (XmlParseContext *context,
					       GPAModel *model);
xmlNodePtr gpa_options_write (XmlParseContext *context, GpaOptions *options);
xmlNodePtr gpa_options_list_write (XmlParseContext *context, GPAList *list);


/* XML Reading */
GpaOptions * gpa_options_new_from_node (xmlNodePtr tree_, GpaOption *parent);
GPAList * gpa_options_list_new_from_node (xmlNodePtr tree_);

/* Given a option type in a string (i.e. "PickOne") returns it's enumed value */
GpaOptionsType gpa_options_get_type_from_string (const gchar *text);


/* Basic "Options" object operations, either by individual objects
 * or by a list of them */
GpaOptions * gpa_options_new (const gchar *name,
			      const gchar *id,
			      gint group);

gboolean   gpa_options_verify (GpaOptions *options,  const GPAModel *model);
gboolean   gpa_options_list_verify (const GPAModel *model);

gboolean   gpa_options_verify_with_settings (GpaOptions *options, GpaSettings *settings);
gboolean   gpa_options_list_verify_with_settings (const GPAModel *model, GpaSettings *settings);

GpaOptions * gpa_options_copy (GpaOptions *options);
gboolean     gpa_options_list_copy (GPAPrinter *printer, GPAModel *model);

/* From gpa-options.h */

/* Utility functions */
GpaOption *    gpa_options_get_selected_option (GpaSettings *settings,
						const GpaOptions *options,
						gboolean fail);

/* Access to the structure from the outside */
const gchar * gpa_options_get_name  (const GpaOptions *options);
const gchar * gpa_options_get_id    (const GpaOptions *options);
const gchar * gpa_options_get_frame (const GpaOptions *options);
      gchar * gpa_options_dup_id    (const GpaOptions *options);
      gchar * gpa_options_dup_path  (const GpaOptions *options);

 GList * gpa_options_get_children   (const GpaOptions *options);
gboolean gpa_options_have_children  (const GpaOptions *options);

const GpaOption * gpa_options_get_parent  (const GpaOptions *options);

GpaOptionsGroup gpa_options_get_group   (const GpaOptions *options);
GpaOptionsType  gpa_options_get_options_type    (const GpaOptions *options);
GpaPickoneType  gpa_options_get_pickone_type (const GpaOptions *options);
GpaContent      gpa_options_get_content (const GpaOptions *options);

#if 0
/* Use gpa_node_unref instead of gpa_options(_list)_free (Lauris) */
void       gpa_options_list_free (GList *options_list);
#endif

END_GNOME_DECLS

#endif /* __GPA_OPTIONS_PRIVATE_H__ */


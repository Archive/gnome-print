#define __GPA_VALUE_C__

/*                                                            
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 */

#include <config.h>
#include <xmlmemory.h>
#include "gpa-value.h"

/* GPAValue */

static void gpa_value_class_init (GPAValueClass *klass);
static void gpa_value_init (GPAValue *value);

static void gpa_value_destroy (GtkObject *object);

GPANode *gpa_value_duplicate (GPANode *node);
static guchar *gpa_value_get_value (GPANode *node);

static GPANodeClass *value_parent_class = NULL;

GtkType
gpa_value_get_type (void) {
	static GtkType value_type = 0;
	if (!value_type) {
		GtkTypeInfo value_info = {
			"GPAValue",
			sizeof (GPAValue),
			sizeof (GPAValueClass),
			(GtkClassInitFunc)  gpa_value_class_init,
			(GtkObjectInitFunc) gpa_value_init,
			NULL, NULL, NULL
		};
		value_type = gtk_type_unique (GPA_TYPE_NODE, &value_info);
	}
	return value_type;
}

static void
gpa_value_class_init (GPAValueClass *klass)
{
	GtkObjectClass *object_class;
	GPANodeClass *node_class;

	object_class = (GtkObjectClass *) klass;
	node_class = (GPANodeClass *) klass;

	value_parent_class = gtk_type_class (GPA_TYPE_NODE);

	object_class->destroy = gpa_value_destroy;

	node_class->duplicate = gpa_value_duplicate;
	node_class->get_value = gpa_value_get_value;
}

static void
gpa_value_init (GPAValue *value)
{
	value->value = NULL;
}

static void
gpa_value_destroy (GtkObject *object)
{
	GPAValue *value;

	value = GPA_VALUE (object);

	if (value->value) {
		g_free (value->value);
		value->value = NULL;
	}

	if (((GtkObjectClass *) value_parent_class)->destroy)
		(* ((GtkObjectClass *) value_parent_class)->destroy) (object);
}

GPANode *
gpa_value_duplicate (GPANode *node)
{
	GPAValue *value, *new;

	value = GPA_VALUE (node);

	new = (GPAValue *) gpa_node_new (GPA_TYPE_VALUE, NULL);

	if (value->value) new->value = g_strdup (value->value);

	return GPA_NODE (new);
}

static guchar *
gpa_value_get_value (GPANode *node)
{
	GPAValue *value;

	value = GPA_VALUE (node);

	if (value->value) return g_strdup (value->value);

	return NULL;
}

GPANode *
gpa_value_new (const guchar *id, const guchar *content)
{
	GPAValue *value;

	g_return_val_if_fail (content != NULL, NULL);
	g_return_val_if_fail (*content != '\0', NULL);
	g_return_val_if_fail (!id || *id, NULL);

	value = GPA_VALUE (gpa_node_new (GPA_TYPE_VALUE, id));

	value->value = g_strdup (content);

	return GPA_NODE (value);
}

GPANode *
gpa_value_new_from_tree (const guchar *id, xmlNodePtr tree)
{
	GPANode *value;
	xmlChar *xmlval;

	g_return_val_if_fail (tree != NULL, NULL);
	g_return_val_if_fail (!id || *id, NULL);

	xmlval = xmlNodeGetContent (tree);
	g_return_val_if_fail (xmlval != NULL, NULL);

	value = gpa_value_new (id, xmlval);

	xmlFree (xmlval);

	return value;
}

gboolean
gpa_value_set_value_forced (GPAValue *value, const guchar *val)
{
	g_return_val_if_fail (value != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_VALUE (value), FALSE);
	g_return_val_if_fail (val != NULL, FALSE);
	g_return_val_if_fail (*val != '\0', FALSE);

	if (value->value) g_free (value->value);
	value->value = g_strdup (val);

	return TRUE;
}



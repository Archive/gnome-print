#define __GPA_ROOT_C__

/*
 * GPARoot
 *
 * Opaque root object to gnome-print configuration tree
 *
 * Authors:
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 */

#include <string.h>
#include "text-utils.h"
#include "gpa-utils.h"
#include "gpa-vendor.h"
#include "gpa-printer.h"
#include "gpa-root.h"

/* GPARoot */

static void gpa_root_class_init (GPARootClass *klass);
static void gpa_root_init (GPARoot *root);

static void gpa_root_destroy (GtkObject *object);

static GPANode *gpa_root_get_child (GPANode *node, GPANode *ref);
static GPANode *gpa_root_lookup (GPANode *node, const guchar *path);
static void gpa_root_modified (GPANode *node);

/* Helpers */

static GPANodeClass *parent_class;

GtkType
gpa_root_get_type (void)
{
	static GtkType root_type = 0;
	if (!root_type) {
		GtkTypeInfo root_info = {
			"GPARoot",
			sizeof (GPARoot),
			sizeof (GPARootClass),
			(GtkClassInitFunc) gpa_root_class_init,
			(GtkObjectInitFunc) gpa_root_init,
			NULL, NULL, NULL
		};
		root_type = gtk_type_unique (gpa_node_get_type (), &root_info);
	}
	return root_type;
}

static void
gpa_root_class_init (GPARootClass *klass)
{
	GtkObjectClass *object_class;
	GPANodeClass *node_class;

	object_class = (GtkObjectClass*) klass;
	node_class = (GPANodeClass *) klass;

	parent_class = gtk_type_class (gpa_node_get_type ());

	object_class->destroy = gpa_root_destroy;

	node_class->get_child = gpa_root_get_child;
	node_class->lookup = gpa_root_lookup;

	node_class->modified = gpa_root_modified;
}

static void
gpa_root_init (GPARoot *root)
{
	root->vendors = gpa_vendor_list_load ();
	g_assert (GPA_NODE (root->vendors)->parent == NULL);
	GPA_NODE (root->vendors)->parent = GPA_NODE (root);
	root->printers = gpa_printer_list_load ();
	g_assert (GPA_NODE (root->printers)->parent == NULL);
	GPA_NODE (root->printers)->parent = GPA_NODE (root);
}

static void
gpa_root_destroy (GtkObject *object)
{
	GPARoot *root;

	root = (GPARoot *) object;

	if (root->vendors) {
		root->vendors = (GPAList *) gpa_node_detach_unref (GPA_NODE (root), GPA_NODE (root->vendors));
	}

	if (root->printers) {
		root->printers = (GPAList *) gpa_node_detach_unref (GPA_NODE (root), GPA_NODE (root->printers));
	}

	if (((GtkObjectClass *) parent_class)->destroy)
		(* ((GtkObjectClass *) parent_class)->destroy) (object);
}

static GPANode *
gpa_root_get_child (GPANode *node, GPANode *ref)
{
	GPARoot *root;

	root = GPA_ROOT (node);

	g_return_val_if_fail (root->vendors != NULL, NULL);
	g_return_val_if_fail (root->printers != NULL, NULL);
	g_return_val_if_fail (!ref || ref->parent == node, NULL);

	if (ref == NULL) {
		gpa_node_ref (GPA_NODE (root->vendors));
		return GPA_NODE (root->vendors);
	} else if (ref == GPA_NODE (root->vendors)) {
		gpa_node_ref (GPA_NODE (root->printers));
		return GPA_NODE (root->printers);
	}

	return NULL;
}

static GPANode *
gpa_root_lookup (GPANode *node, const guchar *path)
{
	GPARoot *root;
	GPANode *child;

	root = GPA_ROOT (node);

	child = NULL;

	if (gpa_node_lookup_ref (&child, GPA_NODE (root->vendors), path, "Vendors")) return child;
	if (gpa_node_lookup_ref (&child, GPA_NODE (root->printers), path, "Printers")) return child;

	return NULL;
}

static void
gpa_root_modified (GPANode *node)
{
	GPARoot *root;

	root = GPA_ROOT (node);

	if (root->vendors && (GTK_OBJECT_FLAGS (root->vendors) & GPA_MODIFIED_FLAG)) {
		gpa_node_emit_modified (GPA_NODE (root->vendors));
	}
	if (root->printers && (GTK_OBJECT_FLAGS (root->printers) & GPA_MODIFIED_FLAG)) {
		gpa_node_emit_modified (GPA_NODE (root->printers));
	}
}

GPARoot *
gpa_root_get (void)
{
	GPARoot *root;

	root = gpa_root ();

	gpa_node_ref (GPA_NODE (root));

	return root;
}

GPARoot *
gpa_root (void)
{
	static GPARoot *root = NULL;

	if (root == NULL) root = gtk_type_new (GPA_TYPE_ROOT);

	return root;
}


#ifndef __GPA_GENERIC_PS_H__
#define __GPA_GENERIC_PS_H__

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include "gpa-root.h"
#include "gpa-printer.h"

extern gboolean   debug_turned_on;

GPAPrinter * gpa_generic_ps_printer (GPARoot *root);

END_GNOME_DECLS

#endif /*  __GPA_GENERIC_PS_H__  */


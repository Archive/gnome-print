#define __GNOME_FONT_INSTALL_C__

/*
 * Fontmap file generator for gnome-print
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 * Authors:
 *   Lauris Kaplinski <lauris@ximian.com>
 *   Chris Lahey <clahey@ximian.com>
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <stdio.h>
#include <popt-gnome.h>
#include <glib.h>
/* I know, that is is not nice, but that is exactly, what xml-config gives us */
#include <parser.h>
#include <xmlmemory.h>
/* End of ugly thing */
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-util.h>
#include <libgnome/gnome-i18n.h>
#include <libgnomeprint/gp-fontmap.h>
#include "gf-pfb.h"
#include "gf-ttf.h"
#include "gf-fontmap.h"

typedef struct {
	gchar *name;
	gchar *familyname;
	gchar *psname;
	GSList *afm_list;
	GSList *pfb_list;
	GSList *ttf_list;
} GFIFontData;

static void gfi_sort_files (GSList **files);
static void gfi_sort_filenames (GSList **names);
static void gfi_sort_aliases (GSList **aliases);

#if 0
static void gfi_verify_font_entry (GPFontEntry *e);
static void gfi_verify_afm_file (GPFontEntry *e, GPFileEntry *f);
static void gfi_verify_pfb_file (GPFontEntry *e, GPFileEntry *f);
static void gfi_verify_ttf_file (GPFontEntry *e, GPFileEntry *f);
#endif

static GFIFileData * gfi_read_afm_file_data (const gchar *name);
static GFIFileData * gfi_read_pfb_file_data (const gchar *name);
static GFIFileData * gfi_read_ttf_file_data (const gchar *name);

#if 0
static gboolean gfi_test_file_changed (GPFileEntry *f);
#endif

static void gfi_scan_directory (const gchar *name);
static void gfi_try_font_file (const gchar *name);
static void gfi_sort_fonts (void);
static void gfi_build_fonts (void);
static void gfi_build_font (GFIFontData *fd);
static void gfi_write_fontmap (FILE * f);
static void gfi_write_font (xmlNodePtr root, GPFontEntry *e);
static void gfi_write_alias (xmlNodePtr root, GFIFontAlias *a);

/*
 * We use simpler arguments than original version
 *
 * --debug prints debugging information
 * --recursive searches directories recursively
 * --aliases specifies file containing font aliases
 * --target specifies output file (stdout if none)
 *
 */

static gboolean gfi_debug = FALSE;
static gboolean gfi_clean = FALSE;
static gboolean gfi_recursive = FALSE;
static gchar *target = NULL;
static void add_alias_file (poptContext ctx, enum poptCallbackReason reason, const struct poptOption *opt, const char *arg, void *data);

#if 0
static void gfi_try_srcfile (const gchar *name);
static void gfi_try_srcfont (xmlNodePtr node);
#endif

static const struct poptOption options[] = {
	{ "debug", 'd', POPT_ARG_NONE, &gfi_debug, 0,
	  N_("Print out debugging information"), NULL },
	{ "clean", 'c', POPT_ARG_NONE, &gfi_clean, 0,
	  N_("Do not try to read existing fontmaps"), NULL },
	{ "recursive", 'r', POPT_ARG_NONE, &gfi_recursive, 0,
	  N_("Search directories recursively"), NULL },
	{ "target", 't', POPT_ARG_STRING, &target, 0,
	  N_("Output file name"), NULL },
	{ NULL, '\0', POPT_ARG_CALLBACK, &add_alias_file, 0 },
	{ "aliases", 'a', POPT_ARG_STRING, NULL, 0,
	  N_("Search this path for .alias files."), N_("PATH") },
	POPT_AUTOHELP
	{ NULL, '\0', 0, NULL, 0 }
};

static GSList * aliasfile_list = NULL;

static GSList * file_list = NULL;
static GSList * name_list = NULL;
static GSList * alias_list = NULL;

static GSList * goodafm_list = NULL;
static GHashTable * goodafm_dict = NULL;
static GSList * goodpfb_list = NULL;
static GHashTable * goodpfb_dict = NULL;
static GSList * goodttf_list = NULL;
static GHashTable * goodttf_dict = NULL;

static GSList * font_list = NULL;
static GHashTable * font_dict = NULL;
static GSList * goodfont_list = NULL;
static GHashTable * goodfont_dict = NULL;

int main (int argc, const char ** argv)
{
	poptContext ctx;
	char ** args;
	FILE *of;
	gint i;

	/* Initialize dictionaries */
	goodafm_dict = g_hash_table_new (g_str_hash, g_str_equal);
	goodpfb_dict = g_hash_table_new (g_str_hash, g_str_equal);
	goodttf_dict = g_hash_table_new (g_str_hash, g_str_equal);
	font_dict = g_hash_table_new (g_str_hash, g_str_equal);
	goodfont_dict = g_hash_table_new (g_str_hash, g_str_equal);

	/* Parse arguments */
	ctx = poptGetContext (NULL, argc, argv, options, 0);
	g_return_val_if_fail (ctx != NULL, 1);
	g_return_val_if_fail (poptGetNextOpt (ctx) == -1, 1);
	args = (char **) poptGetArgs (ctx);

	if (!gfi_clean) {
		/* Step 1: Read existing fontmap(s) */
		if (gfi_debug) fprintf (stderr, "Reading fontmap... ");
		gfi_read_fontmap (&file_list, &name_list, &alias_list);
		if (gfi_debug) fprintf (stderr, "Done\n");
	}

	/* Step 2: Read alias files */
	if (gfi_debug) fprintf (stderr, "Scanning aliases (%d) ... ", g_slist_length (aliasfile_list));
	while (aliasfile_list) {
		gfi_read_alias_file (&alias_list, (guchar *) aliasfile_list->data);
		g_free (aliasfile_list->data);
		aliasfile_list = g_slist_remove (aliasfile_list, aliasfile_list->data);
	}
	if (gfi_debug) fprintf (stderr, "Done\n");

	/* Step 3: Sort file entries */
	if (gfi_debug) fprintf (stderr, "Sorting file entries ... ");
	gfi_sort_files (&file_list);
	if (gfi_debug) fprintf (stderr, "Done\n");

	/* Step 4: Check files referred by name */
	if (gfi_debug) fprintf (stderr, "Sorting file names ... ");
	gfi_sort_filenames (&name_list);
	if (gfi_debug) fprintf (stderr, "Done\n");

	/* Step 5: Scan directories for font files */
	if (args && args[0]) {
		if (gfi_debug) fprintf (stderr, "Scanning directories:\n");
		for (i = 0; args && args[i]; i++) {
			gfi_scan_directory (args[i]);
		}
		if (gfi_debug) fprintf (stderr, "Done directories\n");
	}

	/* Free popt context */
	poptFreeContext (ctx);

	/*
	 * Now we have:
	 *
	 * alias_list, pointing to installer-specific FontAliases
	 * goodafm_list, goodpfb_list pointing to new FontData
	 * goodafm_dict, goodpfb_dict using list member names
	 *
	 */

	/* Sort all files into fonts */
	if (gfi_debug) fprintf (stderr, "Sorting fonts... ");
	gfi_sort_fonts ();
	if (gfi_debug) fprintf (stderr, "Done\n");

	/* Now we are ready to build fonts */
	if (gfi_debug) fprintf (stderr, "Building fonts... ");
	gfi_build_fonts ();
	if (gfi_debug) fprintf (stderr, "Done\n");

	/* As a last step, sort aliases */
	if (gfi_debug) fprintf (stderr, "Sorting aliases... ");
	gfi_sort_aliases (&alias_list);
	if (gfi_debug) fprintf (stderr, "Done\n");

#if 0
	if (gfi_debug) fprintf (stderr, "Verifying fontmap entries ");
	for (l = map->fonts; l != NULL; l = l->next) {
		gfi_verify_font_entry ((GPFontEntry *) l->data);
		if (gfi_debug) fprintf (stderr, ".");
	}
	if (gfi_debug) fprintf (stderr, "Done\n");

	/* Process .font files */
	if (gfi_debug) fprintf (stderr, "Scanning source maps... ");
	gfi_scan_srcpaths (fontmappath_list);
	if (gfi_debug) fprintf (stderr, "Done\n");

	/* Type1Aliases */
	if (gfi_debug) fprintf (stderr, "Sorting Type1 aliases... ");
	gfi_process_type1_aliases ();
	if (gfi_debug) fprintf (stderr, "Done\n");

#endif

	if (!goodfont_list) {
		if (gfi_debug) fprintf (stderr, "Something went very wrong - NO FONTS FOUND\n");
	}

	/*
	 * Write fontmap
	 *
	 */

	if (target) {
		umask (022);
		of = fopen (target, "w");
		if (of) {
			gfi_write_fontmap (of);
			fclose (of);
		} else {
			g_print ("Cannot open output file %s\n", target);
		}
	} else {
		gfi_write_fontmap (stdout);
	}

	return 0;
}

/* Sort file entries into dicts/lists */

static void
gfi_sort_files (GSList **files)
{
	while (*files) {
		GFIFileData *fd;
		gboolean keep;
		fd = (GFIFileData *) (*files)->data;
		keep = FALSE;
		switch (fd->type) {
		case GFI_FILE_PFB:
			if (!g_hash_table_lookup (goodpfb_dict, fd->entry.name)) {
				goodpfb_list = g_slist_prepend (goodpfb_list, fd);
				g_hash_table_insert (goodpfb_dict, fd->entry.name, fd);
				keep = TRUE;
			}
			break;
		case GFI_FILE_AFM:
			if (!g_hash_table_lookup (goodafm_dict, fd->entry.name)) {
				goodafm_list = g_slist_prepend (goodafm_list, fd);
				g_hash_table_insert (goodafm_dict, fd->entry.name, fd);
				keep = TRUE;
			}
			break;
		case GFI_FILE_TTF:
			if (!g_hash_table_lookup (goodttf_dict, fd->entry.name)) {
				goodttf_list = g_slist_prepend (goodttf_list, fd);
				g_hash_table_insert (goodttf_dict, fd->entry.name, fd);
				keep = TRUE;
			}
			break;
		default:
			break;
		}
		if (!keep) {
			if (fd->entry.name) g_free (fd->entry.name);
			if (fd->fontname) g_free (fd->fontname);
			if (fd->familyname) g_free (fd->familyname);
			if (fd->speciesname) g_free (fd->speciesname);
			if (fd->psname) g_free (fd->psname);
			if (fd->version) g_free (fd->version);
			g_free (fd);
		}
		*files = g_slist_remove (*files, (*files)->data);
	}
}

/* Check and sort font file names from list */

static void
gfi_sort_filenames (GSList **names)
{
	while (*names) {
		gfi_try_font_file ((gchar *) (*names)->data);
		*names = g_slist_remove (*names, (*names)->data);
	}
}

/* Scan directories for font files */

static void
gfi_scan_directory (const gchar *name)
{
	DIR * dir;
	struct dirent * dent;
	struct stat s;

	dir = opendir (name);

	if (dir) {
		if (gfi_debug) fprintf (stderr, "Scanning %s\n", name);
		while ((dent = readdir (dir))) {
			gchar * fn;
			if (*dent->d_name == '.') continue;
			fn = g_concat_dir_and_file (name, dent->d_name);
			if (!stat (fn, &s)) {
				if (gfi_recursive && S_ISDIR (s.st_mode)) {
					gfi_scan_directory (fn);
				} else if (S_ISREG (s.st_mode)) {
					gfi_try_font_file (fn);
				}
			}
			g_free (fn);
		}
	} else {
		if (gfi_debug) fprintf (stderr, "Invalid directory: %s\n", name);
	}
}

/* Sort aliases */

static void
gfi_sort_aliases (GSList **aliases)
{
	GSList *newaliases;
	GHashTable *adict;

	newaliases = NULL;
	adict = g_hash_table_new (g_str_hash, g_str_equal);

	while (*aliases) {
		GFIFontAlias *a;
		a = (GFIFontAlias *) (*aliases)->data;
		*aliases = g_slist_remove (*aliases, a);
		if (g_hash_table_lookup (goodfont_dict, a->fontname)) continue;
		if (g_hash_table_lookup (adict, a->fontname)) continue;
		/* There is neither font nor alias with such name */
		if (!a->originalname) {
			GSList *l;
			for (l = goodfont_list; l != NULL; l = l->next) {
				if (!strcmp (a->originalpsname, ((GPFontEntry *) l->data)->psname)) {
					a->originalname = g_strdup (((GPFontEntry *) l->data)->name);
					break;
				}
			}
		}
		if (a->originalname) {
			newaliases = g_slist_prepend (newaliases, a);
			g_hash_table_insert (adict, a->fontname, a);
		}
	}

	g_hash_table_destroy (adict);

	*aliases = newaliases;
}

/*
 * Return newly allocated GFIFileData or NULL if not valid afm file
 */

static GFIFileData *
gfi_read_afm_file_data (const gchar * name)
{
	GFIFileData * fd;
	FILE * f;
	int status;
	Font_Info * fi;
	struct stat s;

	fi = NULL;

	if (stat (name, &s) < 0) return NULL;

	f = fopen (name, "r");
	if (!f) return NULL;

	status = parseFile (f, &fi, P_G);

	fclose (f);
	if (status != AFM_ok) {
		if (fi) parseFileFree (fi);
		return NULL;
	}

	/* Loading afm succeeded, so go ahead */

	fd = g_new (GFIFileData, 1);

	fd->type = GFI_FILE_AFM;
	fd->entry.name = g_strdup (name);
	fd->entry.size = s.st_size;
	fd->entry.mtime = s.st_mtime;
	fd->fontname = g_strdup (fi->gfi->fullName);
	fd->familyname = g_strdup (fi->gfi->familyName);
	fd->psname = g_strdup (fi->gfi->fontName);
	fd->version = g_strdup (fi->gfi->version);

	parseFileFree (fi);

	return fd;
}

/*
 * Return newly allocated GFIFileData or NULL if not valid pfb file
 */

static GFIFileData *
gfi_read_pfb_file_data (const gchar * name)
{
	GFIFileData * fd;
	GFPFB * pfb;
	struct stat s;

	if (stat (name, &s) < 0) return NULL;

	pfb = gf_pfb_open (name);
	if (!pfb) return NULL;

	/* Loading pfb succeeded, so go ahead */

	fd = g_new (GFIFileData, 1);

	fd->type = GFI_FILE_PFB;
	fd->entry.name = g_strdup (name);
	fd->entry.size = s.st_size;
	fd->entry.mtime = s.st_mtime;
	fd->fontname = g_strdup (pfb->gfi.fullName);
	fd->familyname = g_strdup (pfb->gfi.familyName);
	fd->psname = g_strdup (pfb->gfi.fontName);
	fd->version = g_strdup (pfb->gfi.version);

	gf_pfb_close (pfb);

	return fd;
}

/*
 * Return newly allocated GFIFileData or NULL if not valid ttf file
 */

static GFIFileData *
gfi_read_ttf_file_data (const gchar * name)
{
	GFIFileData * fd;
	GFTTF * ttf;
	struct stat s;

	if (stat (name, &s) < 0) return NULL;

	ttf = gf_ttf_open (name);
	if (!ttf) return NULL;

	/* Loading pfb succeeded, so go ahead */

	fd = g_new (GFIFileData, 1);

	fd->type = GFI_FILE_TTF;
	fd->entry.name = g_strdup (name);
	fd->entry.size = s.st_size;
	fd->entry.mtime = s.st_mtime;
	fd->fontname = g_strdup (ttf->gfi.fullName);
	fd->familyname = g_strdup (ttf->gfi.familyName);
	fd->psname = g_strdup (ttf->gfi.fontName);
	fd->version = g_strdup (ttf->gfi.version);

	gf_ttf_close (ttf);

	return fd;
}

#if 0
static gboolean
gfi_test_file_changed (GPFileEntry * f)
{
	struct stat s;

	if (stat (f->name, &s) < 0) return TRUE;

	/* If we do not have file info, expect it to be changed */

	if ((f->size == 0) || (s.st_size == f->size)) return TRUE;
	if ((f->mtime == 0) || (s.st_mtime == f->mtime)) return TRUE;

	return FALSE;
}
#endif

static void
gfi_try_font_file (const gchar *fn)
{
	GFIFileData * fd;
	struct stat s;
	gchar *name;

	if (!g_path_is_absolute (fn)) {
		gchar *cdir;
		cdir = g_get_current_dir ();
		name = g_concat_dir_and_file (cdir, fn);
	} else {
		name = g_strdup (fn);
	}

	if (stat (name, &s) || !S_ISREG (s.st_mode)) {
		g_free (name);
		return;
	}

	fd = g_hash_table_lookup (goodafm_dict, name);
	if (!fd) fd = g_hash_table_lookup (goodpfb_dict, name);
	if (!fd) fd = g_hash_table_lookup (goodttf_dict, name);
	if (fd) {
		g_free (name);
		return;
	}

	/* Not registered, so try to determine file type */

	fd = gfi_read_afm_file_data (name);
	if (fd) {
		goodafm_list = g_slist_prepend (goodafm_list, fd);
		g_hash_table_insert (goodafm_dict, fd->entry.name, fd);
		g_free (name);
		return;
	}

	fd = gfi_read_pfb_file_data (name);
	if (fd) {
		goodpfb_list = g_slist_prepend (goodpfb_list, fd);
		g_hash_table_insert (goodpfb_dict, fd->entry.name, fd);
		g_free (name);
		return;
	}

	fd = gfi_read_ttf_file_data (name);
	if (fd) {
		goodttf_list = g_slist_prepend (goodttf_list, fd);
		g_hash_table_insert (goodttf_dict, fd->entry.name, fd);
		g_free (name);
		return;
	}

	g_free (name);
	/* Cannot read :( */
}

/*
 * Arranges all afm, pfb and ttf FileData into FontData structures
 * goodfont_list - list of new FontData entries
 * goodfont_dict - use FontData name strings
 * Original lists are cleaned
 *
 */

static void
gfi_sort_fonts (void)
{
	GFIFileData * file;
	GFIFontData * font;

	while (goodafm_list) {
		file = (GFIFileData *) goodafm_list->data;
		font = g_hash_table_lookup (font_dict, file->fontname);
		if (!font) {
			font = g_new (GFIFontData, 1);
			font->name = g_strdup (file->fontname);
			font->familyname = g_strdup (file->familyname);
			font->psname = g_strdup (file->psname);
			font->afm_list = font->pfb_list = font->ttf_list = NULL;
			font_list = g_slist_prepend (font_list, font);
			g_hash_table_insert (font_dict, font->name, font);
		}
		font->afm_list = g_slist_prepend (font->afm_list, file);
		goodafm_list = g_slist_remove (goodafm_list, file);
	}

	while (goodpfb_list) {
		file = (GFIFileData *) goodpfb_list->data;
		font = g_hash_table_lookup (font_dict, file->fontname);
		if (!font) {
			font = g_new (GFIFontData, 1);
			font->name = g_strdup (file->fontname);
			font->familyname = g_strdup (file->familyname);
			font->psname = g_strdup (file->psname);
			font->afm_list = font->pfb_list = font->ttf_list = NULL;
			font_list = g_slist_prepend (font_list, font);
			g_hash_table_insert (font_dict, font->name, font);
		}
		font->pfb_list = g_slist_prepend (font->pfb_list, file);
		goodpfb_list = g_slist_remove (goodpfb_list, file);
	}

	while (goodttf_list) {
		file = (GFIFileData *) goodttf_list->data;
		font = g_hash_table_lookup (font_dict, file->fontname);
		if (!font) {
			font = g_new (GFIFontData, 1);
			font->name = g_strdup (file->fontname);
			font->familyname = g_strdup (file->familyname);
			font->psname = g_strdup (file->psname);
			font->afm_list = font->pfb_list = font->ttf_list = NULL;
			font_list = g_slist_prepend (font_list, font);
			g_hash_table_insert (font_dict, font->name, font);
		}
		font->ttf_list = g_slist_prepend (font->ttf_list, file);
		goodttf_list = g_slist_remove (goodttf_list, file);
	}
}


static void
gfi_build_fonts (void)
{
	while (font_list) {
		gfi_build_font ((GFIFontData *) font_list->data);
		font_list = g_slist_remove (font_list, font_list->data);
	}
}

static void
gfi_build_font (GFIFontData * fd)
{
	GPFontEntry * new;
	GFIFileData *afmdata, *pfbdata, *ttfdata;
	gdouble afmversion, pfbversion, ttfversion;
	GSList * l;

	/* Return if we are already registered */
	/* Fixme: We should free structs */
	if (g_hash_table_lookup (goodfont_dict, fd->name)) return;

	pfbdata = NULL;
	pfbversion = -1e18;

	/* Find pfb vith highest version */
	for (l = fd->pfb_list; l != NULL; l = l->next) {
		GFIFileData * d;
		gdouble v;
		d = (GFIFileData *) l->data;
		v = atof (d->version);
		if (v > pfbversion) {
			pfbversion = v;
			pfbdata = d;
		}
	}

	afmdata = NULL;
	afmversion = -1e18;

	/* Find afm vith highest version == pfb version */
	for (l = fd->afm_list; l != NULL; l = l->next) {
		GFIFileData * d;
		gdouble v;
		d = (GFIFileData *) l->data;
		v = atof (d->version);
		if (v == pfbversion) {
			afmversion = v;
			afmdata = d;
			break;
		}
	}

	ttfdata = NULL;
	ttfversion = -1e18;

	/* Find ttf vith highest version > MIN (afm, pfb) version */
	for (l = fd->ttf_list; l != NULL; l = l->next) {
		GFIFileData * d;
		gdouble v;
		d = (GFIFileData *) l->data;
		v = atof (d->version);
		if ((v > afmversion) && (v > pfbversion)) {
			ttfversion = v;
			ttfdata = d;
		}
	}

	if (pfbdata && !ttfdata) {
		GPFontEntryT1 *t1;
		GFPFB *pfb;
		/* We have to read pfb to get weight and italicangle :-( */
		pfb = gf_pfb_open (pfbdata->entry.name);
		/* This shouldn't happen! */
		g_return_if_fail (pfb != NULL);
		/* Now we should have everything we need */
		t1 = g_new0 (GPFontEntryT1, 1);
		new = (GPFontEntry *) t1;
		t1->entry.type = GP_FONT_ENTRY_TYPE1;
		t1->entry.refcount = 1;
		t1->entry.face = NULL;
		t1->entry.name = g_strdup (fd->name);
		t1->entry.version = g_strdup (pfbdata->version);
		t1->entry.familyname = g_strdup (fd->familyname);
		t1->entry.speciesname = gfi_get_species_name (fd->name, fd->familyname);
		t1->entry.psname = g_strdup (fd->psname);
		t1->entry.weight = g_strdup (pfb->gfi.weight);
		if (afmdata) {
			/* AFM */
			t1->afm.name = g_strdup (afmdata->entry.name);
			t1->afm.size = afmdata->entry.size;
			t1->afm.mtime = afmdata->entry.mtime;
		}
		/* PFB */
		t1->pfb.name = g_strdup (pfbdata->entry.name);
		t1->pfb.size = pfbdata->entry.size;
		t1->pfb.mtime = pfbdata->entry.mtime;
		/* Misc */
		/* We ignore weight here */
		t1->ItalicAngle = pfb->gfi.italicAngle;
		/* Release AFM info */
		gf_pfb_close (pfb);
	} else if (ttfdata) {
		GPFontEntryTT *tt;
		GFTTF *ttf;
		/* We have to read ttf again to get weight and italicangle */
		ttf = gf_ttf_open (ttfdata->entry.name);
		g_return_if_fail (ttf != NULL);
		/* Now we should have everything we need */
		tt = g_new (GPFontEntryTT, 1);
		new = (GPFontEntry *) tt;
		tt->entry.type = GP_FONT_ENTRY_TRUETYPE;
		tt->entry.refcount = 1;
		tt->entry.face = NULL;
		tt->entry.name = g_strdup (fd->name);
		tt->entry.version = g_strdup (ttf->gfi.version);
		tt->entry.familyname = g_strdup (fd->familyname);
		tt->entry.speciesname = gfi_get_species_name (fd->name, fd->familyname);
		tt->entry.psname = g_strdup (fd->psname);
		tt->entry.weight = g_strdup (ttf->gfi.weight);
		/* TTF */
		tt->ttf.name = g_strdup (ttfdata->entry.name);
		tt->ttf.size = ttfdata->entry.size;
		tt->ttf.mtime = ttfdata->entry.mtime;
		/* Misc */
		tt->Weight = gp_fontmap_lookup_weight (new->weight);
		tt->ItalicAngle = ttf->gfi.italicAngle;
		/* Release AFM info */
		gf_ttf_close (ttf);
	} else {
		/* No suitable set of files */
		return;
	}

	/* Register it */
	if (gfi_debug) fprintf (stderr, "Registered font: %s\n", new->name);

	goodfont_list = g_slist_prepend (goodfont_list, new);
	g_hash_table_insert (goodfont_dict, new->name, new);
}

static void
gfi_write_fontmap (FILE * f)
{
	xmlDocPtr doc;
	xmlNodePtr root;

	doc = xmlNewDoc ("1.0");
	root = xmlNewDocNode (doc, NULL, "fontmap", NULL);
	xmlDocSetRootElement (doc, root);
	xmlSetProp (root, "version", "3.0");

	while (goodfont_list) {
		gfi_write_font (root, (GPFontEntry *) goodfont_list->data);
		goodfont_list = g_slist_remove (goodfont_list, goodfont_list->data);
	}

	while (alias_list) {
		gfi_write_alias (root, (GFIFontAlias *) alias_list->data);
		alias_list = g_slist_remove (alias_list, alias_list->data);
	}

	xmlDocDump (f, doc);
}

static void
gfi_write_font (xmlNodePtr root, GPFontEntry * e)
{
	xmlNodePtr n, f;
	gchar c[128];

	n = xmlNewDocNode (root->doc, NULL, "font", NULL);
	xmlAddChild (root, n);

	if (e->type == GP_FONT_ENTRY_TRUETYPE) {
		GPFontEntryTT * tt;
		tt = (GPFontEntryTT *) e;
		xmlSetProp (n, "format", "truetype");
		/* afm file */
		f = xmlNewDocNode (root->doc, NULL, "file", NULL);
		xmlAddChild (n, f);
		xmlSetProp (f, "type", "ttf");
		xmlSetProp (f, "path", tt->ttf.name);
		g_snprintf (c, 128, "%d", (gint) tt->ttf.size);
		xmlSetProp (f, "size", c);
		g_snprintf (c, 128, "%d", (gint) tt->ttf.mtime);
		xmlSetProp (f, "mtime", c);
	} else {
		GPFontEntryT1 * t1;
		t1 = (GPFontEntryT1 *) e;
		xmlSetProp (n, "format", "type1");
		if (t1->afm.name) {
			/* afm file */
			f = xmlNewDocNode (root->doc, NULL, "file", NULL);
			xmlAddChild (n, f);
			xmlSetProp (f, "type", "afm");
			xmlSetProp (f, "path", t1->afm.name);
			g_snprintf (c, 128, "%d", (gint) t1->afm.size);
			xmlSetProp (f, "size", c);
			g_snprintf (c, 128, "%d", (gint) t1->afm.mtime);
			xmlSetProp (f, "mtime", c);
		}
		/* pfb file */
		f = xmlNewDocNode (root->doc, NULL, "file", NULL);
		xmlAddChild (n, f);
		xmlSetProp (f, "type", "pfb");
		xmlSetProp (f, "path", t1->pfb.name);
		g_snprintf (c, 128, "%d", (gint) t1->pfb.size);
		xmlSetProp (f, "size", c);
		g_snprintf (c, 128, "%d", (gint) t1->pfb.mtime);
		xmlSetProp (f, "mtime", c);
	}

	/* Other properties */
	xmlSetProp (n, "name", e->name);
	xmlSetProp (n, "version", e->version);
	xmlSetProp (n, "familyname", e->familyname);
	xmlSetProp (n, "speciesname", e->speciesname);
	xmlSetProp (n, "psname", e->psname);
	xmlSetProp (n, "weight", e->weight);

	if (e->type == GP_FONT_ENTRY_TRUETYPE) {
		GPFontEntryTT * tt;
		tt = (GPFontEntryTT *) e;
		g_snprintf (c, 128, "%g", tt->ItalicAngle);
		xmlSetProp (n, "italicangle", c);
	} else {
		GPFontEntryT1 * t1;
		t1 = (GPFontEntryT1 *) e;
		g_snprintf (c, 128, "%g", t1->ItalicAngle);
		xmlSetProp (n, "italicangle", c);
	}
}

static void
gfi_write_alias (xmlNodePtr root, GFIFontAlias *a)
{
	xmlNodePtr n;

	n = xmlNewDocNode (root->doc, NULL, "font", NULL);
	xmlAddChild (root, n);

	xmlSetProp (n, "format", "alias");
	xmlSetProp (n, "name", a->fontname);
	xmlSetProp (n, "familyname", a->familyname);
	xmlSetProp (n, "speciesname", a->speciesname);
	xmlSetProp (n, "original", a->originalname);
	if (a->hideoriginal) xmlSetProp (n, "hideoriginal", "true");
}

#if 0
static gchar *
gfi_get_species_name (const gchar * fullname, const gchar * familyname)
{
	gchar * p;

	p = strstr (fullname, familyname);

	if (!p) return g_strdup ("Normal");

	p = p + strlen (familyname);

	while (*p && (*p < 'A')) p++;

	if (!*p) return g_strdup ("Normal");

	return g_strdup (p);
}
#endif

#if 0
static void
add_assignment (poptContext ctx, enum poptCallbackReason reason, const struct poptOption *opt, const char *arg, void *data)
{
	struct stat s;
	char *comma;

	comma = strchr (arg, ',');

	if (comma != NULL) {
		*comma = '\0';
		if ((stat (comma + 1, &s) == 0) && (S_ISDIR (s.st_mode))) {
			gchar *key, *path;
			GSList *alist;

			key = g_strdup (arg);
			path = g_strdup (comma + 1);
			if (gfi_debug) fprintf (stderr, "Assigned %s : %s\n", key, path);

			if (assignment_dict == NULL) assignment_dict = g_hash_table_new (g_str_hash, g_str_equal);

			alist = g_hash_table_lookup (assignment_dict, key);
			alist = g_slist_append (alist, path);
			g_hash_table_insert (assignment_dict, key, alist);
		}
	}
}
#endif

#if 0
/* Scan those infamous .font files */

static void
gfi_scan_srcpaths (GSList * paths)
{
	GSList * l;

	for (l = paths; l != NULL; l = l->next) {
		DIR *dir;
		struct dirent *direntry;
		gchar * path;

		path = (gchar *) l->data;

		dir = opendir (path);
		while ((direntry = readdir (dir))) {
			gint len;
			len = strlen (direntry->d_name);
			if ((len > 5) && (!strcmp (direntry->d_name + len - 5, ".font"))) {
				gchar *fn;
				fn = g_concat_dir_and_file (path, direntry->d_name);
				gfi_try_srcfile (fn);
				g_free (fn);
			}
		}
		closedir (dir);
	}
}

/* Try to load .font file and split it into components */

static void
gfi_try_srcfile (const gchar *name)
{
	xmlDocPtr doc;
	xmlNodePtr root;

	if (gfi_debug) fprintf (stderr, "Trying srcfile %s\n", name);

	doc = xmlParseFile (name);
	if (doc) {
		root = xmlDocGetRootElement (doc);
		if (!strcmp (root->name, "font")) {
			/* Single font entry */
			gfi_try_srcfont (root);
		} else if (!strcmp (root->name, "fontfile")) {
			xmlNodePtr child;
			/* List of font entries */
			for (child = root->xmlChildrenNode; child != NULL; child = child->next) {
				if (!strcmp (child->name, "font")) {
					gfi_try_srcfont (child);
				}
			}
		}
	}
}

static void
gfi_try_srcfont (xmlNodePtr node)
{
	char *format, *metrics, *glyphs, *alias;

	format = xmlGetProp (node, "format");
	metrics = xmlGetProp (node, "metrics");
	glyphs = xmlGetProp (node, "glyphs");
	alias = xmlGetProp (node, "alias");

	/* Reasonable entry */
	if (format && !strcmp (format, "type1") && metrics && glyphs) {
		GFIFileData *afmd, *pfbd;

		afmd = NULL;

		if (metrics) {
			if (*metrics == '*') {
				gchar *slash;
				GSList *path, *l;
				/* find assignments etc. */
				slash = strchr (metrics, '/');
				if (slash) {
					*slash = '\0';
					slash++;
					path = g_hash_table_lookup (assignment_dict, metrics + 1);
					for (l = path; l != NULL; l = l->next) {
						gchar *fn;
						fn = g_concat_dir_and_file (l->data, slash);
						afmd = g_hash_table_lookup (goodafm_dict, fn);
						if (!afmd) afmd = gfi_read_afm_file_data (fn);
						g_free (fn);
						if (afmd) {
							goodafm_list = g_slist_prepend (goodafm_list, afmd);
							g_hash_table_insert (goodafm_dict, afmd->entry.name, afmd);
							break;
						}
					}
				}
			} else if (*metrics == '/') {
				afmd = g_hash_table_lookup (goodafm_dict, metrics);
				if (!afmd) afmd = gfi_read_afm_file_data (metrics);
				if (afmd) {
					goodafm_list = g_slist_prepend (goodafm_list, afmd);
					g_hash_table_insert (goodafm_dict, afmd->entry.name, afmd);
				}
			} else {
				GSList *l;
				for (l = afmpath_list; l != NULL; l = l->next) {
					gchar *fn;
					fn = g_concat_dir_and_file (l->data, metrics);
					afmd = g_hash_table_lookup (goodafm_dict, fn);
					if (!afmd) afmd = gfi_read_afm_file_data (fn);
					g_free (fn);
					if (afmd) {
						goodafm_list = g_slist_prepend (goodafm_list, afmd);
						g_hash_table_insert (goodafm_dict, afmd->entry.name, afmd);
						break;
					}
				}
			}
		}

		pfbd = NULL;

		if (glyphs) {
			if (*glyphs == '*') {
				gchar *slash;
				GSList *path, *l;
				/* find assignments etc. */
				slash = strchr (glyphs, '/');
				if (slash) {
					*slash = '\0';
					slash++;
					path = g_hash_table_lookup (assignment_dict, glyphs + 1);
					for (l = path; l != NULL; l = l->next) {
						gchar *fn;
						fn = g_concat_dir_and_file (l->data, slash);
						pfbd = g_hash_table_lookup (goodpfb_dict, fn);
						if (!pfbd) pfbd = gfi_read_pfb_file_data (fn);
						g_free (fn);
						if (pfbd) {
							goodpfb_list = g_slist_prepend (goodpfb_list, pfbd);
							g_hash_table_insert (goodpfb_dict, pfbd->entry.name, pfbd);
							break;
						}
					}
				}
			} else if (*glyphs == '/') {
				pfbd = gfi_read_pfb_file_data (glyphs);
				if (!pfbd) pfbd = gfi_read_pfb_file_data (glyphs);
				if (pfbd) {
					goodpfb_list = g_slist_prepend (goodpfb_list, pfbd);
					g_hash_table_insert (goodpfb_dict, pfbd->entry.name, pfbd);
				}
			} else {
				GSList *l;
				for (l = pfbpath_list; l != NULL; l = l->next) {
					gchar *fn;
					fn = g_concat_dir_and_file (l->data, glyphs);
					pfbd = g_hash_table_lookup (goodpfb_dict, fn);
					if (!pfbd) pfbd = gfi_read_pfb_file_data (fn);
					g_free (fn);
					if (pfbd) {
						goodpfb_list = g_slist_prepend (goodpfb_list, pfbd);
						g_hash_table_insert (goodpfb_dict, pfbd->entry.name, pfbd);
						break;
					}
				}
			}
		}

		if (afmd && pfbd && (alias != NULL)) {
			GPFontEntryT1Alias *new;

			/* fixme: should check name matching */
			new = g_new (GPFontEntryT1Alias, 1);
			new->t1.entry.type = GP_FONT_ENTRY_TYPE1_ALIAS;
			new->t1.entry.refcount = 1;
			new->t1.entry.face = NULL;
			new->t1.entry.name = g_strdup (afmd->fontname);
			new->t1.entry.version = g_strdup (afmd->version);
			new->t1.entry.familyname = g_strdup (afmd->familyname);
			new->t1.entry.speciesname = gfi_get_species_name (afmd->fontname, afmd->familyname);
			new->t1.entry.psname = g_strdup (afmd->psname);
			/* fixme: this is fake, but it will not be used anyways */
			new->t1.entry.weight = g_strdup ("Book");
			/* AFM */
			new->t1.afm.name = g_strdup (afmd->entry.name);
			new->t1.afm.size = afmd->entry.size;
			new->t1.afm.mtime = afmd->entry.mtime;
			/* PFB */
			new->t1.pfb.name = g_strdup (pfbd->entry.name);
			new->t1.pfb.size = pfbd->entry.size;
			new->t1.pfb.mtime = pfbd->entry.mtime;
			/* Misc */
			/* fixme: */
			new->t1.Weight = GNOME_FONT_BOOK;
			new->t1.ItalicAngle = 0.0;
			new->alias = g_strdup (alias);

			type1alias_list = g_slist_prepend (type1alias_list, new);
		}
	}

	if (format) xmlFree (format);
	if (metrics) xmlFree (metrics);
	if (glyphs) xmlFree (glyphs);
	if (alias) xmlFree (alias);
}
#endif

/*
 * Checks, whether given alias file exists, and adds it to list of files
 */

static void
add_alias_file (poptContext ctx, enum poptCallbackReason reason, const struct poptOption *opt, const char *arg, void *data)
{
	struct stat s;

	if ((stat (arg, &s) == 0) && (S_ISREG (s.st_mode))) {
		aliasfile_list = g_slist_append (aliasfile_list, g_strdup (arg));
	} else {
		if (gfi_debug) fprintf (stderr, "%s is not regular file\n", arg);
	}
}

#if 0
/*
 * Process GPFontEntry
 *
 * Tests, whether both afm and pfb files are valid
 * Saves file entries to good{$filetype}list
 * If it is Type1Alias, save it to type1aliaslist
 *
 */

static void
gfi_verify_font_entry (GPFontEntry * e)
{
	GPFontEntryT1 * t1;
	GPFontEntryT1Alias * t1a;
	GPFontEntryTT * tt;

	switch (e->type) {
	case GP_FONT_ENTRY_TYPE1_ALIAS:
		t1a = (GPFontEntryT1Alias *) e;
		type1alias_list = g_slist_prepend (type1alias_list, e);
	case GP_FONT_ENTRY_TYPE1:
		t1 = (GPFontEntryT1 *) e;
		gfi_verify_afm_file (e, &t1->afm);
		gfi_verify_pfb_file (e, &t1->pfb);
		break;
	case GP_FONT_ENTRY_TRUETYPE:
		tt = (GPFontEntryTT *) e;
		gfi_verify_ttf_file (e, &tt->ttf);
		break;
	case GP_FONT_ENTRY_ALIAS:
		alias_list = gfi_add_alias_from_map (alias_list, e);
		break;
	default:
		g_assert_not_reached ();
	}
}

/*
 * Verifies afm entry
 *
 * If good, create new FileData and save it to goodafm_list/goodafm_dict
 *
 */

static void
gfi_verify_afm_file (GPFontEntry * e, GPFileEntry * f)
{
	GFIFileData * fd;

	/* Test, whether we are already verified and registered */
	if (g_hash_table_lookup (goodafm_dict, f->name)) return;

	if (!gfi_test_file_changed (f)) {
		fd = g_new0 (GFIFileData, 1);
		fd->type = GFI_FILE_AFM;
		fd->entry.name = g_strdup (f->name);
		fd->entry.size = f->size;
		fd->entry.mtime = f->mtime;
		fd->fontname = g_strdup (e->name);
		fd->familyname = g_strdup (e->familyname);
		fd->psname = g_strdup (e->psname);
		fd->version = g_strdup (e->version);
	} else {
		fd = gfi_read_afm_file_data (f->name);
	}

	if (fd) {
		goodafm_list = g_slist_prepend (goodafm_list, fd);
		g_hash_table_insert (goodafm_dict, fd->entry.name, fd);
	} else {
#ifdef GFI_VERBOSE
		if (gfi_debug) fprintf (stderr, "Not good: %s\n", f->name);
#endif
	}
}

static void
gfi_verify_pfb_file (GPFontEntry * e, GPFileEntry * f)
{
	GFIFileData * fd;

	/* Test, whether we are already verified and registered */
	if (g_hash_table_lookup (goodpfb_dict, f->name)) return;

	if (!gfi_test_file_changed (f)) {
		fd = g_new0 (GFIFileData, 1);
		fd->type = GFI_FILE_PFB;
		fd->entry.name = g_strdup (f->name);
		fd->entry.size = f->size;
		fd->entry.mtime = f->mtime;
		fd->fontname = g_strdup (e->name);
		fd->familyname = g_strdup (e->familyname);
		fd->psname = g_strdup (e->psname);
		fd->version = g_strdup (e->version);
	} else {
		fd = gfi_read_pfb_file_data (f->name);
	}

	if (fd) {
		goodpfb_list = g_slist_prepend (goodpfb_list, fd);
		g_hash_table_insert (goodpfb_dict, fd->entry.name, fd);
	} else {
#ifdef GFI_VERBOSE
		if (gfi_debug) fprintf (stderr, "Not good: %s\n", f->name);
#endif
	}
}

static void
gfi_verify_ttf_file (GPFontEntry * e, GPFileEntry * f)
{
	GFIFileData * fd;

	/* Test, whether we are already verified and registered */
	if (g_hash_table_lookup (goodttf_dict, f->name)) return;

	if (!gfi_test_file_changed (f)) {
		fd = g_new0 (GFIFileData, 1);
		fd->type = GFI_FILE_TTF;
		fd->entry.name = g_strdup (f->name);
		fd->entry.size = f->size;
		fd->entry.mtime = f->mtime;
		fd->fontname = g_strdup (e->name);
		fd->familyname = g_strdup (e->familyname);
		fd->psname = g_strdup (e->psname);
		fd->version = g_strdup (e->version);
	} else {
		fd = gfi_read_ttf_file_data (f->name);
	}

	if (fd) {
		goodttf_list = g_slist_prepend (goodttf_list, fd);
		g_hash_table_insert (goodttf_dict, fd->entry.name, fd);
	} else {
#ifdef GFI_VERBOSE
		if (gfi_debug) fprintf (stderr, "Not good: %s\n", f->name);
#endif
	}
}

#endif

#if 0
static void
gfi_process_type1_aliases (void)
{
	while (type1alias_list) {
		gfi_process_type1_alias ((GPFontEntryT1Alias *) type1alias_list->data);
		type1alias_list = g_slist_remove (type1alias_list, type1alias_list->data);
	}
}

static void
gfi_process_type1_alias (GPFontEntryT1Alias * t1a)
{
	GPFontEntryT1Alias * new;
	GFIFileData * afmdata, * pfbdata;
	GFIFontData * fontdata;
	gdouble afmversion, pfbversion;
	GSList * l;
	FILE * f;
	int status;
	Font_Info * fi;

	/* Return if we are already registered */
	/* fixme: We should test versions here */
	if (g_hash_table_lookup (goodfont_dict, t1a->t1.entry.name)) return;

	afmdata = g_hash_table_lookup (goodafm_dict, t1a->t1.afm.name);
	/* If we do not have good afm, return */
	if (!afmdata) return;

	/* We should probably use assertion here */
	fontdata = g_hash_table_lookup (font_dict, afmdata->fontname);
	if (!fontdata) return;

	/* fixme: mess with locale */
	afmversion = atof (afmdata->version);

	/* Search, whether we have same or better pfb */
	for (l = fontdata->pfb_list; l != NULL; l = l->next) {
		gdouble pfbversion;
		pfbdata = (GFIFileData *) l->data;
		pfbversion = atof (pfbdata->version);
		/* If we have original pfb with same or higher version, we shouldn't use type1 alias */
		if (pfbversion >= afmversion) return;
	}

	/* So there wasn't original pfb file */

	pfbdata = g_hash_table_lookup (goodpfb_dict, t1a->t1.pfb.name);
	if (!pfbdata) return;

	/* Find pfb file with highest version */
	/* We should probably use assertion here */
	fontdata = g_hash_table_lookup (font_dict, pfbdata->fontname);
	if (!fontdata) return;

	/* fixme: mess with locale */
	pfbversion = atof (pfbdata->version);

	for (l = fontdata->pfb_list; l != NULL; l = l->next) {
		GFIFileData * d;
		gdouble v;
		d = (GFIFileData *) l->data;
		v = atof (d->version);
		if (v > pfbversion) {
			pfbversion = v;
			pfbdata = d;
		}
	}

	/* We have to read afm to get weight and italicangle */

	fi = NULL;
	f = fopen (afmdata->entry.name, "r");
	/* This shouldn't happen */
	if (!f) return;
	status = parseFile (f, &fi, P_G);
	fclose (f);
	/* This shouldn't happen! */
	if (status != AFM_ok) {
		if (fi) parseFileFree (fi);
		return;
	}

	/* Now we should have everything we need */

	new = g_new (GPFontEntryT1Alias, 1);
	new->t1.entry.type = GP_FONT_ENTRY_TYPE1_ALIAS;
	new->t1.entry.refcount = 1;
	new->t1.entry.face = NULL;
	new->t1.entry.name = g_strdup (afmdata->fontname);
	new->t1.entry.version = g_strdup (afmdata->version);
	new->t1.entry.familyname = g_strdup (t1a->t1.entry.familyname);
	new->t1.entry.speciesname = g_strdup (t1a->t1.entry.speciesname);
	new->t1.entry.psname = g_strdup (t1a->t1.entry.psname);
	new->t1.entry.weight = g_strdup (fi->gfi->weight);
	/* AFM */
	new->t1.afm.name = g_strdup (afmdata->entry.name);
	new->t1.afm.size = afmdata->entry.size;
	new->t1.afm.mtime = afmdata->entry.mtime;
	/* PFB */
	new->t1.pfb.name = g_strdup (pfbdata->entry.name);
	new->t1.pfb.size = pfbdata->entry.size;
	new->t1.pfb.mtime = pfbdata->entry.mtime;
	/* Misc */
	new->t1.Weight = t1a->t1.Weight;
	new->t1.ItalicAngle = fi->gfi->italicAngle;
	new->alias = g_strdup (pfbdata->psname);

#if 0
	g_print ("<font format=\"alias\" "
		 "name=\"%s\" familyname=\"%s\" speciesname=\"%s\" psname=\"%s\" "
		 "original=\"%s\" hideoriginal=\"true\"/>\n",
		 afmdata->fontname,
		 t1a->t1.entry.familyname,
		 t1a->t1.entry.speciesname,
		 t1a->t1.entry.psname,
		 pfbdata->fontname);
#endif

	/* Release AFM info */
	parseFileFree (fi);

	/* Register it */

	if (gfi_debug) fprintf (stderr, "Registered Type1 alias: %s\n", new->t1.entry.name);

	goodfont_list = g_slist_prepend (goodfont_list, new);
	g_hash_table_insert (goodfont_dict, new->t1.entry.name, new);
}
#endif

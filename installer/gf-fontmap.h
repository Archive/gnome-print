#ifndef __GF_FONTMAP_H__
#define __GF_FONTMAP_H__

/*
 * Process fontmapX files for installer
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include <libgnomeprint/gp-fontmap.h>

/* Known file types */

typedef enum {
	GFI_FILE_UNKNOWN,
	GFI_FILE_PFB,
	GFI_FILE_AFM,
	GFI_FILE_TTF
} GFIFileType;

typedef struct _GFIFileEntry GFIFileEntry;
typedef struct _GFIFileData GFIFileData;
typedef struct _GFIFontAlias GFIFontAlias;

/* Known data about any font file */

struct _GFIFileEntry {
	gchar * name;
	size_t size;
	time_t mtime;
};

struct _GFIFileData {
	GFIFileType type;
	GPFileEntry entry;
	guchar *fontname;
	guchar *familyname;
	guchar *speciesname;
	guchar *psname;
	guchar *version;
};

struct _GFIFontAlias {
	guchar *fontname;
	guchar *familyname;
	guchar *speciesname;
	guchar *psname;
	guchar *originalname;
	guchar *originalpsname;
	gboolean hideoriginal;
};

void gfi_read_fontmap (GSList **files, GSList **names, GSList **aliases);
void gfi_read_alias_file (GSList **aliases, const guchar *name);
gchar *gfi_get_species_name (const gchar *fullname, const gchar *familyname);

END_GNOME_DECLS

#endif

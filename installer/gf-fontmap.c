#define __GF_FONTMAP_C__

/*
 * Process fontmapX files for installer
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2000-2001 Ximian, Inc.
 *
 */

#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <tree.h>
#include <parser.h>
#include <xmlmemory.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-util.h>
#include "gf-fontmap.h"

static void gfi_read_fontmap_from_doc (GSList **files, GSList **names, GSList **aliases, xmlDocPtr map);
static void gfi_read_fontmap_from_tree_1_0 (GSList **files, GSList **names, GSList **aliases, xmlNodePtr root);
static void gfi_read_fontmap_from_node_1_0 (GSList **files, GSList **names, GSList **aliases, xmlNodePtr node);
static void gfi_read_fontmap_from_tree_2_0 (GSList **files, GSList **names, GSList **aliases, xmlNodePtr root);
static void gfi_read_fontmap_from_node_2_0 (GSList **files, GSList **names, GSList **aliases, xmlNodePtr node);
static void gfi_read_fontmap_from_tree_3_0 (GSList **files, GSList **names, GSList **aliases, xmlNodePtr root);
static void gfi_read_fontmap_from_node_3_0 (GSList **files, GSList **names, GSList **aliases, xmlNodePtr node);
static void gfi_check_font_file (GSList **files, GSList **names, xmlNodePtr node, const guchar *type);

static void gfi_read_aliases_from_tree (GSList **aliases, xmlNodePtr root);
static void gfi_read_alias_from_node (GSList **aliases, xmlNodePtr node);

void
gfi_read_fontmap (GSList **files, GSList **names, GSList **aliases)
{
	gchar *home, *name;
	xmlDocPtr map;
	struct stat s;

	map = NULL;
	home = getenv ("HOME");
	name = g_concat_dir_and_file (home, "/.gnome/fonts/fontmap");

	if ((stat (name, &s) == 0) && S_ISREG (s.st_mode) && (map = xmlParseFile (name))) {
		gfi_read_fontmap_from_doc (files, names, aliases, map);
		xmlFreeDoc (map);
	}
	g_free (name);

	name = g_concat_dir_and_file (DATADIR, "/fonts/fontmap3");
	if ((stat (name, &s) == 0) && S_ISREG (s.st_mode) && (map = xmlParseFile (name))) {
		gfi_read_fontmap_from_doc (files, names, aliases, map);
		xmlFreeDoc (map);
		g_free (name);
	} else {
		g_free (name);
		name = g_concat_dir_and_file (DATADIR, "/fonts/fontmap3");
		if ((stat (name, &s) == 0) && S_ISREG (s.st_mode) && (map = xmlParseFile (name))) {
			gfi_read_fontmap_from_doc (files, names, aliases, map);
			xmlFreeDoc (map);
			g_free (name);
		} else {
			g_free (name);
			name = g_concat_dir_and_file (DATADIR, "/fonts/fontmap3");
			if ((stat (name, &s) == 0) && S_ISREG (s.st_mode) && (map = xmlParseFile (name))) {
				gfi_read_fontmap_from_doc (files, names, aliases, map);
				xmlFreeDoc (map);
				g_free (name);
			}
		}
	}
}

void
gfi_read_alias_file (GSList **aliases, const guchar *name)
{
	struct stat st;
	xmlDocPtr doc;
	xmlNodePtr root;

	if (stat (name, &st)) return;
	if (!S_ISREG (st.st_mode)) return;

	doc = xmlParseFile (name);
	if (!doc) return;
	root = doc->xmlRootNode;
	if (root && !strcmp (root->name, "fontmap")) {
		gfi_read_aliases_from_tree (aliases, root);
	}
	xmlFreeDoc (doc);
}

gchar *
gfi_get_species_name (const gchar *fullname, const gchar *familyname)
{
	gchar * p;

	p = strstr (fullname, familyname);

	if (!p) return g_strdup ("Normal");

	p = p + strlen (familyname);

	while (*p && (*p < 'A')) p++;

	if (!*p) return g_strdup ("Normal");

	return g_strdup (p);
}

static void
gfi_read_fontmap_from_doc (GSList **files, GSList **names, GSList **aliases, xmlDocPtr map)
{
	xmlNodePtr root;
	xmlChar *xmlver;

	root = map->xmlRootNode;
	if (!root) return;

	xmlver = xmlGetProp (root, "version");
	if (!xmlver) {
		gfi_read_fontmap_from_tree_1_0 (files, names, aliases, root);
	} else if (!strcmp (xmlver, "2.0")) {
		xmlFree (xmlver);
		gfi_read_fontmap_from_tree_2_0 (files, names, aliases, root);
	} else if (!strcmp (xmlver, "3.0")) {
		xmlFree (xmlver);
		gfi_read_fontmap_from_tree_3_0 (files, names, aliases, root);
	}
}

static void
gfi_read_fontmap_from_tree_1_0 (GSList **files, GSList **names, GSList **aliases, xmlNodePtr root)
{
	xmlNodePtr child;

	for (child = root->xmlChildrenNode; child != NULL; child = child->next) {
		if (!strcmp (child->name, "font")) {
			xmlChar *xmlformat;
			/* We are font entry */
			xmlformat = xmlGetProp (child, "format");
			if (xmlformat && !strcmp (xmlformat, "type1")) {
				gfi_read_fontmap_from_node_1_0 (files, names, aliases, child);
			}
			if (xmlformat) xmlFree (xmlformat);
		}
	}
}

static void
gfi_read_fontmap_from_node_1_0 (GSList **files, GSList **names, GSList **aliases, xmlNodePtr node)
{
	xmlChar *xmlalias;

	xmlalias = xmlGetProp (node, "alias");
	if (xmlalias) {
		xmlChar *xmlname, *xmlfullname, *xmlfamilyname;
		xmlname = xmlGetProp (node, "name");
		xmlfullname = xmlGetProp (node, "fullname");
		xmlfamilyname = xmlGetProp (node, "familyname");
		if (xmlname && xmlfullname && xmlfamilyname) {
			GFIFontAlias *a;
			a = g_new0 (GFIFontAlias, 1);
			a->fontname = g_strdup (xmlfullname);
			a->familyname = g_strdup (xmlfamilyname);
			a->speciesname = gfi_get_species_name (xmlfullname, xmlfamilyname);
			a->psname = g_strdup (xmlname);
			a->originalpsname = g_strdup (xmlalias);
			a->hideoriginal = TRUE;
			*aliases = g_slist_prepend (*aliases, a);
		}
		if (xmlname) xmlFree (xmlname);
		if (xmlfullname) xmlFree (xmlfullname);
		if (xmlfamilyname) xmlFree (xmlfamilyname);
		xmlFree (xmlalias);
	} else {
		xmlChar *xmlglyphs, *xmlmetrics;
		xmlglyphs = xmlGetProp (node, "glyphs");
		if (xmlglyphs) {
			*names = g_slist_prepend (*names, g_strdup (xmlglyphs));
			xmlFree (xmlglyphs);
		}
		xmlmetrics = xmlGetProp (node, "metrics");
		if (xmlmetrics) {
			*names = g_slist_prepend (*names, g_strdup (xmlmetrics));
			xmlFree (xmlmetrics);
		}
	}
}

static void
gfi_read_fontmap_from_tree_2_0 (GSList **files, GSList **names, GSList **aliases, xmlNodePtr root)
{
	xmlNodePtr child;

	for (child = root->xmlChildrenNode; child != NULL; child = child->next) {
		if (!strcmp (child->name, "font")) {
			gfi_read_fontmap_from_node_2_0 (files, names, aliases, child);
		}
	}
}

static void
gfi_read_fontmap_from_node_2_0 (GSList **files, GSList **names, GSList **aliases, xmlNodePtr node)
{
	xmlChar *xmlformat;

	xmlformat = xmlGetProp (node, "format");
	if (xmlformat) {
		if (!strcmp (xmlformat, "type1")) {
			gfi_check_font_file (files, names, node, "afm");
			gfi_check_font_file (files, names, node, "pfb");
		} else if (!strcmp (xmlformat, "truetype")) {
			gfi_check_font_file (files, names, node, "ttf");
		} else if (!strcmp (xmlformat, "type1alias")) {
			xmlChar *xmlname, *xmlfamilyname, *xmlspeciesname, *xmlpsname, *xmlalias;
			gfi_check_font_file (files, names, node, "afm");
			xmlname = xmlGetProp (node, "name");
			xmlfamilyname = xmlGetProp (node, "familyname");
			xmlspeciesname = xmlGetProp (node, "speciesname");
			xmlpsname = xmlGetProp (node, "psname");
			xmlalias = xmlGetProp (node, "alias");
			if (xmlname && xmlfamilyname && xmlspeciesname && xmlpsname && xmlalias) {
				GFIFontAlias *a;
				a = g_new0 (GFIFontAlias, 1);
				a->fontname = g_strdup (xmlname);
				a->familyname = g_strdup (xmlfamilyname);
				a->speciesname = g_strdup (xmlspeciesname);
				a->psname = g_strdup (xmlpsname);
				a->originalpsname = g_strdup (xmlalias);
				a->hideoriginal = TRUE;
				*aliases = g_slist_prepend (*aliases, a);
			}
			if (xmlname) xmlFree (xmlname);
			if (xmlfamilyname) xmlFree (xmlfamilyname);
			if (xmlspeciesname) xmlFree (xmlspeciesname);
			if (xmlpsname) xmlFree (xmlpsname);
			if (xmlalias) xmlFree (xmlalias);
		}
		xmlFree (xmlformat);
	}
}

static void
gfi_read_fontmap_from_tree_3_0 (GSList **files, GSList **names, GSList **aliases, xmlNodePtr root)
{
	xmlNodePtr child;

	for (child = root->xmlChildrenNode; child != NULL; child = child->next) {
		if (!strcmp (child->name, "font")) {
			gfi_read_fontmap_from_node_3_0 (files, names, aliases, child);
		}
	}
}

static void
gfi_read_fontmap_from_node_3_0 (GSList **files, GSList **names, GSList **aliases, xmlNodePtr node)
{
	xmlChar *xmlformat;

	xmlformat = xmlGetProp (node, "format");
	if (xmlformat) {
		if (!strcmp (xmlformat, "type1")) {
			gfi_check_font_file (files, names, node, "afm");
			gfi_check_font_file (files, names, node, "pfb");
		} else if (!strcmp (xmlformat, "truetype")) {
			gfi_check_font_file (files, names, node, "ttf");
		} else if (!strcmp (xmlformat, "alias")) {
			xmlChar *xmlname, *xmlfamilyname, *xmlspeciesname, *xmlalias, *xmlhide;
			xmlname = xmlGetProp (node, "name");
			xmlfamilyname = xmlGetProp (node, "familyname");
			xmlspeciesname = xmlGetProp (node, "speciesname");
			xmlalias = xmlGetProp (node, "original");
			xmlhide = xmlGetProp (node, "hideoriginal");
			if (xmlname && xmlfamilyname && xmlspeciesname && xmlalias) {
				GFIFontAlias *a;
				a = g_new0 (GFIFontAlias, 1);
				a->fontname = g_strdup (xmlname);
				a->familyname = g_strdup (xmlfamilyname);
				a->speciesname = g_strdup (xmlspeciesname);
				a->originalname = g_strdup (xmlalias);
				a->hideoriginal = xmlhide && (!strcasecmp (xmlhide, "true") || !strcmp (xmlhide, "yes"));
				*aliases = g_slist_prepend (*aliases, a);
			}
			if (xmlname) xmlFree (xmlname);
			if (xmlfamilyname) xmlFree (xmlfamilyname);
			if (xmlspeciesname) xmlFree (xmlspeciesname);
			if (xmlalias) xmlFree (xmlalias);
			if (xmlhide) xmlFree (xmlhide);
		}
		xmlFree (xmlformat);
	}
}

static void
gfi_check_font_file (GSList **files, GSList **names, xmlNodePtr node, const guchar *type)
{
	xmlNodePtr child;
	xmlChar *xmlpath;
	xmlChar *xmlsize, *xmlmtime;
	struct stat s;

	for (child = node->xmlChildrenNode; child != NULL; child = child->next) {
		if (!strcmp (child->name, "file")) {
			xmlChar *xmlformat;
			xmlformat = xmlGetProp (child, "type");
			if (xmlformat && (!strcmp (xmlformat, type))) {
				xmlFree (xmlformat);
				break;
			}
			xmlFree (xmlformat);
		}
	}

	if (!child) return;
				
	xmlpath = xmlGetProp (child, "path");
	if (!xmlpath) return;
	if (stat (xmlpath, &s) || !S_ISREG (s.st_mode)) {
		xmlFree (xmlpath);
		return;
	}

	xmlsize = xmlGetProp (child, "size");
	xmlmtime = xmlGetProp (child, "mtime");
	if (xmlsize && xmlmtime) {
		gint size, mtime;
		size = atoi (xmlsize);
		mtime = atoi (xmlmtime);
		if ((s.st_size == size) && (s.st_mtime == mtime)) {
			xmlChar *xmlfontname, *xmlfamilyname, *xmlspeciesname, *xmlpsname, *xmlversion;
			xmlfontname = xmlGetProp (node, "name");
			xmlfamilyname = xmlGetProp (node, "familyname");
			xmlspeciesname = xmlGetProp (node, "speciesname");
			xmlpsname = xmlGetProp (node, "psname");
			xmlversion = xmlGetProp (node, "version");
			if (xmlfontname && xmlfamilyname && xmlspeciesname && xmlpsname && xmlversion) {
				GFIFileData *fd;
				fd = g_new0 (GFIFileData, 1);
				if (!strcmp (type, "afm")) {
					fd->type = GFI_FILE_AFM;
				} else if (!strcmp (type, "pfb")) {
					fd->type = GFI_FILE_PFB;
				} else if (!strcmp (type, "ttf")) {
					fd->type = GFI_FILE_TTF;
				} else {
					fd->type = GFI_FILE_UNKNOWN;
				}
				fd->entry.name = g_strdup (xmlpath);
				fd->entry.size = size;
				fd->entry.mtime = mtime;
				fd->fontname = g_strdup (xmlfontname);
				fd->familyname = g_strdup (xmlfamilyname);
				fd->speciesname = g_strdup (xmlspeciesname);
				fd->psname = g_strdup (xmlpsname);
				fd->version = g_strdup (xmlversion);
				*files = g_slist_prepend (*files, fd);
				xmlFree (xmlpath);
				xmlFree (xmlsize);
				xmlFree (xmlmtime);
				xmlFree (xmlfontname);
				xmlFree (xmlfamilyname);
				xmlFree (xmlspeciesname);
				xmlFree (xmlpsname);
				xmlFree (xmlversion);
				return;
			}
			if (xmlfontname) xmlFree (xmlfontname);
			if (xmlfamilyname) xmlFree (xmlfamilyname);
			if (xmlspeciesname) xmlFree (xmlspeciesname);
			if (xmlpsname) xmlFree (xmlpsname);
			if (xmlversion) xmlFree (xmlversion);
		}
	}
	if (xmlsize) xmlFree (xmlsize);
	if (xmlmtime) xmlFree (xmlmtime);

	*names = g_slist_prepend (*names, g_strdup (xmlpath));

	if (xmlpath) xmlFree (xmlpath);
}

static void
gfi_read_aliases_from_tree (GSList **aliases, xmlNodePtr root)
{
	xmlNodePtr child;
	xmlChar *xmlver;

	xmlver = xmlGetProp (root, "version");
	if (!xmlver) return;
	if (strcmp (xmlver, "3.0")) {
		xmlFree (xmlver);
		return;
	}

	for (child = root->xmlChildrenNode; child != NULL; child = child->next) {
		if (!strcmp (child->name, "font")) {
			gfi_read_alias_from_node (aliases, child);
		}
	}
}

static void
gfi_read_alias_from_node (GSList **aliases, xmlNodePtr node)
{
	xmlChar *type;
	xmlChar *name, *familyname, *speciesname, *original, *hideoriginal;

	type = xmlGetProp (node, "format");
	if (!type) return;
	if (strcmp (type, "alias")) {
		xmlFree (type);
		return;
	}
	xmlFree (type);

	name = xmlGetProp (node, "name");
	familyname = xmlGetProp (node, "familyname");
	speciesname = xmlGetProp (node, "speciesname");
	original = xmlGetProp (node, "original");
	hideoriginal = xmlGetProp (node, "hideoriginal");

	if (name && familyname && speciesname && original) {
		GFIFontAlias *a;
		a = g_new0 (GFIFontAlias, 1);
		a->fontname = g_strdup (name);
		a->familyname = g_strdup (familyname);
		a->speciesname = g_strdup (speciesname);
		a->originalname = g_strdup (original);
		a->hideoriginal = FALSE;
		if (!strcasecmp (hideoriginal, "true")) a->hideoriginal = TRUE;
		if (!strcasecmp (hideoriginal, "yes")) a->hideoriginal = TRUE;
		*aliases = g_slist_prepend (*aliases, a);
	}

	if (name) xmlFree (name);
	if (familyname) xmlFree (familyname);
	if (speciesname) xmlFree (speciesname);
	if (original) xmlFree (original);
	if (hideoriginal) xmlFree (hideoriginal);
}

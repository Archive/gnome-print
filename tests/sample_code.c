/*
  This is sample code for gnome-print.

  THIS FILE WILL NOT COMPILE !!
  because I've not comited the GnomePrintState stuff yet.
  Non the less I think it is usefull for illustrating the use
  of gnome-print.
*/

#include "config.h"
#include <gnome.h>
#include <glade/glade.h>
#include <libgnomeprint/gnome-print.h>
#if 0
#include <libgnomeprint/gnome-print-state.h>
#endif
#include <libgnomeprint/gnome-print-master.h>
#include <libgnomeprint/gnome-print-master-preview.h>
#include <libgpa/gpa-printer.h>

typedef struct _MyAppDocument MyAppDocument;

struct _MyAppDocument
{
#if 0
	GnomePrintState *gps;
#endif	
	gchar *name;
	GpaPrinter *gpa_printer;
};
    

/**
 * my_app_print:
 * @pc: 
 * 
 * This is the function inside the application that given a print context
 * does the actual drawing.
 * 
 * Return Value: 
 **/
static void
my_app_print_to_context (GnomePrintContext *pc)
{
	gnome_print_moveto (pc, 100, 100);
	gnome_print_lineto (pc, 200, 200);
	gnome_print_stroke (pc);

	gnome_print_showpage (pc);
}

GpaPrinter * gpa_printer_new (void);


/**
 * my_app_document_new:
 * @void: 
 * 
 * Each document has a GnomePrintState that holds the selected printer the selected
 * print orientation, etc. If you app is "document less" (like a web browser) use a
 * GnomePrintState app wise. The use of GnomePrintState is optional, but if you don't
 * use it, the dialog will contain the default settings each time it is used. 
 * 
 * Return Value: 
 **/
static MyAppDocument *
my_app_document_new (void)
{
	MyAppDocument *doc;
	
	doc = g_new0 (MyAppDocument, 1);
	doc->name = g_strdup ("My Cool document !\n");
#if 0
	g_print ("Getting a GnomePrintState\n");
	doc->gps = gnome_print_state_new ();
#endif
	doc->gpa_printer = gpa_printer_new ();

	return doc;
}

/**
 * my_app_get_active_document:
 * @void: 
 * 
 *
 * 
 * Return Value: The currently used document
 **/
static MyAppDocument *
my_app_get_current_document (void)
{
	static MyAppDocument *doc = NULL;

	if (doc == NULL)
		doc = my_app_document_new ();
	
	return doc;
}


/**
 * my_app_print:
 * @doc: 
 * @print_preview: 
 * 
 * Do the actual printing
 **/
static void
my_app_print (MyAppDocument *doc, gboolean print_preview)
{
	GnomePrintContext *print_context;
	GnomePrintMaster *print_master;

	print_master = gnome_print_master_new ();
	g_return_if_fail (GNOME_IS_PRINT_MASTER (print_master));
	print_context = gnome_print_master_get_context (print_master);
	
	my_app_print_to_context (print_context);

	gnome_print_master_close (print_master);
	
	if (print_preview) {
		GnomePrintMasterPreview *preview;
		preview = gnome_print_master_preview_new (print_master, _("My App Print Preview"));
		gtk_widget_show (GTK_WIDGET (preview));
	} else {
		gnome_print_master_print (print_master);
	}

	gtk_object_unref (GTK_OBJECT (print_master));
}
	
/**
 * my_app_print_cb:
 * @not_used: 
 * @ditto: 
 * 
 * Callback for the menu option, toolbar or whatever 
 **/
static void
my_app_print_cb (GtkWidget *not_used, gpointer ditto)
{
	MyAppDocument *doc;

	doc = my_app_get_current_document ();

	if (!doc)
		return; /* g_return_if_you always have a doc */

	my_app_print (doc, FALSE);	

}

/**
 * my_app_print_preview_cb:
 * @not_used: 
 * @ditto: 
 * 
 * Callback for the menu option, toolbar or whatever 
 **/
static void
my_app_print_preview_cb (GtkWidget *not_used, gpointer not_ditto)
{
	MyAppDocument *doc;

	doc = my_app_get_current_document ();

	if (!doc)
		return; /* g_return_if_you always have a doc */

	my_app_print (doc, TRUE);
}

/**
 * my_app_quit:
 * @not_used: 
 * @ditto: 
 * 
 * Quit
 **/
static void
my_app_quit (GtkWidget *not_used, gpointer ditto)
{
	gtk_main_quit ();
}

/**
 * my_app_quit_event:
 * @a: 
 * @b: 
 * @c: 
 * 
 * Quit by event.
 **/
static void
my_app_quit_event (gpointer a, gpointer b, gpointer c)
{
	my_app_quit (NULL, c);
}



/**
 * my_app_new:
 * @void: 
 * 
 * Creates the MyApp GnomeApp object. This is the toplevel window
 * 
 * Return Value: 
 **/
static GnomeApp *
my_app_new (void)
{
	GladeXML *gui;
	GnomeApp *app;
	GtkWidget *print_button;
	GtkWidget *print_preview_button;
	GtkWidget *exit_menu_item;

	gui = glade_xml_new ("sample_code.glade", NULL);

	g_return_val_if_fail (gui != NULL, NULL);

	app = GNOME_APP (glade_xml_get_widget (gui, "main_window"));
	print_button         = glade_xml_get_widget (gui, "print_button");
	print_preview_button = glade_xml_get_widget (gui, "print_preview_button");
	exit_menu_item       = glade_xml_get_widget (gui, "exit_menu_item");

	g_return_val_if_fail (app != NULL, NULL);
	g_return_val_if_fail (print_button != NULL, NULL);
	g_return_val_if_fail (print_preview_button != NULL, NULL);
	g_return_val_if_fail (exit_menu_item != NULL, NULL);

	/* Hook the buttons */
	gtk_signal_connect (GTK_OBJECT (print_button), "clicked",
					GTK_SIGNAL_FUNC (my_app_print_cb), NULL);
	gtk_signal_connect (GTK_OBJECT (print_preview_button), "clicked",
					GTK_SIGNAL_FUNC (my_app_print_preview_cb), NULL);

	/* Hook app detroy signals */
	gtk_signal_connect (GTK_OBJECT (exit_menu_item), "activate",
					GTK_SIGNAL_FUNC (my_app_quit), NULL);
	gtk_signal_connect (GTK_OBJECT (app), "delete_event",
					GTK_SIGNAL_FUNC (my_app_quit_event), NULL);

	gtk_widget_show_all (GTK_WIDGET (app));
	
	return app;
}

static const struct poptOption options[] =
{
	{NULL, '\0', 0, NULL, 0}
};

/**
 * main:
 * @argc: 
 * @argv: 
 * 
 * The main function.
 * 
 * Return Value: 
 **/
int
main (int argc, char ** argv)
{
	poptContext popt_context;
	GnomeApp *app = NULL;

	bindtextdomain(PACKAGE, GNOMELOCALEDIR);
	textdomain(PACKAGE);
	
	gnome_init_with_popt_table ("sample-print-code", VERSION, argc,
						   argv, options, 0, &popt_context);
	poptFreeContext(popt_context);

	glade_gnome_init ();

	app = my_app_new ();

	g_return_val_if_fail (app != NULL, -1);

	gtk_main ();

	return 0;
}


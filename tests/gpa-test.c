#define __GPA_TEST_C__

/*
 * gpa-test
 *
 * Utility to test the behaviour of GPA configuration tree
 *
 * Licensed under GNU GPL
 *
 * Authors:
 *  Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 */

#include <gtk/gtkmain.h>
#include <gtk/gtksignal.h>
#include <gtk/gtktree.h>
#include <gtk/gtktreeitem.h>
#include <gtk/gtkscrolledwindow.h>
#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-init.h>
#include <libgnomeprint/gnome-print.h>
#include <libgnomeprint/gpa-node.h>
#include <libgnomeprint/gnome-printer-dialog.h>

static void
tree_item_from_node (GtkTree *tree, GPANode *node)
{
	GtkWidget *item;
	guchar *id;
	GPANode *child;

	id = gpa_node_id (node);
	if (!id) id = g_strdup ("UNNAMED");
	g_print ("Id: %s\n", id);
	item = gtk_tree_item_new_with_label (id);
	g_free (id);
	gtk_tree_append (tree, item);

	child = gpa_node_get_child (node, NULL);
	if (child) {
		GtkWidget *subtree;
		subtree = gtk_tree_new ();
		gtk_tree_item_set_subtree (GTK_TREE_ITEM (item), subtree);
		while (child) {
			tree_item_from_node (GTK_TREE (subtree), child);
			child = gpa_node_get_child (node, child);
		}
		gtk_widget_show (subtree);
	}

	gtk_widget_show (item);
}

static void
config_modified (GPANode *node, GtkTree *tree)
{
	GList *children;

	children = gtk_container_children (GTK_CONTAINER (tree));
	gtk_tree_remove_items (tree, children);
	g_list_free (children);

	tree_item_from_node (tree, node);
}

int
main (int argc, char *argv[])
{
	GtkWidget *w, *pw, *sw, *t;
	GPANode *config;

	gnome_init ("gpa-test", "0.1", argc, argv);

	config = gpa_defaults ();

	w = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (w), "gpa-test-1");
	gtk_signal_connect (GTK_OBJECT (w), "delete_event", GTK_SIGNAL_FUNC (gtk_main_quit), NULL);
	pw = gnome_printer_widget_new_from_node (config);
	gtk_container_add (GTK_CONTAINER (w), pw);
	gtk_widget_show (pw);
	gtk_widget_show (w);

	w = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (w), "gpa-test-2");
	gtk_signal_connect (GTK_OBJECT (w), "delete_event", GTK_SIGNAL_FUNC (gtk_main_quit), NULL);
	pw = gnome_printer_widget_new_from_node (config);
	gtk_container_add (GTK_CONTAINER (w), pw);
	gtk_widget_show (pw);
	gtk_widget_show (w);

	w = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (w), "gpa-test-tree");
	gtk_signal_connect (GTK_OBJECT (w), "delete_event", GTK_SIGNAL_FUNC (gtk_main_quit), NULL);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_container_add (GTK_CONTAINER (w), sw);
	gtk_widget_show (sw);

	t = gtk_tree_new ();
	gtk_signal_connect (GTK_OBJECT (config), "modified", GTK_SIGNAL_FUNC (config_modified), t);
	gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (sw), t);
	gtk_widget_show (t);

	config_modified (config, GTK_TREE (t));

	gtk_widget_show (w);

	gtk_main ();

	return 0;
}

 

[Fax G3]
name=Fax driver (via gfax)
output=command,gfax %s
comment=G3 fax driver, invoking gfax as transmission frontend
location=
mime-type=
driver=gnome-print-fax

[Fax G3, file]
name=Fax driver, save to file
output=file,output.g3
comment=G3 fax driver
location=
mime-type=
driver=gnome-print-fax

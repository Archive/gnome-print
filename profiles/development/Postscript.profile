[Generic Postscript]
name=Generic Postscript (Legacy)
name[pl]=Standardowy PostScript (starsza wersja)
output=command,lpr
comment=
location=
mime-type=application/postscript
driver=gnome-print-ps

[Generic Postscript, file]
name=Generic Postscript (Legacy), print to file
name[pl]=Standardowy PostScript (starsza wersja), zapis do pliku
output=file,output.ps
comment=
location=
mime-type=application/postscript
driver=gnome-print-ps

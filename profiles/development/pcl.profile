[PCL Raster]
name=PCL Raster
name[pl]=PCL, grafika rastrowa
output=command,lpr
comment=DEVELOPMENT
comment[pl]=WERSJA ROZWOJOWA
location=
mime-type=application/pcl
driver=gnome-print-pclr

[PCL Raster, file]
name=PCL Raster, print to file
name[pl]=PCL, grafika rastrowa; zapis do pliku
output=file,output.pcl
comment=DEVELOPMENT
comment[pl]=WERSJA ROZWOJOWA
location=
mime-type=application/pcl
driver=gnome-print-pclr

[PCL Vectors]
name=PCL Vectors
name[pl]=PCL, grafika wektorowa
output=command,lpr
comment=DEVELOPMENT
comment[pl]=WERSJA ROZWOJOWA
location=
mime-type=application/pcl
driver=gnome-print-pclv

[PCL Vectors, file]
name=PCL Vectors, print to file
name[pl]=PCL, grafika wektorowa; zapis do pliku
output=file,output.pcl
comment=DEVELOPMENT
comment[pl]=WERSJA ROZWOJOWA
location=
mime-type=application/pcl
driver=gnome-print-pclv

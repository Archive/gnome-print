[Generic Postscript RGB]
name=Generic Postscript without alpha
name[pl]=Standardowy PostScript bez obs�ugi kana�u alfa
output=command,lpr
comment=
location=
mime-type=application/postscript
driver=gnome-print-ps-rgb

[Generic Postscript RGB, file]
name=Generic Postscript without alpha, print to file
name[pl]=Standardowy PostScript bez obs�ugi kana�u alfa, zapis do pliku
output=file,output.ps
comment=
location=
mime-type=application/postscript
driver=gnome-print-ps-rgb

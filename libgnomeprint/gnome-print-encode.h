#ifndef __GNOME_PRINT_ENCODE_H__
#define __GNOME_PRINT_ENCODE_H__

/*
 *  Copyright (C) 2000-2001 Ximian Inc. and authors
 *
 *  Authors:
 *    Chema Celorio <chema@celorio.com>
 *
 *  Various encoding methods for PS and PDF drivers
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

/* Empty. */
/* I love that header (Lauris) */

END_GNOME_DECLS

#endif /* ! _GNOME_PRINT_ENCODE_H */

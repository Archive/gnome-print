#define __GNOME_PRINT_PREVIEW_C__

/*
 *  Copyright (C) 1999-2001 Ximian Inc. and authors
 *
 *  Authors:
 *    Miguel de Icaza (miguel@gnu.org)
 *    Lauris Kaplinski <lauris@ximian.com>
 *
 *  Preview driver
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <gtk/gtk.h>
#include <string.h>
#include <math.h>

#include <libart_lgpl/art_misc.h>
#include <libart_lgpl/art_pixbuf.h>
#include <libgnomeui/gnome-canvas-image.h>
#include <libgnomeprint/gnome-print-private.h>
#include <libgnomeprint/gnome-print-preview.h>
#include <libgnomeprint/gnome-print-preview-private.h>
#include <libgnomeprint/gnome-canvas-bpath.h>
#include <libgnomeprint/gnome-canvas-hacktext.h>
#include <libgnomeprint/gnome-canvas-clipgroup.h>
#include <libgnomeprint/gnome-font.h>
#include <libgnome/gnome-paper.h>

#include <libgnomeprint/gp-gc.h>

struct _GnomePrintPreviewPrivate {
	GPGC * gc;

	/* Current page displayed */
	int top_page;
	int current_page;

	/* The root group, with a translation setup */
	GnomeCanvasItem *root;

	/* The current page */
	GnomeCanvasItem *page;
};

static GnomePrintContextClass *print_preview_parent_class;

static int
gpp_stroke (GnomePrintContext *pc, const ArtBpath *bpath)
{
	GnomePrintPreview *pp = GNOME_PRINT_PREVIEW (pc);
	GnomePrintPreviewPrivate *priv = pp->priv;
	GnomeCanvasGroup *group;
	GnomeCanvasItem *item;
	GPPath *path;

	/* fixme: currentpath invariants */

	group = (GnomeCanvasGroup *) gp_gc_get_data (priv->gc);
	g_assert (group != NULL);
	g_assert (GNOME_IS_CANVAS_GROUP (group));

	path = gp_path_new_from_foreign_bpath (bpath);

	/* Fixme: Can we assume that linewidth == width_units? */
	/* Probably yes, as object->page ctm == identity */

	item = gnome_canvas_item_new (group,
		gnome_canvas_bpath_get_type (),
		"bpath",	path,
		"width_units",	gp_gc_get_linewidth (pc->gc),
		"cap_style",	gp_gc_get_linecap (pc->gc),
		"join_style",	gp_gc_get_linejoin (pc->gc),
		"outline_color_rgba", gp_gc_get_rgba (pc->gc),
		"miterlimit",	gp_gc_get_miterlimit (pc->gc),
		"dash",		gp_gc_get_dash (pc->gc),
		NULL);

	gp_path_unref (path);

	return 1;
}

static int
gpp_fill (GnomePrintContext *pc, const ArtBpath *bpath, ArtWindRule rule)
{
	GnomePrintPreview *pp;
	GnomePrintPreviewPrivate * priv;
	GnomeCanvasGroup *group;
	GnomeCanvasItem *item;
	GPPath *path;
	
	pp = GNOME_PRINT_PREVIEW (pc);
	priv = pp->priv;

	group = (GnomeCanvasGroup *) gp_gc_get_data (priv->gc);
	g_assert (group != NULL);
	g_assert (GNOME_IS_CANVAS_GROUP (group));

	path = gp_path_new_from_foreign_bpath (bpath);

	item = gnome_canvas_item_new (group,
		gnome_canvas_bpath_get_type (),
		"bpath", path,
		"outline_color", NULL,
		"fill_color_rgba", gp_gc_get_rgba (pc->gc),
		"wind", rule,
		NULL);

	gp_path_unref (path);

	return 1;
}

static int
gpp_clip (GnomePrintContext *pc, const ArtBpath *bpath, ArtWindRule rule)
{
	GnomePrintPreview *pp;
	GnomePrintPreviewPrivate * priv;
	GnomeCanvasGroup * group;
	GnomeCanvasItem * clip;
	GPPath *path;

	pp = GNOME_PRINT_PREVIEW (pc);
	priv = pp->priv;

	group = (GnomeCanvasGroup *) gp_gc_get_data (priv->gc);

	path = gp_path_new_from_foreign_bpath (bpath);

	clip = gnome_canvas_item_new (group,
		gnome_canvas_clipgroup_get_type (),
		"path", path,
		"wind", rule,
		NULL);

	gp_path_unref (path);

	return 1;
}

static int
gpp_image (GnomePrintContext *pc, const gdouble *affine, const char *px, int w, int h, int rowstride, int ch)
{
	GnomePrintPreview *pp = GNOME_PRINT_PREVIEW (pc);
	GnomeCanvasGroup * group;
	GnomeCanvasItem *item;
	ArtPixBuf *pixbuf;
	int size, bpp;
	void *dup;
	
	/*
	 * We do convert gray scale images to RGB
	 */

	if (ch == 1) {
		bpp = 3;
	} else {
		bpp = ch;
	}
	
	size = w * h * bpp;
	dup = art_alloc (size);
	if (!dup) return -1;

	if (ch == 3) {
		memcpy (dup, px, size);
		pixbuf = art_pixbuf_new_rgb (dup, w, h, rowstride);
	} else if (ch == 4) {
		memcpy (dup, px, size);
		pixbuf = art_pixbuf_new_rgba (dup, w, h, rowstride);
	} else if (ch == 1) {
		const char *source;
		char *target;
		int  ix, iy;

		source = px;
		target = dup;

		for (iy = 0; iy < h; iy++){
			for (ix = 0; ix < w; ix++){
				*target++ = *source;
				*target++ = *source;
				*target++ = *source;
				source++;
			}
		}
		pixbuf = art_pixbuf_new_rgb (dup, w, h, rowstride * 3);
	} else
		return -1;

	group = (GnomeCanvasGroup *) gp_gc_get_data (pp->priv->gc);

	item = gnome_canvas_item_new (group,
		gnome_canvas_image_get_type (),
		"pixbuf", pixbuf,
		"x",      0.0,
		"y",      0.0,
		"width",  (gdouble) w,
		"height", (gdouble) h,
		"anchor", GTK_ANCHOR_NW,
		NULL);


	/* Apply the transformation for the image */
	{
		double transform[6];
		double flip[6];
		flip[0] = 1.0 / w;
		flip[1] = 0.0;
		flip[2] = 0.0;
		flip[3] = -1.0 / h;
		flip[4] = 0.0;
		flip[5] = 1.0;
#if 0
		art_affine_scale (flip, 1.0, -1.0);
#endif
		art_affine_multiply (transform, flip, affine);
		gnome_canvas_item_affine_absolute (item, transform);
	}
	
	return 1;
}

static int
gpp_showpage (GnomePrintContext *pc)
{
	return 0;
}

static int
gpp_beginpage (GnomePrintContext *pc, const char *name_of_this_page)
{
	return 0;
}

static int
gpp_glyphlist (GnomePrintContext *pc, const gdouble *affine, GnomeGlyphList * glyphlist)
{
	GnomePrintPreview *pp = GNOME_PRINT_PREVIEW (pc);
	GnomeCanvasGroup *group;
	GnomeCanvasItem *item;
	double transform[6], a[6];


	/*
	 * The X and Y positions were computed to be the base
	 * with the translation already done for the new
	 * Postscript->Canvas translation
	 */
	
	memcpy (transform, gp_gc_get_ctm (pc->gc), sizeof (transform));
	art_affine_scale (a, 1.0, -1.0);
	art_affine_multiply (transform, a, affine);

	group = (GnomeCanvasGroup *) gp_gc_get_data (pp->priv->gc);

	item = gnome_canvas_item_new (group,
		gnome_canvas_hacktext_get_type (),
		"x", 0.0,
		"y", 0.0,
		"glyphlist", glyphlist,
		NULL);

	gnome_canvas_item_affine_absolute (item, transform);

	return 0;
}

static int
gpp_close (GnomePrintContext *pc)
{
	return 0;
}

static void
gpp_destroy (GtkObject *object)
{
	GnomePrintPreview *pp = GNOME_PRINT_PREVIEW (object);
	GnomePrintPreviewPrivate *priv = pp->priv;

	gp_gc_unref (priv->gc);

	if (pp->canvas)
		gtk_object_unref (GTK_OBJECT (pp->canvas));

	if (priv->page)
		gtk_object_destroy (GTK_OBJECT (priv->page));

	if (priv->root)
		gtk_object_destroy (GTK_OBJECT (priv->root));
	
	g_free (priv);

	GTK_OBJECT_CLASS (print_preview_parent_class)->destroy (object);
}

static void
gpp_class_init (GnomePrintPreviewClass *class)
{
	GtkObjectClass *object_class = (GtkObjectClass *) class;
	GnomePrintContextClass *pc_class = (GnomePrintContextClass *) class;

	print_preview_parent_class = gtk_type_class (gnome_print_context_get_type ());

	object_class->destroy = gpp_destroy;
	
	pc_class->beginpage = gpp_beginpage;
	pc_class->showpage = gpp_showpage;

	pc_class->clip = gpp_clip;
	pc_class->fill = gpp_fill;
	pc_class->stroke = gpp_stroke;

	pc_class->image = gpp_image;

	pc_class->glyphlist = gpp_glyphlist;
	
	pc_class->close = gpp_close;
}

static void
gpp_init (GnomePrintPreview *preview)
{
	GnomePrintPreviewPrivate *priv;
	
	priv = preview->priv = g_new0 (GnomePrintPreviewPrivate, 1);

	priv->gc = gp_gc_new ();
}

static void
clear_val (GtkObject *object, void **val)
{
	*val = NULL;
}

/**
 * gnome_print_preview_construct:
 * @preview: the #GnomePrintPreview object to construct
 * @canvas: Canvas on which the preview will render
 * @paper_info: a GnomePaper information
 *
 * Constructs the @preview object.
 */
void
gnome_print_preview_construct (GnomePrintPreview *preview,
			       GnomeCanvas *canvas,
			       const GnomePaper *paper_info)
{
	GnomeCanvasGroup * group;
	double page2root[6];

	g_return_if_fail (preview != NULL);
	g_return_if_fail (GNOME_IS_PRINT_PREVIEW (preview));
	g_return_if_fail (canvas != NULL);
	g_return_if_fail (GNOME_IS_CANVAS (canvas));
	g_return_if_fail (paper_info != NULL);

	gtk_object_ref (GTK_OBJECT (canvas));
	preview->canvas = canvas;

	if (getenv ("GNOME_PRINT_DEBUG_WIDE"))
		gnome_canvas_set_scroll_region (
			canvas,	-900, -900, 900, 900);
	else
		gnome_canvas_set_scroll_region (
			canvas, 0, 0,
			gnome_paper_pswidth (paper_info),
			gnome_paper_psheight (paper_info));

	preview->priv->root = gnome_canvas_item_new (
		gnome_canvas_root (preview->canvas),
		gnome_canvas_group_get_type (),
		"x", 0.0,
		"y", 0.0,
		NULL);

	preview->priv->page = gnome_canvas_item_new (
		gnome_canvas_root (preview->canvas),
		gnome_canvas_group_get_type (),
		"x", 0.0,
		"y", 0.0,
		NULL);

	gtk_signal_connect (GTK_OBJECT (preview->priv->page), "destroy",
			    GTK_SIGNAL_FUNC (clear_val), &preview->priv->page);
	gtk_signal_connect (GTK_OBJECT (preview->priv->root), "destroy",
			    GTK_SIGNAL_FUNC (clear_val), &preview->priv->root);
	/*
	 * Setup base group
	 */

	group = GNOME_CANVAS_GROUP (preview->priv->page);

	gp_gc_set_data (preview->priv->gc, group);

	/*
	 * Setup the affines
	 */

	art_affine_scale (page2root, 1.0, -1.0);
	page2root[5] = gnome_paper_psheight (paper_info);

	gnome_canvas_item_affine_absolute (preview->priv->page, page2root);
}

/**
 * gnome_print_preview_new:
 * @canvas: Canvas on which we display the print preview
 * @paper_size: A valid name for a paper size
 *
 * Creates a new PrintPreview object that use the @canvas GnomeCanvas 
 * as its rendering backend.
 *
 * Returns: A GnomePrintContext suitable for using with the GNOME print API.
 */
GnomePrintContext *
gnome_print_preview_new (GnomeCanvas *canvas, const char *paper_size)
{
	GnomePrintPreview *preview;
	const GnomePaper *paper_info;
	
	g_return_val_if_fail (canvas != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_CANVAS (canvas), NULL);
	g_return_val_if_fail (paper_size != NULL, NULL);

	paper_info = gnome_paper_with_name (paper_size);
	if (paper_info == NULL)
		g_return_val_if_fail (FALSE, NULL);

	preview = gtk_type_new (gnome_print_preview_get_type ());
	if (preview == NULL)
		return NULL;

	gnome_print_preview_construct (preview, canvas, paper_info);
	return GNOME_PRINT_CONTEXT (preview);
}

/**
 * gnome_print_preview_get_type:
 *
 * GTK type identification routine for #GnomePrintPreview
 *
 * Returns: The Gtk type for the #GnomePrintPreview object
 */
GtkType
gnome_print_preview_get_type (void)
{
	static GtkType type = 0;

	if (!type){
		GtkTypeInfo info = {
			"GnomePrintPreview",
			sizeof (GnomePrintPreview),
			sizeof (GnomePrintPreviewClass),
			(GtkClassInitFunc) gpp_class_init,
			(GtkObjectInitFunc) gpp_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};
		
		type = gtk_type_unique (gnome_print_context_get_type (), &info);
	}
	return type;
}


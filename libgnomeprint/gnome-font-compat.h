#ifndef __GNOME_FONT_COMPAT_H__
#define __GNOME_FONT_COMPAT_H__

/*
 *  Copyright (C) 2000-2001 Ximian Inc. and authors
 *
 *  Authors:
 *    Jody Goldberg <jody@ximian.com>
 *    Miguel de Icaza <miguel@ximian.com>
 *    Lauris Kaplinski <lauris@ximian.com>
 *    Christopher James Lahey <clahey@ximian.com>
 *    Michael Meeks <michael@ximian.com>
 *    Morten Welinder <terra@diku.dk>
 *
 *  GnomeFont - basic user visible handle to scaled typeface
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include <libgnomeprint/gnome-font-face.h>

/* Various backward compatibility typedefs */
typedef struct _GnomeFontFacePrivate GnomeFontFacePrivate;
typedef struct _GnomeFontFace GnomeFontUnsized;
typedef struct _GnomeFontFace GnomeFontMap;

/*
 * These numbers are very loosely adapted from the Univers numbering
 * convention. I've had to insert some to accomodate all the
 * distinctions in the Adobe font catalog. Also, I've offset the
 * numbering so that the default is 0.
 */

typedef enum {
	GNOME_FONT_LIGHTEST = -3,
	GNOME_FONT_EXTRA_LIGHT = -3,
	GNOME_FONT_THIN = -2,
	GNOME_FONT_LIGHT = -1,
	GNOME_FONT_BOOK = 0, /* also known as "regular" or "roman" */
	/* gap here so that if book is missing, light wins over medium */
	GNOME_FONT_MEDIUM = 2,
	GNOME_FONT_SEMI = 3, /* also known as "demi" */
	GNOME_FONT_BOLD = 4,
	/* gap here so that if bold is missing, semi wins over heavy */
	GNOME_FONT_HEAVY = 6,
	GNOME_FONT_EXTRABOLD = 7,
	GNOME_FONT_BLACK = 8,
	GNOME_FONT_EXTRABLACK = 9, /* also known as "ultra" */
	GNOME_FONT_HEAVIEST = 9
} GnomeFontWeight;

#define GNOME_FONT_NUM_WEIGHTS (GNOME_FONT_HEAVIEST + 1 - GNOME_FONT_LIGHTEST)

#define gnome_font_face_ref(f) gtk_object_ref (GTK_OBJECT (f))
#define gnome_font_face_unref(f) gtk_object_unref (GTK_OBJECT (f))

/*
 * Various backward-compatibility and conveninence methods
 *
 * NB! Those usually do not scale for international fonts, so use with
 * caution.
 */

gdouble gnome_font_face_get_ascender (const GnomeFontFace * face);
gdouble gnome_font_face_get_descender (const GnomeFontFace * face);
gdouble gnome_font_face_get_underline_position (const GnomeFontFace * face);
gdouble gnome_font_face_get_underline_thickness (const GnomeFontFace * face);

gdouble gnome_font_face_get_glyph_width (const GnomeFontFace * face, gint glyph);
gdouble gnome_font_face_get_glyph_kerning (const GnomeFontFace * face, gint glyph1, gint glyph2);
const gchar * gnome_font_face_get_glyph_ps_name (const GnomeFontFace * face, gint glyph);

GnomeFontWeight gnome_font_face_get_weight_code (const GnomeFontFace * face);
gboolean gnome_font_face_is_italic (const GnomeFontFace * face);
gboolean gnome_font_face_is_fixed_width (const GnomeFontFace * face);


gchar * gnome_font_face_get_pfa (const GnomeFontFace * face);

/* Find the closest face matching the family name, weight, and italic */

GnomeFontFace * gnome_font_unsized_closest (const char *family_name,
					    GnomeFontWeight weight,
					    gboolean italic);

/*
 * Create font
 *
 * Those allow one to get sized font object from given typeface. Theoretically
 * GnomeFont should be optimized for certain resolution (resx, resy). If that
 * resolution is not known, get_font_default should give reasonably well-
 * looking font for most occasions
 */

GnomeFont * gnome_font_face_get_font (const GnomeFontFace * face, gdouble size, gdouble xres, gdouble yres);
GnomeFont * gnome_font_face_get_font_default (const GnomeFontFace * face, gdouble size);


/* Font */
GnomeFontWeight gnome_font_get_weight_code (const GnomeFont * font);
gboolean gnome_font_is_italic (const GnomeFont * font);

END_GNOME_DECLS

#endif

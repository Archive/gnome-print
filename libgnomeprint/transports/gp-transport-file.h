#ifndef __GP_TRANSPORT_FILE_H__
#define __GP_TRANSPORT_FILE_H__

/*
 * FILE transport destination
 *
 * Authors:
 *   Raph Levien (raph@acm.org)
 *   Miguel de Icaza (miguel@kernel.org)
 *   Lauris Kaplinski <lauris@ximian.com>
 *   Chema Celorio (chema@celorio.com)
 *
 * Copyright (C) 1999-2001 Ximian, Inc. and authors
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include <libgnomeprint/gnome-print-transport.h>

#define GP_TYPE_TRANSPORT_FILE (gp_transport_file_get_type ())
#define GP_TRANSPORT_FILE(obj) (GTK_CHECK_CAST ((obj), GP_TYPE_TRANSPORT_FILE, GPTransportFile))
#define GP_TRANSPORT_FILE_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GP_TYPE_TRANSPORT_FILE, GPTransportFileClass))
#define GP_IS_TRANSPORT_FILE(obj) (GTK_CHECK_TYPE ((obj), GP_TYPE_TRANSPORT_FILE))
#define GP_IS_TRANSPORT_FILE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GP_TYPE_TRANSPORT_FILE))

typedef struct _GPTransportFile GPTransportFile;
typedef struct _GPTransportFileClass GPTransportFileClass;

struct _GPTransportFile {
	GnomePrintTransport transport;
	guchar *name;
	gint fd;
};

struct _GPTransportFileClass {
	GnomePrintTransportClass parent_class;
};

GtkType gp_transport_file_get_type (void);

END_GNOME_DECLS

#endif



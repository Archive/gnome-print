#define __GP_TRANSPORT_LPR_C__

/*
 * LPR transport destination
 *
 * Authors:
 *   Raph Levien (raph@acm.org)
 *   Miguel de Icaza (miguel@kernel.org)
 *   Lauris Kaplinski <lauris@ximian.com>
 *   Chema Celorio (chema@celorio.com)
 *
 * Copyright (C) 1999-2001 Ximian, Inc. and authors
 *
 */

#include <libgnomeprint/gnome-print.h>
#include "gp-transport-lpr.h"

static void gp_transport_lpr_class_init (GPTransportLPRClass *klass);
static void gp_transport_lpr_init (GPTransportLPR *tlpr);

static void gp_transport_lpr_destroy (GtkObject *object);

static gint gp_transport_lpr_construct (GnomePrintTransport *transport);
static gint gp_transport_lpr_open (GnomePrintTransport *transport);
static gint gp_transport_lpr_close (GnomePrintTransport *transport);
static gint gp_transport_lpr_write (GnomePrintTransport *transport, const guchar *buf, gint len);

#ifdef MODULAR_LIBRARY
GtkType gnome_print__transport_get_type (void);
#endif

static GnomePrintTransportClass *parent_class = NULL;

GtkType
gp_transport_lpr_get_type (void)
{
	static GtkType type = 0;
	if (!type) {
		GtkTypeInfo info = {
			"GPTransportLPR",
			sizeof (GPTransportLPR),
			sizeof (GPTransportLPRClass),
			(GtkClassInitFunc) gp_transport_lpr_class_init,
			(GtkObjectInitFunc) gp_transport_lpr_init,
			NULL, NULL, NULL
		};
		type = gtk_type_unique (GNOME_TYPE_PRINT_TRANSPORT, &info);
	}
	return type;
}

static void
gp_transport_lpr_class_init (GPTransportLPRClass *klass)
{
	GtkObjectClass *object_class;
	GnomePrintTransportClass *transport_class;

	object_class = (GtkObjectClass *) klass;
	transport_class = (GnomePrintTransportClass *) klass;

	parent_class = gtk_type_class (GNOME_TYPE_PRINT_TRANSPORT);

	object_class->destroy = gp_transport_lpr_destroy;

	transport_class->construct = gp_transport_lpr_construct;
	transport_class->open = gp_transport_lpr_open;
	transport_class->close = gp_transport_lpr_close;
	transport_class->write = gp_transport_lpr_write;
}

static void
gp_transport_lpr_init (GPTransportLPR *tlpr)
{
	tlpr->printer = NULL;
	tlpr->pipe = NULL;
}

static void
gp_transport_lpr_destroy (GtkObject *object)
{
	GPTransportLPR *tlpr;

	tlpr = GP_TRANSPORT_LPR (object);

	if (tlpr->pipe) {
		g_warning ("Destroying GnomePrintTransportLPR with open pipe");
		pclose (tlpr->pipe);
		tlpr->pipe = NULL;
	}

	if (tlpr->printer) {
		g_free (tlpr->printer);
		tlpr->printer = NULL;
	}

	if (((GtkObjectClass *) (parent_class))->destroy)
		(* ((GtkObjectClass *) (parent_class))->destroy) (object);
}

static gint
gp_transport_lpr_construct (GnomePrintTransport *transport)
{
	GPTransportLPR *tlpr;
	guchar *value;

	tlpr = GP_TRANSPORT_LPR (transport);

	value = gpa_node_get_path_value (transport->config, "Settings.Transport.Backend.Printer");

	if (value && *value) {
		tlpr->printer = value;
	}

	return GNOME_PRINT_ERROR_UNKNOWN;
}

static gint
gp_transport_lpr_open (GnomePrintTransport *transport)
{
	GPTransportLPR *tlpr;
	guchar *command;

	tlpr = GP_TRANSPORT_LPR (transport);

	if (tlpr->printer) {
		command = g_strdup_printf ("lpr -P%s", tlpr->printer);
	} else {
		command = g_strdup ("lpr");
	}

	tlpr->pipe = popen (command, "w");
	g_free (command);

	if (tlpr->pipe == NULL) {
		g_warning ("Opening 'lpr -P%s' for output failed", tlpr->printer);
		return GNOME_PRINT_ERROR_UNKNOWN;
	}

	return GNOME_PRINT_OK;
}

static gint
gp_transport_lpr_close (GnomePrintTransport *transport)
{
	GPTransportLPR *tlpr;

	tlpr = GP_TRANSPORT_LPR (transport);

	g_return_val_if_fail (tlpr->pipe != NULL, GNOME_PRINT_ERROR_UNKNOWN);

	if (pclose (tlpr->pipe) < 0) {
		g_warning ("Closing output pipe failed");
		return GNOME_PRINT_ERROR_UNKNOWN;
	}

	tlpr->pipe = NULL;

	return GNOME_PRINT_OK;
}

static gint
gp_transport_lpr_write (GnomePrintTransport *transport, const guchar *buf, gint len)
{
	GPTransportLPR *tlpr;
	size_t written;

	tlpr = GP_TRANSPORT_LPR (transport);

	g_return_val_if_fail (tlpr->pipe != NULL, GNOME_PRINT_ERROR_UNKNOWN);

	written = fwrite (buf, sizeof (guchar), len, tlpr->pipe);

	if (written < 0) {
		g_warning ("Writing output pipe failed");
		return GNOME_PRINT_ERROR_UNKNOWN;
	}

	return len;
}

#ifdef MODULAR_LIBRARY
GtkType
gnome_print__transport_get_type (void)
{
	return GP_TYPE_TRANSPORT_LPR;
}
#endif

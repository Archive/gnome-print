#define __GP_TRANSPORT_FILE_C__

/*
 * FILE transport destination
 *
 * Authors:
 *   Raph Levien (raph@acm.org)
 *   Miguel de Icaza (miguel@kernel.org)
 *   Lauris Kaplinski <lauris@ximian.com>
 *   Chema Celorio (chema@celorio.com)
 *
 * Copyright (C) 1999-2001 Ximian, Inc. and authors
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <libgnomeprint/gnome-print.h>
#include "gp-transport-file.h"

static void gp_transport_file_class_init (GPTransportFileClass *klass);
static void gp_transport_file_init (GPTransportFile *tf);

static void gp_transport_file_destroy (GtkObject *object);

static gint gp_transport_file_construct (GnomePrintTransport *transport);
static gint gp_transport_file_open (GnomePrintTransport *transport);
static gint gp_transport_file_close (GnomePrintTransport *transport);
static gint gp_transport_file_write (GnomePrintTransport *transport, const guchar *buf, gint len);

#ifdef MODULAR_LIBRARY
GtkType gnome_print__transport_get_type (void);
#endif

static GnomePrintTransportClass *parent_class = NULL;

GtkType
gp_transport_file_get_type (void)
{
	static GtkType type = 0;
	if (!type) {
		GtkTypeInfo info = {
			"GPTransportFile",
			sizeof (GPTransportFile),
			sizeof (GPTransportFileClass),
			(GtkClassInitFunc) gp_transport_file_class_init,
			(GtkObjectInitFunc) gp_transport_file_init,
			NULL, NULL, NULL
		};
		type = gtk_type_unique (GNOME_TYPE_PRINT_TRANSPORT, &info);
	}
	return type;
}

static void
gp_transport_file_class_init (GPTransportFileClass *klass)
{
	GtkObjectClass *object_class;
	GnomePrintTransportClass *transport_class;

	object_class = (GtkObjectClass *) klass;
	transport_class = (GnomePrintTransportClass *) klass;

	parent_class = gtk_type_class (GNOME_TYPE_PRINT_TRANSPORT);

	object_class->destroy = gp_transport_file_destroy;

	transport_class->construct = gp_transport_file_construct;
	transport_class->open = gp_transport_file_open;
	transport_class->close = gp_transport_file_close;
	transport_class->write = gp_transport_file_write;
}

static void
gp_transport_file_init (GPTransportFile *tf)
{
	tf->name = NULL;
	tf->fd = -1;
}

static void
gp_transport_file_destroy (GtkObject *object)
{
	GPTransportFile *tf;

	tf = GP_TRANSPORT_FILE (object);

	if (tf->fd) {
		g_warning ("Destroying GPTransportFile with open file descriptor");
	}

	if (tf->name) {
		g_free (tf->name);
		tf->name = NULL;
	}

	if (((GtkObjectClass *) (parent_class))->destroy)
		(* ((GtkObjectClass *) (parent_class))->destroy) (object);
}

static gint
gp_transport_file_construct (GnomePrintTransport *transport)
{
	GPTransportFile *tf;
	guchar *value;

	tf = GP_TRANSPORT_FILE (transport);

	value = gpa_node_get_path_value (transport->config, "Settings.Transport.Backend.FileName");

	if (value) {
		tf->name = value;
		return GNOME_PRINT_OK;
	}

	g_warning ("Configuration does not specify filename");

	return GNOME_PRINT_ERROR_UNKNOWN;
}

static gint
gp_transport_file_open (GnomePrintTransport *transport)
{
	GPTransportFile *tf;

	tf = GP_TRANSPORT_FILE (transport);

	g_return_val_if_fail (tf->name != NULL, GNOME_PRINT_ERROR_UNKNOWN);

	tf->fd = open (tf->name, O_CREAT | O_TRUNC | O_WRONLY, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);

	if (tf->fd < 0) {
		g_warning ("Opening file %s for output failed", tf->name);
		return GNOME_PRINT_ERROR_UNKNOWN;
	}

	return GNOME_PRINT_OK;
}

static gint
gp_transport_file_close (GnomePrintTransport *transport)
{
	GPTransportFile *tf;

	tf = GP_TRANSPORT_FILE (transport);

	g_return_val_if_fail (tf->fd >= 0, GNOME_PRINT_ERROR_UNKNOWN);

	if (close (tf->fd) < 0) {
		g_warning ("Closing output file failed");
		return GNOME_PRINT_ERROR_UNKNOWN;
	}

	tf->fd = -1;

	return GNOME_PRINT_OK;
}

static gint
gp_transport_file_write (GnomePrintTransport *transport, const guchar *buf, gint len)
{
	GPTransportFile *tf;
	gint l;

	tf = GP_TRANSPORT_FILE (transport);

	g_return_val_if_fail (tf->fd >= 0, GNOME_PRINT_ERROR_UNKNOWN);

	l = len;
	while (l > 0) {
		size_t written;
		written = write (tf->fd, buf, len);
		if (written < 0) {
			g_warning ("Writing output file failed");
			return GNOME_PRINT_ERROR_UNKNOWN;
		}
		buf += written;
		l -= written;
	}

	return len;
}

#ifdef MODULAR_LIBRARY
GtkType
gnome_print__transport_get_type (void)
{
	return GP_TYPE_TRANSPORT_FILE;
}
#endif

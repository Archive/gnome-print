#ifndef __GP_TRANSPORT_LPR_H__
#define __GP_TRANSPORT_LPR_H__

/*
 * LPR transport destination
 *
 * Authors:
 *   Raph Levien (raph@acm.org)
 *   Miguel de Icaza (miguel@kernel.org)
 *   Lauris Kaplinski <lauris@ximian.com>
 *   Chema Celorio (chema@celorio.com)
 *
 * Copyright (C) 1999-2001 Ximian, Inc. and authors
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include <stdio.h>
#include <libgnomeprint/gnome-print-transport.h>

#define GP_TYPE_TRANSPORT_LPR (gp_transport_lpr_get_type ())
#define GP_TRANSPORT_LPR(obj) (GTK_CHECK_CAST ((obj), GP_TYPE_TRANSPORT_LPR, GPTransportLPR))
#define GP_TRANSPORT_LPR_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GP_TYPE_TRANSPORT_LPR, GPTransportLPRClass))
#define GP_IS_TRANSPORT_LPR(obj) (GTK_CHECK_TYPE ((obj), GP_TYPE_TRANSPORT_LPR))
#define GP_IS_TRANSPORT_LPR_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GP_TYPE_TRANSPORT_LPR))

typedef struct _GPTransportLPR GPTransportLPR;
typedef struct _GPTransportLPRClass GPTransportLPRClass;

struct _GPTransportLPR {
	GnomePrintTransport transport;
	guchar *printer;
	FILE *pipe;
};

struct _GPTransportLPRClass {
	GnomePrintTransportClass parent_class;
};

GtkType gp_transport_lpr_get_type (void);

END_GNOME_DECLS

#endif



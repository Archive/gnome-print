#ifndef __GNOME_PRINT_FRGBA_H__
#define __GNOME_PRINT_FRGBA_H__

/*
 *  Copyright (C) 2000-2001 Ximian Inc. and authors
 *
 *  Authors:
 *    Lauris Kaplinski (lauris@ximian.com)
 *
 *  Wrapper context that renders semitransparent objects as bitmaps
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#define GNOME_TYPE_PRINT_FRGBA (gnome_print_frgba_get_type ())
#define GNOME_PRINT_FRGBA(obj) (GTK_CHECK_CAST ((obj), GNOME_TYPE_PRINT_FRGBA, GnomePrintFRGBA))
#define GNOME_PRINT_FRGBA_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_PRINT_FRGBA, GnomePrintFRGBAClass))
#define GNOME_IS_PRINT_FRGBA(obj) (GTK_CHECK_TYPE ((obj), GNOME_TYPE_PRINT_FRGBA))
#define GNOME_IS_PRINT_FRGBA_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_PRINT_FRGBA))

typedef struct _GnomePrintFRGBA GnomePrintFRGBA;
typedef struct _GnomePrintFRGBAClass GnomePrintFRGBAClass;
#ifndef BREAK_COMPATIBILITY
typedef struct _GnomePrintFRGBAPrivate GnomePrintFRGBAPrivate;
#endif

#include <libgnomeprint/gnome-print.h>

GtkType gnome_print_frgba_get_type (void);

/* Creates new FRGBA wrapper context around existing one (usable mostly for PostScript) */
GnomePrintContext *gnome_print_frgba_new (GnomePrintContext *context);

END_GNOME_DECLS

#endif /* __GNOME_PRINT_FRGBA_H__ */


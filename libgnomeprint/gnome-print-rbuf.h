#ifndef __GNOME_PRINT_RBUF_H__
#define __GNOME_PRINT_RBUF_H__

/*
 *  Copyright (C) 2000-2001 Ximian Inc. and authors
 *
 *  Authors:
 *    Lauris Kaplinski (lauris@ariman.ee)
 *
 *  Driver that renders into transformed rectangular RGB(A) buffer
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#define GNOME_TYPE_PRINT_RBUF (gnome_print_rbuf_get_type ())
#define GNOME_PRINT_RBUF(obj) (GTK_CHECK_CAST ((obj), GNOME_TYPE_PRINT_RBUF, GnomePrintRBuf))
#define GNOME_PRINT_RBUF_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_PRINT_RBUF, GnomePrintRBufClass))
#define GNOME_IS_PRINT_RBUF(obj) (GTK_CHECK_TYPE ((obj), GNOME_TYPE_PRINT_RBUF))
#define GNOME_IS_PRINT_RBUF_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_PRINT_RBUF))

typedef struct _GnomePrintRBuf GnomePrintRBuf;
typedef struct _GnomePrintRBufClass GnomePrintRBufClass;

#ifndef BREAK_COMPATIBILITY
#include <gdk-pixbuf/gdk-pixbuf.h>
#endif
#include <libgnomeprint/gnome-print.h>

GtkType gnome_print_rbuf_get_type (void);

GnomePrintContext * gnome_print_rbuf_new (guchar *pixels, gint width, gint height, gint rowstride,
					  gdouble page2buf[6], gboolean alpha);

#ifndef BREAK_COMPATIBILITY
GnomePrintRBuf * gnome_print_rbuf_construct (GnomePrintRBuf * rbuf,
					     guchar * pixels, gint width, gint height, gint rowstride,
					     gdouble page2buf[6], gboolean alpha);
#endif

END_GNOME_DECLS

#endif /* __GNOME_PRINT_RBUF_H__ */


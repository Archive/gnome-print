#ifndef _GNOME_PRINT_TRANSPORT_H_
#define _GNOME_PRINT_TRANSPORT_H_

/*
 * Abstract base class for transport providers
 *
 * Authors:
 *   Raph Levien (raph@acm.org)
 *   Miguel de Icaza (miguel@kernel.org)
 *   Lauris Kaplinski <lauris@ximian.com>
 *   Chema Celorio (chema@celorio.com)
 *
 * Copyright (C) 1999-2001 Ximian, Inc. and authors
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include <gtk/gtkobject.h>
#include "gpa-node.h"

#define GNOME_TYPE_PRINT_TRANSPORT         (gnome_print_transport_get_type ())
#define GNOME_PRINT_TRANSPORT(obj)         (GTK_CHECK_CAST ((obj), GNOME_TYPE_PRINT_TRANSPORT, GnomePrintTransport))
#define GNOME_PRINT_TRANSPORT_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_PRINT_TRANSPORT, GnomePrintTransportClass))
#define GNOME_IS_PRINT_TRANSPORT(obj)      (GTK_CHECK_TYPE ((obj), GNOME_TYPE_PRINT_TRANSPORT))
#define GNOME_IS_PRINT_TRANSPORT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_PRINT_TRANSPORT))

typedef struct _GnomePrintTransport       GnomePrintTransport;
typedef struct _GnomePrintTransportClass  GnomePrintTransportClass;

struct _GnomePrintTransport {
	GtkObject object;
	GPANode *config;
	guint opened : 1;
};

struct _GnomePrintTransportClass {
	GtkObjectClass parent_class;

	gint (* construct) (GnomePrintTransport *transport);

	gint (* open) (GnomePrintTransport *transport);
	gint (* close) (GnomePrintTransport *transport);

	gint (* write) (GnomePrintTransport *transport, const guchar *buf, gint len);
};

GtkType gnome_print_transport_get_type (void);

gint gnome_print_transport_construct (GnomePrintTransport *transport, GPANode *config);
gint gnome_print_transport_open (GnomePrintTransport *transport);
gint gnome_print_transport_close (GnomePrintTransport *transport);

gint gnome_print_transport_write (GnomePrintTransport *transport, const guchar *buf, gint len);

/* Convenience methods */

gint gnome_print_transport_printf (GnomePrintTransport *pc, const char *fmt, ...);

GnomePrintTransport *gnome_print_transport_new (GPANode *config);

#if 0
int gnome_print_transport_open_file (GnomePrintTransport *pc, const char *filename);
int gnome_print_transport_write_file (GnomePrintTransport *pc, const void *buf, size_t size);
int gnome_print_transport_close_file (GnomePrintTransport *pc);
#endif

END_GNOME_DECLS

#endif



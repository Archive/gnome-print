#ifndef __GNOME_PRINTER_PRIVATE_H__
#define __GNOME_PRINTER_PRIVATE_H__

/*
 * GnomePrinter
 *
 * A wrapper class around settings GPANode
 *
 * Authors:
 *   Unknown author
 *   Chema Celorio <chema@ximian.com>
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright 1999-2001 Ximian, Inc. and authors
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include <gtk/gtkobject.h>
#include "gnome-printer.h"
#include "gpa-node.h"

struct _GnomePrinter {
	GtkObject object;

	GPANode *node;
};

struct _GnomePrinterClass {
	GtkObjectClass object_class;
};


GnomePrinter *gnome_printer_new (GPANode *settings);

#if 0
const gchar *gnome_printer_const_get (GnomePrinter *printer, const gchar *info_id);
#endif

END_GNOME_DECLS

#endif /* __GNOME_PRINTER_PRIVATE_H__ */


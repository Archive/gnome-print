#ifndef __GNOME_PRINT_RGBP_H__
#define __GNOME_PRINT_RGBP_H__

#include <libart_lgpl/art_rect.h>
#include <libgnome/gnome-defs.h>
#include <libgnomeprint/gnome-print-private.h>

BEGIN_GNOME_DECLS

/*
 * GnomePrintRGBP printer context.
 */

#define GNOME_TYPE_PRINT_RGBP (gnome_print_rgbp_get_type ())
#define GNOME_PRINT_RGBP(obj) (GTK_CHECK_CAST ((obj), GNOME_TYPE_PRINT_RGBP, GnomePrintRGBP))
#define GNOME_PRINT_RGBP_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_PRINT_RGBP, GnomePrintRGBPClass))
#define GNOME_IS_PRINT_RGBP(obj) (GTK_CHECK_TYPE ((obj), GNOME_TYPE_PRINT_RGBP))
#define GNOME_IS_PRINT_RGBP_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_PRINT_RGBP))

typedef struct _GnomePrintRGBP GnomePrintRGBP;
typedef struct _GnomePrintRGBPClass GnomePrintRGBPClass;

/* RGBP is private class, so everything can be exposed in header */

struct _GnomePrintRGBP {
	GnomePrintContext pc;

	/* Margins */
	ArtDRect margins;
	/* Resolution */
	gdouble dpix, dpiy;
	gint band_height;

	GnomePrintContext *meta;
};

struct _GnomePrintRGBPClass {
	GnomePrintContextClass parent_class;

	int (*print_init) (GnomePrintRGBP *rgbp, gdouble dpix, gdouble dpiy);
	int (*page_begin) (GnomePrintRGBP *rgbp);
	int (*page_end)   (GnomePrintRGBP *rgbp);
	int (*print_band) (GnomePrintRGBP *rgbp, guchar *rgb_buffer, ArtIRect *rect);
};

GtkType gnome_print_rgbp_get_type (void);

GnomePrintContext *gnome_print_rgbp_new (ArtDRect *margins, gdouble dpix, gdouble dpiy, gint band_height);

gint gnome_print_rgbp_construct (GnomePrintRGBP *rgbp, ArtDRect *margins, gdouble dpix, gdouble dpiy, gint band_height);

END_GNOME_DECLS

#endif /* __GNOME_PRINT_RGBP_H__ */


#define __GNOME_PRINT_TRANSPORT_C__

/*
 * Abstract base class for transport providers
 *
 * Authors:
 *   Raph Levien (raph@acm.org)
 *   Miguel de Icaza (miguel@kernel.org)
 *   Lauris Kaplinski <lauris@ximian.com>
 *   Chema Celorio (chema@celorio.com)
 *
 * Copyright (C) 1999-2001 Ximian, Inc. and authors
 *
 */

#include <string.h>
#include <locale.h>
#include <gmodule.h>
#include "gnome-print.h"
#ifndef MODULAR_LIBRARY
#include "transports/gp-transport-file.h"
#include "transports/gp-transport-lpr.h"
#endif
#include "gnome-print-transport.h"

static void gnome_print_transport_class_init (GnomePrintTransportClass *klass);
static void gnome_print_transport_init (GnomePrintTransport *transport);

static void gnome_print_transport_destroy (GtkObject *object);

static GnomePrintTransport *gnome_print_transport_create (gpointer get_type, GPANode *config);

static GtkObjectClass *parent_class = NULL;

GtkType
gnome_print_transport_get_type (void)
{
	static GtkType pc_type = 0;
	if (!pc_type) {
		GtkTypeInfo pc_info = {
			"GnomePrintTransport",
			sizeof (GnomePrintTransport),
			sizeof (GnomePrintTransportClass),
			(GtkClassInitFunc) gnome_print_transport_class_init,
			(GtkObjectInitFunc) gnome_print_transport_init,
			NULL, NULL, NULL
		};
		pc_type = gtk_type_unique (gtk_object_get_type (), &pc_info);
	}
	return pc_type;
}

static void
gnome_print_transport_class_init (GnomePrintTransportClass *class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass*) class;

	parent_class = gtk_type_class (gtk_object_get_type ());

	object_class->destroy = gnome_print_transport_destroy;
}

static void
gnome_print_transport_init (GnomePrintTransport *transport)
{
	transport->config = NULL;
	transport->opened = FALSE;
}

static void
gnome_print_transport_destroy (GtkObject *object)
{
	GnomePrintTransport *transport;

	transport = GNOME_PRINT_TRANSPORT (object);

	if (transport->opened) {
		g_warning ("Destroying open transport provider");
	}

	if (transport->config) {
		gpa_node_unref (transport->config);
		transport->config = NULL;
	}

	if (((GtkObjectClass *) (parent_class))->destroy)
		(* ((GtkObjectClass *) (parent_class))->destroy) (object);
}

gint
gnome_print_transport_construct (GnomePrintTransport *transport, GPANode *config)
{
	g_return_val_if_fail (transport != NULL, GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (GNOME_IS_PRINT_TRANSPORT (transport), GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (config != NULL, GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (GPA_IS_NODE (config), GNOME_PRINT_ERROR_UNKNOWN);

	g_return_val_if_fail (transport->config == NULL, GNOME_PRINT_ERROR_UNKNOWN);

	transport->config = config;
	gpa_node_ref (transport->config);

	if (((GnomePrintTransportClass *) ((GtkObject *) transport)->klass)->construct)
		return (* ((GnomePrintTransportClass *) ((GtkObject *) transport)->klass)->construct) (transport);

	return TRUE;
}

gint
gnome_print_transport_open (GnomePrintTransport *transport)
{
	gint ret;

	g_return_val_if_fail (transport != NULL, GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (GNOME_IS_PRINT_TRANSPORT (transport), GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (transport->config != NULL, GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (GPA_IS_NODE (transport->config), GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (!transport->opened, GNOME_PRINT_ERROR_UNKNOWN);

	ret = GNOME_PRINT_OK;

	if (((GnomePrintTransportClass *) ((GtkObject *) transport)->klass)->open)
		ret = (* ((GnomePrintTransportClass *) ((GtkObject *) transport)->klass)->open) (transport);

	if (ret == GNOME_PRINT_OK) {
		transport->opened = TRUE;
	}

	return ret;
}

gint
gnome_print_transport_close (GnomePrintTransport *transport)
{
	gint ret;

	g_return_val_if_fail (transport != NULL, GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (GNOME_IS_PRINT_TRANSPORT (transport), GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (transport->opened, GNOME_PRINT_ERROR_UNKNOWN);

	ret = GNOME_PRINT_OK;

	if (((GnomePrintTransportClass *) ((GtkObject *) transport)->klass)->close)
		ret = (* ((GnomePrintTransportClass *) ((GtkObject *) transport)->klass)->close) (transport);

	if (ret == GNOME_PRINT_OK) {
		transport->opened = FALSE;
	}

	return ret;
}

gint
gnome_print_transport_write (GnomePrintTransport *transport, const guchar *buf, gint len)
{
	g_return_val_if_fail (transport != NULL, GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (GNOME_IS_PRINT_TRANSPORT (transport), GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (buf != NULL, GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (len >= 0, GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (transport->opened, GNOME_PRINT_ERROR_UNKNOWN);

	if (((GnomePrintTransportClass *) ((GtkObject *) transport)->klass)->write)
		return (* ((GnomePrintTransportClass *) ((GtkObject *) transport)->klass)->write) (transport, buf, len);

	return 0;
}

gint
gnome_print_transport_printf (GnomePrintTransport *transport, const char *format, ...)
{
	va_list arguments;
	const char *loc;
	gchar *buf;
	gint ret;

	g_return_val_if_fail (transport != NULL, GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (GNOME_IS_PRINT_TRANSPORT (transport), GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (format != NULL, GNOME_PRINT_ERROR_UNKNOWN);
	g_return_val_if_fail (transport->opened, GNOME_PRINT_ERROR_UNKNOWN);

	loc = setlocale (LC_NUMERIC, NULL);
	setlocale (LC_NUMERIC, "C");

	va_start (arguments, format);
	buf = g_strdup_vprintf (format, arguments);
	va_end (arguments);

	ret = GNOME_PRINT_OK;

	if (((GnomePrintTransportClass *) ((GtkObject *) transport)->klass)->write)
		ret = (* ((GnomePrintTransportClass *) ((GtkObject *) transport)->klass)->write) (transport, buf, strlen (buf));

	g_free (buf);

	setlocale (LC_NUMERIC, loc);

	return ret;
}

GnomePrintTransport *
gnome_print_transport_new (GPANode *config)
{
	GnomePrintTransport *transport;
	guchar *drivername;
	guchar *modulename;

	g_return_val_if_fail (config != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (config), NULL);

	drivername = gpa_node_get_path_value (config, "Settings.Transport.Backend");
	if (!drivername) {
		g_warning ("Settings do not specify transport driver");
		return NULL;
	}

	transport = NULL;

	modulename = gpa_node_get_path_value (config, "Settings.Transport.Backend.Module");
	if (modulename) {
#ifdef MODULAR_LIBRARY
		GModule *module;
		gchar *path;
		path = g_module_build_path (GNOME_PRINT_LIBDIR "/transports", modulename);
		module = g_module_open (path, G_MODULE_BIND_LAZY);
		if (module) {
			gpointer get_type;
			if (g_module_symbol (module, "gnome_print__transport_get_type", &get_type)) {
				transport = gnome_print_transport_create (get_type, config);
			} else {
				g_warning ("Missing gnome_print__transport_get_type in %s\n", modulename);
				g_module_close (module);
			}
		} else {
			g_warning ("Cannot open module: %s\n", modulename);
		}
		g_free (path);
		g_free (modulename);
#else
		if (!strcmp (modulename, "libgnomeprint-file.so")) {
			transport = gnome_print_transport_create (gp_transport_file_get_type, config);
		} else if (!strcmp (modulename, "libgnomeprint-lpr.so")) {
			transport = gnome_print_transport_create (gp_transport_lpr_get_type, config);
		}
#endif
	} else {
		g_warning ("Unknown transport driver: %s", modulename);
	}

	g_free (drivername);

	return transport;
}

static GnomePrintTransport *
gnome_print_transport_create (gpointer get_type, GPANode *config)
{
	GnomePrintTransport *transport;
	GtkType (* transport_get_type) (void);
	GtkType type;

	transport_get_type = get_type;

	type = (* transport_get_type) ();
	g_return_val_if_fail (gtk_type_is_a (type, GNOME_TYPE_PRINT_TRANSPORT), NULL);

	transport = gtk_type_new (type);
	gnome_print_transport_construct (transport, config);

	return transport;
}


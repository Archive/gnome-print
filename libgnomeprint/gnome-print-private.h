#ifndef _GNOME_PRINT_PRIVATE_H_
#define _GNOME_PRINT_PRIVATE_H_

/*
 * Abstract base class for drivers
 *
 * Authors:
 *   Raph Levien (raph@acm.org)
 *   Miguel de Icaza (miguel@kernel.org)
 *   Lauris Kaplinski <lauris@ximian.com>
 *   Chema Celorio (chema@celorio.com)
 *
 * Copyright (C) 1999-2001 Ximian, Inc. and authors
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include <stdio.h>
#include <libart_lgpl/art_svp.h>
#include <libart_lgpl/art_svp_wind.h>
#include <libgnomeprint/gp-gc.h>
#include "gnome-print-transport.h"
#include "gnome-print.h"

#define ALLOW_BROKEN_PGL

#if 0
typedef enum {
	GNOME_PRINT_OUTPUT_NULL,
	GNOME_PRINT_OUTPUT_FILE,
	GNOME_PRINT_OUTPUT_PIPE,
	GNOME_PRINT_OUTPUT_PROGRAM
} GnomePrintOutputType;
#endif

struct _GnomePrintContext {
	GtkObject object;

	GPANode *config;
	GnomePrintTransport *transport;

	GPGC * gc;
	gboolean haspage;

#if 0
	GnomePrintOutputType output;
	gchar *command;
	gchar *filename;
	FILE *f;
#endif
};

struct _GnomePrintContextClass {
	GtkObjectClass parent_class;

	int (* construct) (GnomePrintContext *ctx);

	int (* beginpage) (GnomePrintContext *pc, const char *name_of_this_page);
	int (* showpage) (GnomePrintContext *pc);

	int (* gsave) (GnomePrintContext *pc);
	int (* grestore) (GnomePrintContext *pc);

	int (* clip) (GnomePrintContext *pc, const ArtBpath *bpath, ArtWindRule rule);
	int (* fill) (GnomePrintContext *pc, const ArtBpath *bpath, ArtWindRule rule);
	int (* stroke) (GnomePrintContext *pc, const ArtBpath *bpath);

	int (* image) (GnomePrintContext *pc, const gdouble *affine, const char *px, int w, int h, int rowstride, int ch);

	int (* glyphlist) (GnomePrintContext * pc, const gdouble *affine, GnomeGlyphList *gl);

	int (* close) (GnomePrintContext *pc);
};

int gnome_print_context_construct (GnomePrintContext *ctx, GPANode *config);

int gnome_print_context_create_transport (GnomePrintContext *ctx);

#if 0
/* These were moved into transport implementation */
/* These are functions for writing bytes to the printer - generally to a
   file or piped to lpr. */

int gnome_print_context_open_file (GnomePrintContext *pc, const char *filename);
int gnome_print_context_write_file (GnomePrintContext *pc, const void *buf, size_t size);
int gnome_print_context_fprintf (GnomePrintContext *pc, const char *fmt, ...);
int gnome_print_context_close_file (GnomePrintContext *pc);
#endif

int gnome_print_clip_bpath_rule (GnomePrintContext *pc, const ArtBpath *bpath, ArtWindRule rule);
int gnome_print_fill_bpath_rule (GnomePrintContext *pc, const ArtBpath *bpath, ArtWindRule rule);
int gnome_print_stroke_bpath (GnomePrintContext *pc, const ArtBpath *bpath);
int gnome_print_image_transform (GnomePrintContext *pc, const gdouble *affine, const char *px, int w, int h, int rowstride, int ch);
int gnome_print_glyphlist_transform (GnomePrintContext *pc, const gdouble *affine, GnomeGlyphList *gl);

END_GNOME_DECLS

#endif



#define __GNOME_PRINTER_C__

/*
 * GnomePrinter
 *
 * A typedef of GPANode
 *
 * Authors:
 *   Unknown author
 *   Chema Celorio <chema@ximian.com>
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright 1999-2001 Ximian, Inc. and authors
 *
 */

#include "config.h"
#include "gnome-print-i18n.h"
#include <libgpa/gpa-node-private.h>
#include "gnome-printer.h"

/* fixme: These are probably bogus */

struct _GnomePrinter {
	GPANode node;
};

struct _GnomePrinterClass {
	GPANodeClass node_class;
};

/* fixme: Eventually we want to use GnomePrintConfig or something */

GtkType
gnome_printer_get_type (void)
{
	return GPA_TYPE_NODE;
}

GnomePrinter *
gnome_printer_new_generic_ps (const gchar *filename)
{
	GPANode *config;

	g_return_val_if_fail (filename != NULL, NULL);
	g_return_val_if_fail (*filename != '\0', NULL);

	config = gpa_defaults ();
	g_return_val_if_fail (config != NULL, NULL);

	if (!gpa_node_set_path_value (config, "Printer", "GNOME-GENERIC-PS")) {
		g_warning ("file %s: line %d: Cannot select GNOME-GENERIC-PS printer", __FILE__, __LINE__);
		gpa_node_unref (config);
		return NULL;
	}

	if (*filename != '|') {
		/* We are file, really */
		if (!gpa_node_set_path_value (config, "Settings.Transport.Backend", "file")) {
			g_warning ("file %s: line %d: Cannot select 'file' as transport backend", __FILE__, __LINE__);
			gpa_node_unref (config);
			return NULL;
		}
		if (!gpa_node_set_path_value (config, "Settings.Transport.Backend.FileName", filename + 1)) {
			g_warning ("file %s: line %d: Cannot select '%s' as output filename", __FILE__, __LINE__, filename + 1);
			gpa_node_unref (config);
			return NULL;
		}
	} else {
		g_warning ("file %s: line %d: Output to pipe not supported yet", __FILE__, __LINE__);
		gpa_node_unref (config);
		return NULL;
	}

	return (GnomePrinter *) config;
}

/**
 * gnome_printer_get_status:
 * @printer: The printer
 *
 * Returns the current status of @printer
 */
GnomePrinterStatus
gnome_printer_get_status (GnomePrinter *printer)
{
	g_return_val_if_fail (printer != NULL, GNOME_PRINTER_INACTIVE);
	g_return_val_if_fail (GNOME_IS_PRINTER (printer), GNOME_PRINTER_INACTIVE);

	return GNOME_PRINTER_INACTIVE;
}

/**
 * gnome_printer_str_status
 * @status: A status type
 *
 * Returns a string representation of the printer status code @status
 */
const gchar *
gnome_printer_str_status (GnomePrinterStatus status)
{
	switch (status){
	case GNOME_PRINTER_ACTIVE:
		return _("Printer is active");

	case GNOME_PRINTER_INACTIVE:
		return _("Printer is ready to print");

	case GNOME_PRINTER_OFFLINE:
		return _("Printer is off-line");

	case GNOME_PRINTER_NET_FAILURE:
		return _("Can not communicate with printer");

	}
	return _("Unknown status");
}

void
gnome_printer_set_print_to_file (GnomePrinter *printer, gboolean print_to_file)
{
	g_return_if_fail (printer != NULL);
	g_return_if_fail (GNOME_IS_PRINTER (printer));

	g_warning ("file %s: line %d: You should not use gnome_printer_set_print_to_file", __FILE__, __LINE__);
}


gchar *
gnome_printer_dup_command (GnomePrinter *printer)
{
	g_return_val_if_fail (printer != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_PRINTER (printer), NULL);

	g_warning ("file %s: line %d: You should not use gnome_printer_dup_command", __FILE__, __LINE__);

	return NULL;
}


#define __GNOME_FONT_COMPAT_C__

/*
 * Compatibility functions for font classes
 *
 * Authors:
 *   Jody Goldberg <jody@helixcode.com>
 *   Miguel de Icaza <miguel@helixcode.com>
 *   Lauris Kaplinski <lauris@helixcode.com>
 *   Christopher James Lahey <clahey@helixcode.com>
 *   Michael Meeks <michael@helixcode.com>
 *   Morten Welinder <terra@diku.dk>
 *
 * Copyright (C) 1999-2000 Ximian, Inc. and authors
 *
 */

#if 0
#include <config.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdarg.h>
#include <libgnomeprint/gnome-print-i18n.h>
#include <libgnomeprint/gnome-font-private.h>
#include "gp-fontmap.h"
#include "gp-truetype-utils.h"
#include <freetype/ftoutln.h>
#endif

#include "gp-fontmap.h"
#include "gnome-font-private.h"

#if 0
/* fixme: remove - not needed any more (02/06/2001) */
/* return a pointer to the (PostScript) name of the font */
const gchar * gnome_font_unsized_get_glyph_name (const GnomeFontFace * face)
{
	g_return_val_if_fail (face != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), NULL);

	if (face->private->entry->type == GP_FONT_ENTRY_TYPE1_ALIAS) {
		return ((GPFontEntryT1Alias *) face->private->entry)->alias;
	} else {
		return face->private->entry->psname;
	}
}
#endif

gdouble
gnome_font_face_get_ascender (const GnomeFontFace *face)
{
	g_return_val_if_fail (face != NULL, 1000.0);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), 1000.0);

	if (!GFF_LOADED (face)) {
		g_warning ("file %s: line %d: face %s: Cannot load face", __FILE__, __LINE__, face->entry->name);
		return 1000.0;
	}

	return face->ft_face->ascender * face->ft2ps;
}

gdouble
gnome_font_face_get_descender (const GnomeFontFace * face)
{
	g_return_val_if_fail (face != NULL, 500.0);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), 500.0);

	if (!GFF_LOADED (face)) {
		g_warning ("file %s: line %d: face %s: Cannot load face", __FILE__, __LINE__, face->entry->name);
		return 500.0;
	}

	/* fixme: I am clueless - FT spec says it is positive, but actually is not (Lauris) */
	return -face->ft_face->descender * face->ft2ps;
}

gdouble
gnome_font_face_get_underline_position (const GnomeFontFace * face)
{
	g_return_val_if_fail (face != NULL, -100.0);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), -100.0);

	if (!GFF_LOADED (face)) {
		g_warning ("file %s: line %d: face %s: Cannot load face", __FILE__, __LINE__, face->entry->name);
		return -100.0;
	}

	return face->ft_face->underline_position * face->ft2ps;
}

gdouble
gnome_font_face_get_underline_thickness (const GnomeFontFace * face)
{
	g_return_val_if_fail (face != NULL, 10.0);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), 10.0);

	if (!GFF_LOADED (face)) {
		g_warning ("file %s: line %d: face %s: Cannot load face", __FILE__, __LINE__, face->entry->name);
		return 10.0;
	}

	return face->ft_face->underline_thickness * face->ft2ps;
}

GnomeFontWeight
gnome_font_face_get_weight_code (const GnomeFontFace *face)
{
	GPFontEntry *e;

	g_return_val_if_fail (face != NULL, GNOME_FONT_BOOK);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), GNOME_FONT_BOOK);

	if (face->entry->type == GP_FONT_ENTRY_ALIAS) {
		e = ((GPFontEntryAlias *) face->entry)->ref;
	} else {
		e = face->entry;
	}

	switch (e->type) {
	case GP_FONT_ENTRY_TYPE1:
		return ((GPFontEntryT1 *) e)->Weight;
		break;
	case GP_FONT_ENTRY_TRUETYPE:
		return ((GPFontEntryTT *) e)->Weight;
		break;
	default:
		g_warning ("file %s: line %d: face %s: Illegal entry type", __FILE__, __LINE__, e->name);
		break;
	}

	return GNOME_FONT_BOOK;
}

gboolean
gnome_font_face_is_italic (const GnomeFontFace * face)
{
	GPFontEntry *e;

	g_return_val_if_fail (face != NULL, FALSE);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), FALSE);

	if (face->entry->type == GP_FONT_ENTRY_ALIAS) {
		e = ((GPFontEntryAlias *) face->entry)->ref;
	} else {
		e = face->entry;
	}

	switch (e->type) {
	case GP_FONT_ENTRY_TYPE1:
		return (((GPFontEntryT1 *) e)->ItalicAngle != 0.0);
		break;
	case GP_FONT_ENTRY_TRUETYPE:
		return (((GPFontEntryTT *) e)->ItalicAngle != 0.0);
		break;
	default:
		g_warning ("file %s: line %d: face %s: Illegal entry type", __FILE__, __LINE__, e->name);
		break;
	}

	return FALSE;
}

gboolean
gnome_font_face_is_fixed_width (const GnomeFontFace *face)
{
	g_return_val_if_fail (face != NULL, FALSE);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), FALSE);

	if (!GFF_LOADED (face)) {
		g_warning ("file %s: line %d: face %s: Cannot load face", __FILE__, __LINE__, face->entry->name);
		return FALSE;
	}

	return FT_IS_FIXED_WIDTH (face->ft_face);
}

/*
 * Returns PostScript name for glyph
 */

const gchar *
gnome_font_face_get_glyph_ps_name (const GnomeFontFace *face, gint glyph)
{
	static GHashTable *sgd = NULL;
	FT_Error status;
	gchar c[256], *name;

	g_return_val_if_fail (face != NULL, ".notdef");
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), ".notdef");

	if (!GFF_LOADED (face)) {
		g_warning ("file %s: line %d: Face %s: Cannot load face", __FILE__, __LINE__, face->entry->name);
		return ".notdef";
	}

	if (!sgd) sgd = g_hash_table_new (g_str_hash, g_str_equal);

	if ((glyph < 0) || (glyph >= face->num_glyphs)) glyph = 0;

	status = FT_Get_Glyph_Name (face->ft_face, glyph, c, 256);
	if (status != FT_Err_Ok) return ".notdef";

	name = g_hash_table_lookup (sgd, c);
	if (!name) {
		name = g_strdup (c);
		g_hash_table_insert (sgd, name, name);
	}

	return name;
}

GnomeFontWeight
gnome_font_get_weight_code (const GnomeFont *font)
{
	g_return_val_if_fail (font != NULL, GNOME_FONT_BOOK);
	g_return_val_if_fail (GNOME_IS_FONT (font), GNOME_FONT_BOOK);

	return gnome_font_face_get_weight_code (font->private->fontmap_entry);
}

gboolean
gnome_font_is_italic (const GnomeFont * font)
{
	g_return_val_if_fail (font != NULL, FALSE);
	g_return_val_if_fail (GNOME_IS_FONT (font), FALSE);

	return gnome_font_face_get_weight_code (font->private->fontmap_entry);
}


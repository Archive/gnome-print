#ifndef __GNOME_FONT_PRIVATE_H__
#define __GNOME_FONT_PRIVATE_H__

/*
 * Private declarations for fonts
 *
 * Authors:
 *   Jody Goldberg <jody@helixcode.com>
 *   Miguel de Icaza <miguel@helixcode.com>
 *   Lauris Kaplinski <lauris@helixcode.com>
 *   Christopher James Lahey <clahey@helixcode.com>
 *   Michael Meeks <michael@helixcode.com>
 *   Morten Welinder <terra@diku.dk>
 *
 * Copyright (C) 1999-2000 Helix Code, Inc. and authors
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include <freetype/freetype.h>
#include <gtk/gtkobject.h>
#include <libgnomeprint/gp-character-block.h>
#include <libgnomeprint/gp-fontmap.h>
#include <libgnomeprint/gnome-font.h>
#include <libgnomeprint/gnome-rfont.h>

typedef struct _GFFGlyphInfo GFFGlyphInfo;

struct _GnomeFontFace {
	GtkObject object;
	/* Pointer to our fontmap entry */
	GPFontEntry * entry;

	/* Glyph storage */
	gint num_glyphs;
	GFFGlyphInfo * glyphs;

	/* FT -> PostScript scaling coefficent */
	gdouble ft2ps;

	/* Face bounding box */
	ArtDRect bbox;

	/* FreeType stuff */
	FT_Face ft_face;
};

struct _GnomeFontFaceClass {
	GtkObjectClass parent_class;
};

struct _GFFGlyphInfo {
	guint metrics : 1;
	ArtPoint advance;
	ArtDRect bbox;
	ArtBpath * bpath;
};

typedef struct _GFPSObject GFPSObject;

struct _GFPSObject {
	gint bufsize;
	gint length;
	guchar * buf;
	guchar * encodedname;
	gint encodedbytes;
};

GFPSObject * gnome_font_face_create_ps_object (const GnomeFontFace * face);

typedef struct _GnomeFontKernPair	GnomeFontKernPair;
typedef struct _GnomeFontLigList	GnomeFontLigList;

struct _GnomeFontPrivate {
	double size;
	GnomeFontFace *fontmap_entry;
	double scale;
	gchar * name;
	GHashTable *outlines;
};

#if 0
struct _GnomeFontFacePrivate {

	GnomeFontWeight weight_code;
	gboolean italic;
	gboolean fixed_width;



	/* Encoding */

	GPUCMap * unimap;

	gint num_private;
	GHashTable * privencoding;

	/* Font metric info follows */
	/* above is font metric info stored in the fontmap, below is font
	 * metric info parsed from the afm file. */

	int   ascender;
	int   descender;
	int   underline_position;
	int   underline_thickness;
	double capheight;
	double italics_angle;
	double xheight;
	ArtDRect bbox;
	GnomeFontKernPair *kerns;
	int num_kerns;
	GnomeFontLigList **ligs; /* one liglist for each glyph */

	Gt1LoadedFont * loadedfont;
};
#endif

struct _GnomeFontKernPair {
	int glyph1;
	int glyph2;
	int x_amt;
};

struct _GnomeFontLigList {
	GnomeFontLigList *next;
	int succ, lig;
};

struct _GnomeFont {
	GtkObject object;
	GnomeFontPrivate * private;
};

struct _GnomeFontClass {
	GtkObjectClass parent_class;
};

/* Private face loader */

gboolean gff_load (GnomeFontFace *face);

#define GFF_LOADED(f) ((f)->ft_face || gff_load ((GnomeFontFace *) f))


/*
 * Returns PostScript name for glyph
 */

const gchar * gnome_font_face_get_glyph_ps_name (const GnomeFontFace * face, gint glyph);
const gchar * gnome_font_unsized_get_glyph_name (const GnomeFontFace * face);

ArtPoint * gnome_rfont_get_glyph_stdkerning (const GnomeRFont * rfont, gint glyph0, gint glyph1, ArtPoint * kerning);

/*
 * AFM metric info can now be retrieved via GtkArg system
 */

END_GNOME_DECLS

#endif


#ifndef __GPA_WIDGET_H__
#define __GPA_WIDGET_H__

/*
 * gpa-widget
 *
 * Abstract base class for gnome-print configuration widgets
 *
 * Author:
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include <gtk/gtkbin.h>
#include <libgnomeprint/gpa-node.h>

#define GPA_TYPE_WIDGET (gpa_widget_get_type ())
#define GPA_WIDGET(obj) (GTK_CHECK_CAST ((obj), GPA_TYPE_WIDGET, GPAWidget))
#define GPA_WIDGET_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GPA_TYPE_WIDGET, GPAWidgetClass))
#define GPA_IS_WIDGET(obj) (GTK_CHECK_TYPE ((obj), GPA_TYPE_WIDGET))
#define GPA_IS_WIDGET_CLASS (GTK_CHECK_CLASS ((obj), GPA_TYPE_WIDGET))

typedef struct _GPAWidget GPAWidget;
typedef struct _GPAWidgetClass GPAWidgetClass;

struct _GPAWidget {
	GtkBin bin;
	GPANode *node;
};

struct _GPAWidgetClass {
	GtkBinClass bin_class;
	gint (* construct) (GPAWidget *widget);
};

GtkType gpa_widget_get_type (void);

GtkWidget *gpa_widget_new (GtkType type, GPANode *node);

gboolean gpa_widget_construct (GPAWidget *widget, GPANode *node);

END_GNOME_DECLS

#endif

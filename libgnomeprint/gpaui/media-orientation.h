#ifndef __GPA_MEDIA_ORIENTATION_H__
#define __GPA_MEDIA_ORIENTATION_H__

/*
 * GPAMediaOrientationWidget
 *
 * Selector for papaer orientation
 *
 * Author:
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include "gpa-widget.h"

#define GPA_TYPE_MEDIA_ORIENTATION_WIDGET GPA_TYPE_WIDGET
#define GPA_MEDIA_ORIENTATION_WIDGET GPA_WIDGET
#define GPA_MEDIA_ORIENTATION_WIDGET_CLASS GPA_WIDGET_CLASS
#define GPA_IS_MEDIA_ORIENTATION_WIDGET GPA_IS_WIDGET
#define GPA_IS_MEDIA_ORIENTATION_WIDGET_CLASS GPA_IS_WIDGET_CLASS

typedef struct _GPAWidget GPAMediaOrientationWidget;
typedef struct _GPAWidgetClass GPAMediaOrinetationWidgetClass;

GtkWidget *gpa_media_orientation_widget_new (GPANode *node);
void gpa_media_orientation_widget_construct (GPAMediaOrientationWidget *widget, GPANode *node);

END_GNOME_DECLS

#endif

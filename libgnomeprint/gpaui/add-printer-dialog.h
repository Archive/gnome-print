#ifndef __GPA_ADD_PRINTER_DIALOG_H__
#define __GPA_ADD_PRINTER_DIALOG_H__

/*
 * add-printer-dialog
 *
 * Dialog for adding printers via libgpa framework
 *
 * Authors:
 *   Jose M. Celorio <chema@ximian.com>
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2000-2001 Ximian, Inc. and authors
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include "gpa-widget.h"

GtkWidget *gpa_add_printer_widget_new (GPANode *node);

GtkWidget *gpa_add_printer_dialog_new (GPANode *node);

END_GNOME_DECLS

#endif

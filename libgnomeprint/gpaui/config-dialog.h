#ifndef __GPA_CONFIG_DIALOG_H__
#define __GPA_CONFIG_DIALOG_H__

/*
 * config-dialog
 *
 * Dialog and widget for changing printer settings
 *
 * Authors:
 *   Jose M. Celorio <chema@ximian.com>
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2000-2001 Ximian, Inc. and authors
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include "gpa-widget.h"

GtkWidget *gpa_config_widget_new (GPANode *node);

GtkWidget *gpa_config_dialog_new (GPANode *node);

END_GNOME_DECLS

#endif

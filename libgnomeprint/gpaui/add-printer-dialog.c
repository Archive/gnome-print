#define __GPA_ADD_PRINTER_DIALOG_C__

/*
 * add-printer-dialog.c
 *
 * Dialog for adding printers via libgpa framework
 *
 * Authors:
 *   Jose M. Celorio <chema@ximian.com>
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2000-2001 Ximian, Inc. and authors
 *
 */

#include <libgnomeprint/gnome-print-i18n.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkclist.h>
#include <gtk/gtkcontainer.h>
#include <gtk/gtkdialog.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkframe.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtktable.h>
#include <libgnomeui/gnome-stock.h>
#include <libgnomeui/gnome-uidefs.h>
#include <libgpa/gpa-printer.h>
#include "add-printer-dialog.h"

static void gpa_apw_select_vendor (GtkCList *clist, gint row, gint column, GdkEvent *event, GPAWidget *gpaw);
static void gpa_apw_unselect_vendor (GtkCList *clist, gint row, gint column, GdkEvent *event, GPAWidget *gpaw);
static void gpa_apw_select_model (GtkCList *clist, gint row, gint column, GdkEvent *event, GPAWidget *gpaw);
static void gpa_apw_unselect_model (GtkCList *clist, gint row, gint column, GdkEvent *event, GPAWidget *gpaw);
static void gpa_apw_read_vendors (GPAWidget *gpaw);
static void gpa_apw_read_models (GPAWidget *gpaw, GPANode *vendor);

static void gpa_apd_ok_clicked (GtkButton *button, GtkWidget *apd);
static void gpa_apd_cancel_clicked (GtkButton *button, GtkWidget *apd);

GtkWidget *
gpa_add_printer_widget_new (GPANode *node)
{
	GtkWidget *gpa;
	GtkWidget *vb, *f, *t, *l, *sw, *cl, *hb, *e;

	g_return_val_if_fail (node != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (node), NULL);

	gpa = gpa_widget_new (GPA_TYPE_WIDGET, node);

	vb = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_container_add (GTK_CONTAINER (gpa), vb);
	gtk_widget_show (vb);

	f = gtk_frame_new (_("Select printer model"));
	gtk_box_pack_start (GTK_BOX (vb), f, TRUE, TRUE, 0);
	gtk_widget_show (f);

	t = gtk_table_new (2, 2, FALSE);
	gtk_container_add (GTK_CONTAINER (f), t);
	gtk_widget_show (t);

	l = gtk_label_new (_("Manufacturer"));
	gtk_misc_set_alignment (GTK_MISC (l), 0.0, 0.5);
	gtk_table_attach (GTK_TABLE (t), l, 0, 1, 0, 1, GTK_EXPAND | GTK_FILL, GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
	gtk_widget_show (l);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy ((GtkScrolledWindow *) sw, GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_table_attach (GTK_TABLE (t), sw, 0, 1, 1, 2, GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
	gtk_widget_show (sw);

	cl = gtk_clist_new (1);
	gtk_clist_set_selection_mode ((GtkCList *) cl, GTK_SELECTION_SINGLE);
	gtk_clist_column_titles_hide ((GtkCList *) cl);
	gtk_container_add (GTK_CONTAINER (sw), cl);
	gtk_widget_show (cl);
	gtk_signal_connect (GTK_OBJECT (cl), "select_row", GTK_SIGNAL_FUNC (gpa_apw_select_vendor), gpa);
	gtk_signal_connect (GTK_OBJECT (cl), "unselect_row", GTK_SIGNAL_FUNC (gpa_apw_unselect_vendor), gpa);
	gtk_object_set_data (GTK_OBJECT (gpa), "manufacturer_list", cl);

	l = gtk_label_new (_("Model"));
	gtk_misc_set_alignment (GTK_MISC (l), 0.0, 0.5);
	gtk_table_attach (GTK_TABLE (t), l, 1, 2, 0, 1, GTK_EXPAND | GTK_FILL, GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
	gtk_widget_show (l);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy ((GtkScrolledWindow *) sw, GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_table_attach (GTK_TABLE (t), sw, 1, 2, 1, 2, GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
	gtk_widget_show (sw);

	cl = gtk_clist_new (1);
	gtk_clist_set_selection_mode ((GtkCList *) cl, GTK_SELECTION_SINGLE);
	gtk_clist_column_titles_hide ((GtkCList *) cl);
	gtk_container_add (GTK_CONTAINER (sw), cl);
	gtk_widget_show (cl);
	gtk_signal_connect (GTK_OBJECT (cl), "select_row", GTK_SIGNAL_FUNC (gpa_apw_select_model), gpa);
	gtk_signal_connect (GTK_OBJECT (cl), "unselect_row", GTK_SIGNAL_FUNC (gpa_apw_unselect_model), gpa);
	gtk_object_set_data (GTK_OBJECT (gpa), "model_list", cl);

	hb = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_box_pack_start (GTK_BOX (vb), hb, FALSE, FALSE, GNOME_PAD_SMALL);
	gtk_widget_show (hb);

	l = gtk_label_new (_("Name"));
	gtk_misc_set_alignment (GTK_MISC (l), 1.0, 0.5);
	gtk_box_pack_start (GTK_BOX (hb), l, FALSE, FALSE, GNOME_PAD_SMALL);
	gtk_widget_show (l);

	e = gtk_entry_new ();
	gtk_box_pack_start (GTK_BOX (hb), e, TRUE, TRUE, GNOME_PAD_SMALL);
	gtk_widget_show (e);
	gtk_object_set_data (GTK_OBJECT (gpa), "name_entry", e);

	gpa_apw_read_vendors (GPA_WIDGET (gpa));

	return gpa;
}

static void
gpa_apw_select_vendor (GtkCList *clist, gint row, gint column, GdkEvent *event, GPAWidget *gpaw)
{
	GPANode *vendor;

	vendor = gtk_clist_get_row_data (clist, row);
	g_return_if_fail (vendor != NULL);

	gtk_object_set_data (GTK_OBJECT (gpaw), "vendor", vendor);

	gpa_apw_read_models (gpaw, vendor);
}

static void
gpa_apw_unselect_vendor (GtkCList *clist, gint row, gint column, GdkEvent *event, GPAWidget *gpaw)
{
	GtkCList *mlist;

	gtk_object_set_data (GTK_OBJECT (gpaw), "vendor", NULL);

	mlist = gtk_object_get_data (GTK_OBJECT (gpaw), "model_list");
	g_assert (GTK_IS_CLIST (mlist));
	gtk_clist_clear (mlist);
}

static void
gpa_apw_select_model (GtkCList *clist, gint row, gint column, GdkEvent *event, GPAWidget *gpaw)
{
	GPANode *model;
	gchar *name;
	GtkEntry *entry;

	entry = gtk_object_get_data (GTK_OBJECT (gpaw), "name_entry");
	g_assert (GTK_IS_ENTRY (entry));

	model = gtk_clist_get_row_data (clist, row);
	g_return_if_fail (GPA_IS_NODE (model));
	name = gpa_node_get_path_value (model, "Name");
	g_return_if_fail (name != NULL);

	gtk_object_set_data (GTK_OBJECT (gpaw), "model", model);

	gtk_entry_set_text (entry, name);

	g_free (name);
}

static void
gpa_apw_unselect_model (GtkCList *clist, gint row, gint column, GdkEvent *event, GPAWidget *gpaw)
{
	GtkEntry *entry;

	gtk_object_set_data (GTK_OBJECT (gpaw), "vendor", NULL);

	entry = gtk_object_get_data (GTK_OBJECT (gpaw), "name_entry");
	g_assert (GTK_IS_ENTRY (entry));

	gtk_entry_set_text (entry, "");
}

static void
gpa_apw_read_vendors (GPAWidget *gpaw)
{
	GtkCList *clist;
	GPANode *vendors, *vendor;

	clist = gtk_object_get_data (GTK_OBJECT (gpaw), "manufacturer_list");
	g_assert (GTK_IS_CLIST (clist));

	gtk_clist_clear (clist);

	vendors = gpa_node_get_path_node (gpaw->node, "Globals.Vendors");
	g_return_if_fail (vendors != NULL);

	gtk_clist_freeze (clist);

	vendor = gpa_node_get_child (vendors, NULL);
	while (vendor != NULL) {
		GPANode *next;
		gchar *name;
		name = gpa_node_get_path_value (vendor, "Name");
		if (name != NULL) {
			gint row;
			row = gtk_clist_append (clist, &name);
			gpa_node_ref (vendor);
			gtk_clist_set_row_data_full (clist, row, vendor, (GtkDestroyNotify) gtk_object_unref);
			g_free (name);
		}
		next = gpa_node_get_child (vendors, vendor);
		gpa_node_unref (vendor);
		vendor = next;
	}
	gpa_node_unref (vendors);

	gtk_clist_thaw (clist);
}

static void
gpa_apw_read_models (GPAWidget *gpaw, GPANode *vendor)
{
	GtkCList *clist;
	GPANode *models, *model;

	clist = gtk_object_get_data (GTK_OBJECT (gpaw), "model_list");
	g_return_if_fail (clist != NULL);
	g_return_if_fail (GTK_IS_CLIST (clist));

	gtk_clist_clear (clist);

	models = gpa_node_get_path_node (vendor, "Models");
	g_return_if_fail (models != NULL);

	gtk_clist_freeze (clist);

	model = gpa_node_get_child (models, NULL);
	while (model != NULL) {
		GPANode *next;
		gchar *name;
		name = gpa_node_get_path_value (model, "Name");
		if (name != NULL) {
			gint row;
			row = gtk_clist_append (clist, &name);
			gpa_node_ref (model);
			gtk_clist_set_row_data_full (clist, row, model, (GtkDestroyNotify) gtk_object_unref);
			g_free (name);
		}
		next = gpa_node_get_child (models, model);
		gpa_node_unref (model);
		model = next;
	}
	gpa_node_unref (models);

	gtk_clist_thaw (clist);
}

GtkWidget *
gpa_add_printer_dialog_new (GPANode *node)
{
	GtkWidget *dialog;
	GtkWidget *gpa;
	GtkWidget *c, *b;

	dialog = gtk_dialog_new ();
	gtk_window_set_title (GTK_WINDOW (dialog), _("Add printer"));
	gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);

	c = ((GtkDialog *) dialog)->vbox;
	gtk_container_set_border_width (GTK_CONTAINER (c), GNOME_PAD_SMALL);

	gpa = gpa_add_printer_widget_new (node);
	gtk_box_pack_start (GTK_BOX (c), GTK_WIDGET (gpa), TRUE, TRUE, 4);
	gtk_widget_show (GTK_WIDGET (gpa));
	gtk_object_set_data (GTK_OBJECT (dialog), "gpawidget", gpa);

	c = ((GtkDialog *) dialog)->action_area;
	b = gtk_button_new_with_label (_("From PPD..."));
	gtk_box_pack_start (GTK_BOX (c), b, FALSE, TRUE, GNOME_PAD_SMALL);
	gtk_widget_show (b);
	b = gnome_stock_button (GNOME_STOCK_BUTTON_OK);
	gtk_box_pack_start (GTK_BOX (c), b, FALSE, TRUE, GNOME_PAD_SMALL);
	gtk_signal_connect (GTK_OBJECT (b), "clicked", GTK_SIGNAL_FUNC (gpa_apd_ok_clicked), dialog);
	gtk_widget_show (b);
	b = gnome_stock_button (GNOME_STOCK_BUTTON_CANCEL);
	gtk_box_pack_start (GTK_BOX (c), b, FALSE, TRUE, GNOME_PAD_SMALL);
	gtk_signal_connect (GTK_OBJECT (b), "clicked", GTK_SIGNAL_FUNC (gpa_apd_cancel_clicked), dialog);
	gtk_widget_show (b);

	return dialog;
}

static void
gpa_apd_ok_clicked (GtkButton *button, GtkWidget *apd)
{
	GPANode *model, *printer;
	GPANode *root;
	GPAWidget *gpaw;
	GtkEntry *entry;
	gchar *name;

	gpaw = gtk_object_get_data (GTK_OBJECT (apd), "gpawidget");
	g_assert (GPA_IS_WIDGET (gpaw));
	entry = gtk_object_get_data (GTK_OBJECT (gpaw), "name_entry");
	g_assert (GTK_IS_ENTRY (entry));
	model = gtk_object_get_data (GTK_OBJECT (gpaw), "model");
	g_return_if_fail (GPA_IS_NODE (model));

	name = gtk_entry_get_text (entry);
	g_return_if_fail (name != NULL);
	g_return_if_fail (*name != '\0');

	root = gpa_node_get_path_node (gpaw->node, "Globals");
	g_return_if_fail (root != NULL);

	/* fixme: test name clash */
	/* fixme: implement ing libgpa */
	printer = gpa_printer_new_from_model (GPA_MODEL (model), name);
	g_return_if_fail (printer != NULL);

	gpa_printer_save (GPA_PRINTER (printer));

	gtk_widget_destroy (GTK_WIDGET (apd));
}

static void
gpa_apd_cancel_clicked (GtkButton *button, GtkWidget *apd)
{
	gtk_widget_destroy (GTK_WIDGET (apd));
}


#define __GPA_MEDIA_ORIENTATION_C__

/*
 * GPAMediaOrientationWidget
 *
 * Selector for papaer orientation
 *
 * Author:
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 */

#include <string.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkcheckbutton.h>
#include <gtk/gtkradiobutton.h>
#include <gtk/gtkvbox.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-i18n.h>
#include <libgnomeui/gnome-uidefs.h>
#include "media-orientation.h"

static void gpa_mow_changed (GtkWidget *w, GPAMediaOrientationWidget *mow);
static void gpa_mow_config_changed (GPANode *node, GPAMediaOrientationWidget *mow);

GtkWidget *
gpa_media_orientation_widget_new (GPANode *node)
{
	GPAMediaOrientationWidget *mow;

	g_return_val_if_fail ((!node) || GPA_IS_NODE (node), NULL);

	mow = gtk_type_new (GPA_TYPE_MEDIA_ORIENTATION_WIDGET);

	if (node) gpa_media_orientation_widget_construct (mow, node);

	return GTK_WIDGET (mow);
}

void
gpa_media_orientation_widget_construct (GPAMediaOrientationWidget *mow, GPANode *node)
{
	GtkWidget *vb, *b;
	GSList *group;

	g_return_if_fail (mow != NULL);
	g_return_if_fail (GPA_IS_MEDIA_ORIENTATION_WIDGET (mow));
	g_return_if_fail (node != NULL);
	g_return_if_fail (GPA_IS_NODE (node));

	gpa_widget_construct (GPA_WIDGET (mow), node);

	vb = gtk_vbox_new (TRUE, GNOME_PAD_SMALL);
	gtk_container_add (GTK_CONTAINER (mow), vb);

	b = gtk_radio_button_new_with_label (NULL, _("Portrait"));
	group = gtk_radio_button_group (GTK_RADIO_BUTTON (b));
	gtk_box_pack_start (GTK_BOX (vb), b, TRUE, FALSE, GNOME_PAD_SMALL);
	gtk_object_set_data (GTK_OBJECT (mow), "portrait", b);
	gtk_signal_connect (GTK_OBJECT (b), "toggled", GTK_SIGNAL_FUNC (gpa_mow_changed), mow);
	b = gtk_radio_button_new_with_label (group, _("Landscape"));
	group = gtk_radio_button_group (GTK_RADIO_BUTTON (b));
	gtk_box_pack_start (GTK_BOX (vb), b, TRUE, FALSE, GNOME_PAD_SMALL);
	gtk_object_set_data (GTK_OBJECT (mow), "landscape", b);
	gtk_signal_connect (GTK_OBJECT (b), "toggled", GTK_SIGNAL_FUNC (gpa_mow_changed), mow);
	b = gtk_check_button_new_with_label (_("Rotate 180"));
	gtk_box_pack_start (GTK_BOX (vb), b, TRUE, FALSE, GNOME_PAD_SMALL);
	gtk_object_set_data (GTK_OBJECT (mow), "rotate", b);
	gtk_signal_connect (GTK_OBJECT (b), "toggled", GTK_SIGNAL_FUNC (gpa_mow_changed), mow);
	gtk_widget_show_all (vb);

	gtk_signal_connect_while_alive (GTK_OBJECT (node), "modified", GTK_SIGNAL_FUNC (gpa_mow_config_changed), mow, GTK_OBJECT (mow));

	gpa_mow_changed (NULL, mow);
}

static void
gpa_mow_changed (GtkWidget *w, GPAMediaOrientationWidget *mow)
{
	gpointer b;
	gboolean portrait, rotate;
	const gchar *v;

	b = gtk_object_get_data (GTK_OBJECT (mow), "portrait");
	portrait = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b));
	b = gtk_object_get_data (GTK_OBJECT (mow), "rotate");
	rotate = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (b));

	if (portrait) {
		if (rotate) {
			v = "ReversePortrait";
		} else {
			v = "Portrait";
		}
	} else {
		if (rotate) {
			v = "ReverseLandscape";
		} else {
			v = "Landscape";
		}
	}

	if (!gpa_node_set_path_value (GPA_WIDGET (mow)->node, "Settings.Media.Orientation", v)) {
		g_warning ("Cannot set media orientation value");
	}
}

static void
gpa_mow_config_changed (GPANode *node, GPAMediaOrientationWidget *mow)
{
	gpointer b;
	gboolean portrait, rotate;
	const gchar *v;

	v = gpa_node_get_path_value (GPA_WIDGET (mow)->node, "Settings.Media.Orientation");
	if (v == NULL) {
		g_warning ("Cannot get media orientation value");
		return;
	} else if (!strcmp (v, "Portrait")) {
		portrait = TRUE;
		rotate = FALSE;
	} else if (!strcmp (v, "ReversePortrait")) {
		portrait = TRUE;
		rotate = TRUE;
	} else if (!strcmp (v, "Landscape")) {
		portrait = FALSE;
		rotate = FALSE;
	} else if (!strcmp (v, "ReverseLandscape")) {
		portrait = FALSE;
		rotate = TRUE;
	} else {
		g_warning ("Illegal orientation value");
		return;
	}

	if (portrait) {
		b = gtk_object_get_data (GTK_OBJECT (mow), "portrait");
	} else {
		b = gtk_object_get_data (GTK_OBJECT (mow), "landscape");
	}
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (b), TRUE);
	b = gtk_object_get_data (GTK_OBJECT (mow), "rotate");
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (b), rotate);
}

#ifndef __GNOME_GLYPHLIST_H__
#define __GNOME_GLYPHLIST_H__

/*
 *  Copyright (C) 2000-2001 Ximian Inc. and authors
 *
 *  Authors:
 *    Lauris Kaplinski <lauris@ximian.com>
 *
 *  Experimental device independent rich text representation system
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#ifndef BREAK_COMPATIBILITY
#define GNOME_TYPE_GLYPHLIST (gnome_glyphlist_get_type ())
#define GNOME_GLYPHLIST(obj) (GTK_CHECK_CAST ((obj), GNOME_TYPE_GLYPHLIST, GnomeGlyphList))
#define GNOME_GLYPHLIST_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_GLYPHLIST, GnomeGlyphListClass))
#define GNOME_IS_GLYPHLIST(obj) (GTK_CHECK_TYPE ((obj), GNOME_TYPE_GLYPHLIST))
#define GNOME_IS_GLYPHLIST_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_GLYPHLIST))
#endif

typedef struct _GnomeGlyphList		GnomeGlyphList;
#ifndef BREAK_COMPATIBILITY
typedef struct _GnomeGlyphListClass	GnomeGlyphListClass;
#endif

#include <libgnomeprint/gnome-font.h>

#ifndef BREAK_COMPATIBILITY
/* The one and only Gtk+ type */
GtkType gnome_glyphlist_get_type (void);
/* Methods */
#define gnome_glyphlist_ref(o) gtk_object_ref (GTK_OBJECT (o))
#define gnome_glyphlist_unref(o) gtk_object_unref (GTK_OBJECT (o))
#endif

/*
 * the _dumb_ versions of glyphlist creation
 * It just places glyphs one after another - no ligaturing etc.
 * text is utf8, of course
 */

GnomeGlyphList *gnome_glyphlist_from_text_dumb (GnomeFont *font, guint32 color,
						gdouble kerning, gdouble letterspace,
						const guchar *text);

GnomeGlyphList *gnome_glyphlist_from_text_sized_dumb (GnomeFont *font, guint32 color,
						      gdouble kerning, gdouble letterspace,
						      const guchar *text, gint length);

/*
 * Well - the API is slow and dumb. Hopefully you all will be composing
 * glyphlists by hand in future...
 */

void gnome_glyphlist_glyph (GnomeGlyphList *gl, gint glyph);
void gnome_glyphlist_glyphs (GnomeGlyphList *gl, gint *glyphs, gint num_glyphs);
void gnome_glyphlist_advance (GnomeGlyphList *gl, gboolean advance);
void gnome_glyphlist_moveto (GnomeGlyphList *gl, gdouble x, gdouble y);
void gnome_glyphlist_rmoveto (GnomeGlyphList *gl, gdouble x, gdouble y);
void gnome_glyphlist_font (GnomeGlyphList *gl, GnomeFont * font);
void gnome_glyphlist_color (GnomeGlyphList *gl, guint32 color);
void gnome_glyphlist_kerning (GnomeGlyphList *gl, gdouble kerning);
void gnome_glyphlist_letterspace (GnomeGlyphList *gl, gdouble letterspace);

void gnome_glyphlist_text_dumb (GnomeGlyphList *gl, const gchar *text);
void gnome_glyphlist_text_sized_dumb (GnomeGlyphList *gl, const gchar *text, gint length);

END_GNOME_DECLS

#endif /* __GNOME_GLYPHLIST_H__ */



























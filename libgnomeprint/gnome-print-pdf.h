#ifndef __GNOME_PRINT_PDF_H__
#define __GNOME_PRINT_PDF_H__

/*
 * gnome-print-pdf.h: A PDF driver for gnome print based on
 * original PS driver by Raph Levien.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * References :
 * [1] Portable Document Format Referece Manual, Version 1.3 (March 11, 1999)
 *
 * Authors:
 *   Chema Celorio <chema@celorio.com>
 *   Lauris Kaplinski <lauris@helixcode.com>
 *
 * Copyright 2000-2001 Ximian, Inc. and authors
 *
 */

#include <libgnomeprint/gnome-print.h>
#include "gpa-node.h"

BEGIN_GNOME_DECLS

#define GNOME_TYPE_PRINT_PDF (gnome_print_pdf_get_type ())
#define GNOME_PRINT_PDF(obj) (GTK_CHECK_CAST ((obj), GNOME_TYPE_PRINT_PDF, GnomePrintPdf))
#define GNOME_PRINT_PDF_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_PRINT_PDF, GnomePrintPdfClass))
#define GNOME_IS_PRINT_PDF(obj) (GTK_CHECK_TYPE ((obj), GNOME_TYPE_PRINT_PDF))
#define GNOME_IS_PRINT_PDF_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_PRINT_PDF))

typedef struct _GnomePrintPdf GnomePrintPdf;
typedef struct _GnomePrintPdfClass GnomePrintPdfClass;
typedef struct _GnomePrintPdfFont GnomePrintPdfFont;
typedef struct _T1Glyph T1Glyph;

struct _GnomePrintPdfFont {
	GnomeFont *gnome_font;
	T1Glyph *glyphs;
	gint glyphs_num;
	gint glyphs_max;

	gint font_number;
	gchar *font_name;
	gboolean is_basic_14;

	guint object_number;
	guint object_number_descriptor;
	guint object_number_encoding;
	guint object_number_pfb;
};

struct _T1Glyph {
	gchar *name;
	guchar *crypted;
	gint crypted_size;
	guchar *uncrypted;
	gint uncrypted_size;
};

GtkType gnome_print_pdf_get_type (void);

GnomePrintContext *gnome_print_pdf_new (GPANode *config);

/* fixme: audit these */
gint gnome_print_pdf_object_end (GnomePrintContext *pc, guint object_number, guint dont_print);
gint gnome_print_pdf_object_start (GnomePrintContext *pc, guint object_number);

gint gnome_print_pdf_add_bytes_written (GnomePrintPdf *pdf, gint bytes);

gint gnome_print_pdf_write (GnomePrintContext *pc, const char *format, ...);

END_GNOME_DECLS

#endif /* __GNOME_PRINT_PDF_H__ */

#ifndef __GNOME_PRINT_PIXBUF_H__
#define __GNOME_PRINT_PIXBUF_H__

/*
 *  Copyright (C) 1999-2001 Ximian Inc. and authors
 *
 *  Authors:
 *    Miguel de Icaza (miguel@gnu.org)
 *    Lauris Kaplinski (lauris@ariman.ee)
 *
 *  Driver that generates GdkPixbuf image from each page
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#define GNOME_TYPE_PRINT_PIXBUF (gnome_print_pixbuf_get_type ())
#define GNOME_PRINT_PIXBUF(obj) (GTK_CHECK_CAST ((obj), GNOME_TYPE_PRINT_PIXBUF, GnomePrintPixbuf))
#define GNOME_PRINT_PIXBUF_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_PRINT_PIXBUF, GnomePrintPixbufClass))
#define GNOME_IS_PRINT_PIXBUF(obj) (GTK_CHECK_TYPE ((obj), GNOME_TYPE_PRINT_PIXBUF))
#define GNOME_IS_PRINT_PIXBUF_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_PRINT_PIXBUF))

typedef struct _GnomePrintPixbuf GnomePrintPixbuf;
typedef struct _GnomePrintPixbufClass GnomePrintPixbufClass;

#include <libgnomeprint/gnome-print.h>

/*
 * The easiest way to get pixbufs is to connect signal handler to
 * "showpixbuf" signal of GnomePrintPixbuf object, with signature:
 *
 * void (* showpixbuf) (GnomePrintPixbuf *gpb, GdkPixbuf *pixbuf, gint pagenum);
 *
 * It will be called in each showpage, with copy of internal pixbuf, so you can
 * simply ref it, and use in your application. But beware that although same pixbuf
 * is not shared with context itself, it is shared by all signal handlers.
 */

GtkType gnome_print_pixbuf_get_type (void);

/* Creates new GnomePrintPixbuf context */
GnomePrintContext *gnome_print_pixbuf_new (gdouble x0, gdouble y0,
					   gdouble x1, gdouble y1,
					   gdouble xdpi, gdouble ydpi,
					   gboolean alpha);
#ifndef BREAK_COMPATIBILITY
GnomePrintContext *gnome_print_pixbuf_construct (GnomePrintPixbuf * gpb,
						 gdouble x0, gdouble y0,
						 gdouble x1, gdouble y1,
						 gdouble xdpi, gdouble ydpi,
						 gboolean alpha);
#endif

/* Get pointer to shared pixbuf structure */
GdkPixbuf *gnome_print_pixbuf_get_pixbuf (GnomePrintPixbuf *gpb);
/* Get current page number */
gint gnome_print_pixbuf_get_pagenum (GnomePrintPixbuf *gpb);

END_GNOME_DECLS

#endif /* __GNOME_PRINT_PIXBUF_H__ */


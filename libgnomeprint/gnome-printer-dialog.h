#ifndef __GNOME_PRINTER_DIALOG_H__
#define __GNOME_PRINTER_DIALOG_H__

/*
 *  Copyright (C) 1999-2001 Ximian Inc. and authors
 *
 *  Authors:
 *    Raph Levien (raph@acm.org)
 *    Miguel de Icaza (miguel@kernel.org)
 *    Lauris Kaplinski <lauris@ximian.com>
 *    Chema Celorio (chema@celorio.com)
 *
 *  Printer selection widget and dialog
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#include <libgnomeui/gnome-dialog.h>
#include <libgnomeprint/gnome-printer.h>

#define GNOME_TYPE_PRINTER_WIDGET (gnome_printer_widget_get_type ())
#define GNOME_PRINTER_WIDGET(obj) (GTK_CHECK_CAST ((obj), GNOME_TYPE_PRINTER_WIDGET, GnomePrinterWidget))
#define GNOME_PRINTER_WIDGET_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_PRINTER_WIDGET, GnomePrinterWidgetClass))
#define GNOME_IS_PRINTER_WIDGET(obj) (GTK_CHECK_TYPE ((obj), GNOME_TYPE_PRINTER_WIDGET))
#define GNOME_IS_PRINTER_WIDGET_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_PRINTER_WIDGET))

typedef struct _GnomePrinterWidget GnomePrinterWidget;
typedef struct _GnomePrinterWidgetClass GnomePrinterWidgetClass;

GtkType gnome_printer_widget_get_type (void);

GtkWidget *gnome_printer_widget_new (void);

#ifdef BRAVE_NEW_WORLD
GtkWidget *gnome_printer_widget_new_from_node (GPANode *root);
#else
GtkWidget *gnome_printer_widget_new_from_node (GnomePrinter *printer);
#endif

void gnome_printer_widget_bind_editable_enters (GnomePrinterWidget *gpw, GnomeDialog *dialog);
void gnome_printer_widget_bind_accel_group (GnomePrinterWidget *gpw, GtkWindow *window);

GnomePrinter *gnome_printer_widget_get_printer (GnomePrinterWidget *widget);

#define GNOME_TYPE_PRINTER_DIALOG (gnome_printer_dialog_get_type ())
#define GNOME_PRINTER_DIALOG(obj) (GTK_CHECK_CAST ((obj), GNOME_TYPE_PRINTER_DIALOG, GnomePrinterDialog))
#define GNOME_PRINTER_DIALOG_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_PRINTER_DIALOG, GnomePrinterDialogClass))
#define GNOME_IS_PRINTER_DIALOG(obj) (GTK_CHECK_TYPE ((obj), GNOME_TYPE_PRINTER_DIALOG))
#define GNOME_IS_PRINTER_DIALOG_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_PRINTER_DIALOG))

typedef struct _GnomePrinterDialog GnomePrinterDialog;
typedef struct _GnomePrinterDialogClass GnomePrinterDialogClass;

GtkType gnome_printer_dialog_get_type (void);

GtkWidget *gnome_printer_dialog_new (void);
GnomePrinter *gnome_printer_dialog_new_modal (void);

GnomePrinter *gnome_printer_dialog_get_printer (GnomePrinterDialog *dialog);

END_GNOME_DECLS

#endif /* __GNOME_PRINTER_DIALOG_H__ */

#ifndef __GNOME_FONT_H__
#define __GNOME_FONT_H__

/*
 *  Copyright (C) 2000-2001 Ximian Inc. and authors
 *
 *  Authors:
 *    Jody Goldberg <jody@ximian.com>
 *    Miguel de Icaza <miguel@ximian.com>
 *    Lauris Kaplinski <lauris@ximian.com>
 *    Christopher James Lahey <clahey@ximian.com>
 *    Michael Meeks <michael@ximian.com>
 *    Morten Welinder <terra@diku.dk>
 *
 *  GnomeFont - basic user visible handle to scaled typeface
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * I am not sure whether to preserve these, but they remain until binary break (Lauris)
 *
 * The following arguments are available:
 *
 * name		       type		read/write	description
 * --------------------------------------------------------------------------------
 * ascender            double           R               maximum ascender for the face
 * descender           double           R               maximum descender for the face
 * underline_position  double           R               underline position for the face
 * underline_thickness double           R               underline thickness for the face
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#define GNOME_TYPE_FONT (gnome_font_get_type ())
#define GNOME_FONT(obj) (GTK_CHECK_CAST ((obj), GNOME_TYPE_FONT, GnomeFont))
#define GNOME_FONT_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_FONT, GnomeFontClass))
#define GNOME_IS_FONT(obj) (GTK_CHECK_TYPE ((obj), GNOME_TYPE_FONT))
#define GNOME_IS_FONT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_FONT))

typedef struct _GnomeFont GnomeFont;
typedef struct _GnomeFontClass GnomeFontClass;

#ifndef BREAK_COMPATIBILITY
typedef struct _GnomeFontPrivate GnomeFontPrivate;
typedef struct _GnomeFontClassPrivate GnomeFontClassPrivate;
#endif

#include <gdk/gdk.h>
#include <libgnomeprint/gnome-font-face.h>

#ifndef BREAK_COMPATIBILITY
#include <libgnomeprint/gnome-font-compat.h>
#endif

/* The one and only Gtk+ type */
GtkType gnome_font_get_type (void);

#define gnome_font_ref(f) gtk_object_ref (GTK_OBJECT (f))
#define gnome_font_unref(f) gtk_object_unref (GTK_OBJECT (f))

/*
 * Methods
 *
 * Look into gnome-font-face for explanations
 */

/* Naming */
const gchar *gnome_font_get_name (const GnomeFont *font);
const gchar *gnome_font_get_family_name (const GnomeFont *font);
const gchar *gnome_font_get_species_name (const GnomeFont *font);
const gchar *gnome_font_get_ps_name (const GnomeFont *font);

/* Unicode -> glyph translation */
gint gnome_font_lookup_default (const GnomeFont *font, gint unicode);

/*
 * Metrics
 *
 * Note that GnomeFont metrics are given in typographic points
 */
ArtPoint *gnome_font_get_glyph_stdadvance (const GnomeFont *font, gint glyph, ArtPoint * advance);
ArtDRect *gnome_font_get_glyph_stdbbox (const GnomeFont *font, gint glyph, ArtDRect * bbox);
const ArtBpath *gnome_font_get_glyph_stdoutline (const GnomeFont *font, gint glyph);

/*
 * Backward compatibility and convenience methods
 *
 * NB! Those usually do not scale for international fonts, so use with
 * caution.
 */
gdouble gnome_font_get_ascender (const GnomeFont * font);
gdouble gnome_font_get_descender (const GnomeFont * font);
gdouble gnome_font_get_underline_position (const GnomeFont * font);
gdouble gnome_font_get_underline_thickness (const GnomeFont * font);

gdouble gnome_font_get_glyph_width (const GnomeFont * font, gint glyph);
gdouble gnome_font_get_glyph_kerning (const GnomeFont * font, gint glyph1, gint glyph2);

#ifndef BREAK_COMPATIBILITY
/* This will be removed ASAP (Lauris) */
gchar * gnome_font_get_pfa (const GnomeFont * font);
#endif

const GnomeFontFace *gnome_font_get_face (const GnomeFont *font);
gdouble gnome_font_get_size (const GnomeFont *font);

/*
 * Font fetching
 */

GnomeFont *gnome_font_new (const gchar *name, gdouble size);
GnomeFont *gnome_font_new_closest (const gchar *family_name, GnomeFontWeight weight, gboolean italic, gdouble size);
GnomeFont *gnome_font_new_from_full_name (const gchar *string);

/*
 * Font browsing
 */

/* Return a list of fonts, as a g_list of strings */

GList * gnome_font_list (void);
void gnome_font_list_free (GList *fontlist);

/* Return a list of font families, as a g_list of newly allocated strings */

GList * gnome_font_family_list (void);
void gnome_font_family_list_free (GList *fontlist);

#ifndef BREAK_COMPATIBILITY
/*
 * Compatibility
 */
/* typedef struct
 * {
 *  GnomeFontUnsized *unsized_font;   font before scaling
 *  GnomeFont *gnome_font;            scaled font
 *  double scale;                     scaling factor requested by a view
 * 
 *  char *x_font_name;                x name that got us gdk_font
 *  GdkFont *gdk_font;                used for drawing
 * } GnomeDisplayFont;
 */

typedef struct _GnomeRFont GnomeDisplayFont;

GnomeDisplayFont * gnome_font_get_display_font (GnomeFont *font);
const char * gnome_font_weight_to_string (GnomeFontWeight gfw);
GnomeFontWeight string_to_gnome_font_weight (const char *weight);
GnomeDisplayFont * gnome_get_display_font (const char * family, GnomeFontWeight weight, gboolean italic, double points, double scale);
int gnome_display_font_height (GnomeDisplayFont * gdf);

/*
 * These are for attribute reading
 *
 * If writing fresh code, use rfont methods directly
 */
const GnomeFontFace * gnome_display_font_get_face (const GnomeDisplayFont * gdf);
const GnomeFont * gnome_display_font_get_font (const GnomeDisplayFont * gdf);
gdouble gnome_display_font_get_scale (const GnomeDisplayFont * gdf);
const gchar * gnome_display_font_get_x_font_name (const GnomeDisplayFont * gdf);
GdkFont * gnome_display_font_get_gdk_font (const GnomeDisplayFont * gdf);

void gnome_display_font_ref (GnomeDisplayFont * gdf);
void gnome_display_font_unref (GnomeDisplayFont * gdf);
#endif

/* Misc */
gchar * gnome_font_get_full_name (const GnomeFont *font);

/*
 * We keep these at moment, but in future better go with Pango/glyphlists
 */

/* these routines are fast; go ahead and use them */

#ifndef BREAK_COMPATIBILITY
/*
 * WARNING WARNING WARNING
 *
 * These functions expect string to be iso-8859-1
 *
 * WARNING WARNING WARNING
 */
double gnome_font_get_width_string (const GnomeFont *font, const char *s);
double gnome_font_get_width_string_n (const GnomeFont *font, const char *s, int n);
#endif

#ifndef BREAK_COMPATIBILITY
/* Normal utf8 functions */
/* These are still crap, as you cannot expect ANYTHING about layouting rules */
double gnome_font_get_width_utf8 (const GnomeFont *font, const char *s);
double gnome_font_get_width_utf8_sized (const GnomeFont *font, const char *s, int n);
#endif

END_GNOME_DECLS

#endif /* __GNOME_FONT_H__ */




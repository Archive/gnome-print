#define __GNOME_PRINT_PDF_C__

/*
 * gnome-print-pdf.h: A PDF driver for gnome print based on
 * original PS driver by Raph Levien.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * References :
 * [1] Portable Document Format Referece Manual, Version 1.3 (March 11, 1999)
 *
 * Authors:
 *   Chema Celorio <chema@celorio.com>
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright 2000-2001 Ximian, Inc. and authors
 *
 */

/*
	This driver needs some love. I need to rewrite parts of it.
	This is the stuff I plan to do :

	1. Remove the _moveto _lineto _curveto etc .. functions.
	us gp_gc instead.

	2. We are sucking memory when printing images, althou this is
	not a problem with your normal Image, for large images is.
	We need to modify the gnome_print_encode functions so that we can
	use them "blockified" so we would do something like :

	temp = g_malloc (SOME_CONSTANT_A);
	gnome_print_encode_ascii85_ini (SOME_CONSTANT_A);
	while (offset < size) {
	   gnome_print_encode_ascii85_body (buffer_in, offset, temp)
		 gnome_print_pdf_write (temp, SOME_CONSTANT_A)
		 offset += SOME_CONSTANT_A;
 }
 gnome_print_endode_ascii85_end (); (or flush ? )

 and thus only using SOME_CONSTANT_A memory for the process.

 3. Move the gnome-print images printing to gdk_pixbufs. Ref them
 use them and unref them. For the old functions, create a pixbuf,
 print with it and destroy it.

 4. Finish the Font subseting code for type1 fonst. (I think we can also
 compress the fonts with /Filter .


 5. (minor) Add page name to pdf file (set by begin page)
 6. (minor) Add /Creator ( program name that created the PDF (ie. "gedit", "gnumeric")

 7. Add JPEG Compression. And CCTII (or whatever) compression for images. gdk_pixbuf
 might do them for us.

 8. For non ortogonal scales, we are not honoring the scale factors (see setlinewidth
 for example)

 9. We need to be able to start writing to disk the content of the pages, we need to
 compress them so this can be tricky but fun :-).
 
 Chema
 (Nov 27, 2000)

*/

/* __FUNCTION__ is not defined in Irix. thanks, <drk@sgi.com>*/
#ifndef __GNUC__
  #define __FUNCTION__   ""
#endif
#define debug(section,str) if (FALSE) printf ("%s:%d (%s) %s\n", __FILE__, __LINE__, __FUNCTION__, str); 
	
#include "config.h"
#include <unistd.h> /* For getpid () */
#include <time.h>   /* For time() */
#include <gtk/gtk.h>
#include <string.h>
#include <math.h>
#include <locale.h>

#include <libart_lgpl/art_affine.h>
#include <libart_lgpl/art_misc.h>
#include <libgnome/gnome-paper.h>
#include <libgnomeprint/gp-unicode.h>
#include <libgnomeprint/gnome-print-pdf.h>
#include <libgnomeprint/gnome-print-private.h>
#include <libgnomeprint/gnome-printer-private.h>
#include <libgnomeprint/gnome-font.h>
#include <libgnomeprint/gnome-print-encode-private.h>
#include <libgnomeprint/gnome-print-pdf-type1.h>

#include "gnome-pgl-private.h"

#define EOL "\r\n"
#define GNOME_PRINT_PDF_BUFFER_SIZE 1024

#if 1 /* Use a very low number of elements so that we reallocate stuff
				 and test the code for array growing */
#define GNOME_PRINT_NUMBER_OF_ELEMENTS 2
#define GNOME_PRINT_NUMBER_OF_ELEMENTS_GROW 2
#else
#define GNOME_PRINT_NUMBER_OF_ELEMENTS 16
#define GNOME_PRINT_NUMBER_OF_ELEMENTS_GROW 8
#endif

#define GNOME_PRINT_PDF_FONT_UNDEFINED 9999

typedef struct _GPPDFImage GPPDFImage;
typedef struct _GPPDFPage GPPDFPage;

typedef struct _GnomePrintPdfObject       GnomePrintPdfObject;
typedef struct _GnomePrintPdfGsave        GnomePrintPdfGsave;
typedef struct _GnomePrintPdfGraphicState GnomePrintPdfGraphicState;

/* These are the PostScript 35, assumed to be in the printer. */
typedef struct {
	gchar *font_name;
	gint is_basic_14;
} ps_internal_font;

static const ps_internal_font gnome_print_pdf_internal_fonts[] = {
	{  "AvantGarde-Book",              FALSE},
	{  "AvantGarde-BookOblique",       FALSE},
	{  "AvantGarde-Demi",              FALSE},
	{  "AvantGarde-DemiOblique",       FALSE},
	{  "Bookman-Demi",                 FALSE},
	{  "Bookman-DemiItalic",           FALSE},
	{  "Bookman-Light",                FALSE},
	{  "Bookman-LightItalic",          FALSE},
	{  "Courier",                      TRUE},
	{  "Courier-Bold",                 TRUE},
	{  "Courier-BoldOblique",          TRUE},
	{  "Courier-Oblique",              TRUE},
	{  "Helvetica",                    TRUE},
	{  "Helvetica-Bold",               TRUE},
	{  "Helvetica-BoldOblique",        TRUE},
	{  "Helvetica-Narrow",             FALSE},
	{  "Helvetica-Narrow-Bold",        FALSE},
	{  "Helvetica-Narrow-BoldOblique", FALSE},
	{  "Helvetica-Narrow-Oblique",     FALSE},
	{  "Helvetica-Oblique",            TRUE},
	{  "NewCenturySchlbk-Bold",        FALSE},
	{  "NewCenturySchlbk-BoldItalic",  FALSE},
	{  "NewCenturySchlbk-Italic",      FALSE},
	{  "NewCenturySchlbk-Roman",       FALSE},
	{  "Palatino-Bold",                FALSE},
	{  "Palatino-BoldItalic",          FALSE},
	{  "Palatino-Italic",              FALSE},
	{  "Palatino-Roman",               FALSE},
	{  "Symbol",                       TRUE},
	{  "Times-Bold",                   TRUE},
	{  "Times-BoldItalic",             TRUE},
	{  "Times-Italic",                 TRUE},
	{  "Times-Roman",                  TRUE},
	{  "ZapfChancery-MediumItalic",    FALSE},
	{  "ZapfDingbats",                 TRUE}
};

#if 0
typedef enum {
	PDF_COLOR_MODE_DEVICEGRAY,
	PDF_COLOR_MODE_DEVICERGB,
	PDF_COLOR_MODE_DEVICECMYK,
	PDF_COLOR_MODE_UNDEFINED
}PdfColorModes;
#endif

typedef enum {
	PDF_GRAPHIC_MODE_GRAPHICS,
	PDF_GRAPHIC_MODE_TEXT,
	PDF_GRAPHIC_MODE_UNDEFINED,
}PdfGraphicModes;

typedef enum {
	PDF_COLOR_GROUP_FILL,
	PDF_COLOR_GROUP_STROKE,
	PDF_COLOR_GROUP_BOTH
}PdfColorGroup;

typedef enum {
	PDF_IMAGE_GRAYSCALE,
	PDF_IMAGE_RGB,
}PdfImageType;

/* This are really compression/encoding methods */
typedef enum {
	PDF_COMPRESSION_NONE,
	PDF_COMPRESSION_FLATE,
	PDF_COMPRESSION_HEX
}PdfCompressionType;

struct _GnomePrintPdfObject {
	guint number;
	guint offset;
};

struct _GPPDFImage {
	GPPDFImage *next;
	gchar *data;
	gint data_length;
	gint width;
	gint height;
	gint rowstride;
	gint bytes_per_pixel;
	gint image_number;
	gint object_number;
	gint image_type;
	PdfCompressionType compr_type;
};

struct _GnomePrintPdfGsave {
	gint graphics_mode;
	GnomePrintPdfGraphicState *graphic_state;
	GnomePrintPdfGraphicState *graphic_state_set;
};

struct _GPPDFPage{
	GPPDFPage *next;

	guint shown : 1;

	gint used_color_images : 1;
	gint used_grayscale_images :1;
	
	/* Page number */
	guint page_number;
	gchar *page_name;
	
	/* object numbers */
	guint object_number_page;
	guint object_number_contents;
	guint object_number_resources;

	/* Contents */
	gchar *stream;
	gint   stream_used;
	gint   stream_allocated;

	/* Resources */
	GList *resources;

	/* Images */
	GPPDFImage *images;
};

struct _GnomePrintPdfGraphicState{
	gboolean dirty        : 1;
	gboolean written      : 1;
	gboolean dirty_text   : 1;
	gboolean written_text : 1;

#if 0
	/* CTM */
	double ctm[6];
#endif
	
#if 0
	/* Color */
	gint   color_stroke_mode;
	gint   color_fill_mode;
	double color_stroke[4];
	double color_fill  [4];
#endif
	
	/* Font */
	gint   pdf_font_number;
	double font_size;
	double font_character_spacing;
	double font_word_spacing;
	gint text_flag : 1;
	
};

struct _GnomePrintPdf {
	GnomePrintContext pc;

	gboolean ascii_format;

	/* Graphic states */
	GnomePrintPdfGraphicState *graphic_state;
	GnomePrintPdfGraphicState *graphic_state_set;
	gint graphics_mode;

	/* Offset in the output file */
	guint offset;
	
	/* Objects */
	GList *objects;
	guint object_number_last_used;
	guint object_number_pages;
	guint object_number_catalog;
	guint object_number_current;
	guint object_number_info;
	guint object_number_gstate;

	/* xref location */
	guint xref_location;

	/* Fonts */
	gint  fonts_internal_number;
	ps_internal_font *fonts_internal;
	gint fonts_max;
	gint fonts_number;
	GnomePrintPdfFont* fonts;

	/* gsave/grestore */
	gint gsave_level_number;
	gint gsave_level_max;
	GnomePrintPdfGsave *gsave_stack;

	/* Page stuff */
	GPPDFPage *pages;

	gint active_color_flag;
	gdouble r, g, b;

	ArtDRect bbox;
};

struct _GnomePrintPdfClass
{
	GnomePrintContextClass parent_class;
};

static void gnome_print_pdf_class_init (GnomePrintPdfClass *klass);
static void gnome_print_pdf_init (GnomePrintPdf *pdf);
static void gnome_print_pdf_destroy (GtkObject *object);

static gint gnome_print_pdf_construct (GnomePrintContext *ctx);
static gint gnome_print_pdf_beginpage (GnomePrintContext *pc, const char *name);
static gint gnome_print_pdf_showpage (GnomePrintContext *pc);
static gint gnome_print_pdf_gsave (GnomePrintContext *pc);
static gint gnome_print_pdf_grestore (GnomePrintContext *pc);
static gint gnome_print_pdf_clip (GnomePrintContext *ctx, const ArtBpath *bpath, ArtWindRule rule);
static gint gnome_print_pdf_fill (GnomePrintContext *ctx, const ArtBpath *bpath, ArtWindRule rule);
static gint gnome_print_pdf_stroke (GnomePrintContext *ctx, const ArtBpath *bpath);
static gint gnome_print_pdf_image (GnomePrintContext *ctx, const gdouble *ctm, const gchar *px, gint w, gint h, gint rowstride, gint ch);
static gint gnome_print_pdf_glyphlist (GnomePrintContext *pc, const gdouble *a, GnomeGlyphList *gl);
static gint gnome_print_pdf_close (GnomePrintContext *pc);

static guint gnome_print_pdf_object_number (GnomePrintContext *pc);
static gint gnome_print_pdf_page_write_contents (GnomePrintContext *pc, GPPDFPage *page);
static gint gnome_print_pdf_fonts (GnomePrintContext *pc);
static gint gnome_print_pdf_error (gint fatal, const char *format, ...);
static gint gnome_print_pdf_images (GnomePrintContext *pc, GPPDFPage *page);
static gint gnome_print_pdf_page_write_resources (GnomePrintContext *pc, GPPDFPage *page);
static void gnome_print_pdf_graphic_state_free (GnomePrintPdfGraphicState *gs);
static GnomePrintPdfGraphicState *gnome_print_pdf_graphic_state_new (gint undefined);
static gint gnome_print_pdf_xref (GnomePrintContext *pc);
static gint gnome_print_pdf_trailer (GnomePrintContext *pc);
static gint gnome_print_pdf_pages (GnomePrintContext *pc);
static guint gnome_print_pdf_catalog (GnomePrintContext *pc);
static guint gnome_print_pdf_info (GnomePrintContext *pc);
static gint gnome_print_pdf_halftone_default (GnomePrintContext *pc);
static gint gnome_print_pdf_default_GS (GnomePrintContext *pc);
static gint gnome_print_pdf_write_content (GnomePrintPdf *pdf, const char *format, ...);
static GnomePrintPdfGraphicState *gnome_print_pdf_graphic_state_set (GnomePrintPdf *pdf);
static gint gnome_print_pdf_path_print (GnomePrintPdf *pdf, const ArtBpath *path);
static GnomePrintPdfGraphicState *gnome_print_pdf_graphic_state_duplicate (GnomePrintPdfGraphicState *gs_in);
static gint gnome_print_pdf_set_font_private (GnomePrintPdf *pdf, GnomeFont *font);
static gint gnome_print_pdf_image_compressed (GnomePrintContext *ctx, const gdouble *ctm,
					      const guchar *px, gint w, gint h, gint rowstride, gint ch);
static gint gnome_print_pdf_page_free (GPPDFPage *page);
static gboolean gnome_print_pdf_free_objects (GnomePrintPdf *pdf);
static gboolean gnome_print_pdf_free_fonts (GnomePrintPdf *pdf);
static gint gnome_print_pdf_set_color_private (GnomePrintPdf *pdf, gdouble r, gdouble g, gdouble b);


gint gnome_print_pdf_write (GnomePrintContext *pc, const char *format, ...);
static gint gnome_print_pdf_graphic_mode_set (GnomePrintPdf *pdf, gint mode);

static GnomePrintContextClass *parent_class = NULL;

GtkType
gnome_print_pdf_get_type (void)
{
	static GtkType pdf_type = 0;
	if (!pdf_type) {
		GtkTypeInfo pdf_info = {
			"GnomePrintPdf",
			sizeof (GnomePrintPdf),
			sizeof (GnomePrintPdfClass),
			(GtkClassInitFunc)  gnome_print_pdf_class_init,
			(GtkObjectInitFunc) gnome_print_pdf_init,
			NULL, NULL, NULL
		};
		pdf_type = gtk_type_unique (GNOME_TYPE_PRINT_CONTEXT, &pdf_info);
	}
	return pdf_type;
}

static void
gnome_print_pdf_init (GnomePrintPdf *pdf)
{
	GnomePrintContext *pc;
	gint n;
	
	pc = GNOME_PRINT_CONTEXT (pdf);

	pdf->pages = NULL;

	/* Object numbers */
	pdf->objects = NULL;
	pdf->object_number_last_used = 0;
	pdf->object_number_current = 0;
	pdf->object_number_gstate = 0;
	pdf->offset = 0;

	pdf->graphics_mode = PDF_GRAPHIC_MODE_GRAPHICS;

	/* Fonts */
	pdf->fonts_internal_number = sizeof(gnome_print_pdf_internal_fonts) /
		                           sizeof(ps_internal_font);
	pdf->fonts_internal = g_new (ps_internal_font, pdf->fonts_internal_number);
	for (n = 0; n < pdf->fonts_internal_number; n++) {
		pdf->fonts_internal[n].font_name = gnome_print_pdf_internal_fonts[n].font_name;
		pdf->fonts_internal[n].is_basic_14 = gnome_print_pdf_internal_fonts[n].is_basic_14;
	}
	
	pdf->fonts_max = GNOME_PRINT_NUMBER_OF_ELEMENTS;
	pdf->fonts_number = 0;
	pdf->fonts = g_new (GnomePrintPdfFont, pdf->fonts_max);

	/* Allocate and set defaults for the current graphic state */
	pdf->graphic_state     = gnome_print_pdf_graphic_state_new (FALSE);
	pdf->graphic_state_set = gnome_print_pdf_graphic_state_new (TRUE);

	/* gsave/grestore */
	pdf->gsave_level_max = GNOME_PRINT_NUMBER_OF_ELEMENTS;
	pdf->gsave_level_number = 0;
	pdf->gsave_stack = g_new (GnomePrintPdfGsave, pdf->gsave_level_max);
}

static void
gnome_print_pdf_class_init (GnomePrintPdfClass *class)
{
	GtkObjectClass *object_class;
	GnomePrintContextClass *pc_class;

	object_class = (GtkObjectClass *)class;
	pc_class = (GnomePrintContextClass *)class;

	parent_class = gtk_type_class (gnome_print_context_get_type ());
	
	object_class->destroy = gnome_print_pdf_destroy;

	pc_class->construct = gnome_print_pdf_construct;
	pc_class->beginpage = gnome_print_pdf_beginpage;
	pc_class->showpage = gnome_print_pdf_showpage;
	pc_class->gsave = gnome_print_pdf_gsave;
	pc_class->grestore = gnome_print_pdf_grestore;
	pc_class->fill = gnome_print_pdf_fill;
	pc_class->clip = gnome_print_pdf_clip;
	pc_class->stroke = gnome_print_pdf_stroke;
	pc_class->image = gnome_print_pdf_image;
	pc_class->glyphlist = gnome_print_pdf_glyphlist;
	pc_class->close = gnome_print_pdf_close;
}

static void
gnome_print_pdf_destroy (GtkObject *object)
{
	GnomePrintPdf *pdf;

	pdf = GNOME_PRINT_PDF (object);

	if (pdf->gsave_level_number != 0) {
		g_warning ("gsave unmatched. Should end with an empty stack");
	}

	gnome_print_pdf_graphic_state_free (pdf->graphic_state);
	gnome_print_pdf_graphic_state_free (pdf->graphic_state_set);
	gnome_print_pdf_free_objects (pdf);

	while (pdf->pages) {
		GPPDFPage *page;
		page = pdf->pages;
		pdf->pages = page->next;
		gnome_print_pdf_page_free (page);
	}

	g_list_free (pdf->objects);

	gnome_print_pdf_free_fonts (pdf);

	g_free (pdf->gsave_stack);
	g_free (pdf->fonts);
	g_free (pdf->fonts_internal);

	if (((GtkObjectClass *) parent_class)->destroy)
		(* ((GtkObjectClass *) parent_class)->destroy) (object);
}

static gint
gnome_print_pdf_construct (GnomePrintContext *ctx)
{
	GnomePrintPdf *pdf;
	gint ret;

	pdf = GNOME_PRINT_PDF (ctx);

	/* Create and open transport */
	ret = gnome_print_context_create_transport (ctx);
	g_return_val_if_fail (ret == GNOME_PRINT_OK, GNOME_PRINT_ERROR_UNKNOWN);
	ret = gnome_print_transport_open (ctx->transport);
	g_return_val_if_fail (ret == GNOME_PRINT_OK, GNOME_PRINT_ERROR_UNKNOWN);

	/* Read paper size */
	pdf->bbox.x0 = 0.0;
	pdf->bbox.y0 = 0.0;
	pdf->bbox.x1 = 21.0 * (72.0 / 2.54);
	pdf->bbox.y1 = 29.7 * (72.0 / 2.54);
	gpa_node_get_double_path_value (ctx->config, "Output.Media.PhysicalSize.Width", &pdf->bbox.x1);
	gpa_node_get_double_path_value (ctx->config, "Output.Media.PhysicalSize.Height", &pdf->bbox.y1);

	pdf->ascii_format = FALSE;
	gpa_node_get_bool_path_value (ctx->config, "Settings.Engine.Backend.AsciiFormat", &pdf->ascii_format);
	
	/* for now, we don't need to require 1.3. This could change ...*/
	ret = gnome_print_pdf_write (ctx, "%%PDF-1.2" EOL);
	g_return_val_if_fail (ret == GNOME_PRINT_OK, ret);

	/* binary file [1] Page 22-23 (Lets be nice - Lauris ;) */
	ret += gnome_print_pdf_write (ctx,"%%%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c" EOL,
				      0xce, 0x95, 0xce, 0xbb, 0xce, 0xbb, 0xce, 0xb7,
				      0xce, 0xbd, 0xce, 0xb9, 0xce, 0xba, 0xce, 0xac);

	return GNOME_PRINT_OK;
}

static gint
gnome_print_pdf_beginpage (GnomePrintContext *pc, const char *name)
{
	GnomePrintPdf *pdf;
	GPPDFPage *page;

	pdf = GNOME_PRINT_PDF (pc);
	
	page = g_new (GPPDFPage, 1);

	page->next = pdf->pages;
	pdf->pages = page;

	page->shown = FALSE;
	page->page_name = g_strdup (name);
	page->used_color_images = FALSE;
	page->used_grayscale_images = FALSE;
	page->page_number = (page->next) ? page->next->page_number + 1 : 1;
	page->stream_allocated = GNOME_PRINT_PDF_BUFFER_SIZE;
	page->stream = g_malloc (page->stream_allocated);
	page->stream[0] = 0;
	page->stream_used = 0;

	page->images = NULL;
	
	return GNOME_PRINT_OK;
}

static gint
gnome_print_pdf_showpage (GnomePrintContext *pc)
{
	GnomePrintPdf *pdf;
	GPPDFPage *page;
	gint ret;

	pdf = GNOME_PRINT_PDF (pc);
	page = pdf->pages;

	gnome_print_pdf_graphic_mode_set (pdf, PDF_GRAPHIC_MODE_GRAPHICS);

	/* Ask for object numbers for this page */
	page->object_number_page = gnome_print_pdf_object_number (pc);
	page->object_number_contents = gnome_print_pdf_object_number (pc);
	page->object_number_resources = gnome_print_pdf_object_number (pc);
	ret = gnome_print_pdf_images (pc, page);
	ret = gnome_print_pdf_page_write_contents  (pc, page);

	g_free (page->stream);
	page->stream = NULL;

	ret += gnome_print_pdf_page_write_resources (pc, page);

	if (page->page_number == 1) {
		pdf->object_number_pages = gnome_print_pdf_object_number (pc);
	}

	pdf->graphic_state->written = FALSE;

	/* We need to clean the Graphic State */
	gnome_print_pdf_graphic_state_free (pdf->graphic_state);
	gnome_print_pdf_graphic_state_free (pdf->graphic_state_set);
	pdf->graphic_state = gnome_print_pdf_graphic_state_new (FALSE);
	pdf->graphic_state_set = gnome_print_pdf_graphic_state_new (TRUE);

	page->shown = TRUE;
	pdf->active_color_flag = GP_GC_FLAG_UNSET;
		
	return GNOME_PRINT_OK;
}

static gint
gnome_print_pdf_gsave (GnomePrintContext *pc)
{
	GnomePrintPdf *pdf;
	GnomePrintPdfGraphicState *gs_push_me;
	GnomePrintPdfGraphicState *gs_push_me_set;
	gint ret = 0;

	pdf = GNOME_PRINT_PDF (pc);

	/* This is messy, but I don't want to play arround with it before
	   Gnome 1.4 is out. I don't think there is anything wrong. But since
	   we are setting the mode to graphics before gsave we are always going
	   to do gsaves and grestores in GRAPHICS mode, for that reason we don't
	   need to save the graphics_mode, we know it to be MODE_GRAPHICS. Just clean
	   it after 1.4 Chema. */

	gnome_print_pdf_graphic_mode_set (pdf, PDF_GRAPHIC_MODE_GRAPHICS);
	
	gs_push_me     = gnome_print_pdf_graphic_state_duplicate (pdf->graphic_state);
	gs_push_me_set = gnome_print_pdf_graphic_state_duplicate (pdf->graphic_state_set);

	pdf->gsave_stack [pdf->gsave_level_number].graphics_mode     = pdf->graphics_mode;
	pdf->gsave_stack [pdf->gsave_level_number].graphic_state     = gs_push_me;
	pdf->gsave_stack [pdf->gsave_level_number].graphic_state_set = gs_push_me_set;

	pdf->gsave_level_number++;

	if (pdf->gsave_level_number == pdf->gsave_level_max) {
		pdf->gsave_stack = g_realloc (pdf->gsave_stack, sizeof (GnomePrintPdfGsave) *
					      (pdf->gsave_level_max += GNOME_PRINT_NUMBER_OF_ELEMENTS_GROW));
	}
	
	ret += gnome_print_pdf_write_content (pdf, "q" EOL);

	return ret;
}

static gint
gnome_print_pdf_grestore (GnomePrintContext *pc)
{
	GnomePrintPdf *pdf;
	gint ret = 0;
	gint mode;

	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT(pc), -1);
	pdf = GNOME_PRINT_PDF (pc);
	g_return_val_if_fail (pdf != NULL, -1);

	/* * * * * * * * * * */
#if 0	
	mode = pdf->gsave_stack [pdf->gsave_level_number].graphics_mode;
	gnome_print_pdf_graphic_mode_set (pdf, PDF_GRAPHIC_MODE_GRAPHICS);
#endif	
	/* * * * * * * * * * */
	
	pdf->gsave_level_number--;

	if (pdf->gsave_level_number < 0) {
		gnome_print_pdf_error (TRUE, "grestore, graphic state stack empty");
		return 0;
	}

	gnome_print_pdf_graphic_state_free (pdf->graphic_state_set);
	gnome_print_pdf_graphic_state_free (pdf->graphic_state);
	pdf->graphic_state_set = pdf->gsave_stack [pdf->gsave_level_number].graphic_state_set;
	pdf->graphic_state     = pdf->gsave_stack [pdf->gsave_level_number].graphic_state;

	/* * * * * * * * * * */
	mode = pdf->gsave_stack [pdf->gsave_level_number].graphics_mode;
	gnome_print_pdf_graphic_mode_set (pdf, mode);
	/* * * * * * * * * * */

	ret += gnome_print_pdf_write_content (pdf, "Q" EOL);

	pdf->active_color_flag = GP_GC_FLAG_UNSET;

	return ret;
}

static gint
gnome_print_pdf_clip (GnomePrintContext *ctx, const ArtBpath *bpath, ArtWindRule rule)
{
	GnomePrintPdf *pdf;
	gint ret;

	pdf = GNOME_PRINT_PDF (ctx);

	/* fixme: Think (Lauris) */
	gnome_print_pdf_graphic_mode_set (pdf, PDF_GRAPHIC_MODE_GRAPHICS);
	gnome_print_pdf_graphic_state_set (pdf);

	ret = gnome_print_pdf_path_print (pdf, bpath);
	g_return_val_if_fail (ret == GNOME_PRINT_OK, ret);

	if (rule == ART_WIND_RULE_NONZERO) {
		ret = gnome_print_pdf_write_content (pdf, "W n" EOL);
	} else {
		ret = gnome_print_pdf_write_content (pdf, "W* n" EOL);
	}

	return ret;
}

static gint
gnome_print_pdf_fill (GnomePrintContext *ctx, const ArtBpath *bpath, ArtWindRule rule)
{
	GnomePrintPdf *pdf;
	gint ret = 0;

	pdf = GNOME_PRINT_PDF (ctx);

	/* fixme: Think (Lauris) */
	gnome_print_pdf_graphic_mode_set (pdf, PDF_GRAPHIC_MODE_GRAPHICS);
	gnome_print_pdf_graphic_state_set (pdf);

	ret = gnome_print_pdf_path_print (pdf, bpath);
	g_return_val_if_fail (ret == GNOME_PRINT_OK, ret);

	if (rule == ART_WIND_RULE_NONZERO)
		ret = gnome_print_pdf_write_content (pdf, "f" EOL);
	else
		ret = gnome_print_pdf_write_content (pdf, "f*" EOL);

	return ret;
}

static gint
gnome_print_pdf_stroke (GnomePrintContext *ctx, const ArtBpath *bpath)
{
	GnomePrintPdf *pdf;
	gint ret;
	
	pdf = GNOME_PRINT_PDF (ctx);

	/* fixme: Think (Lauris) */
	gnome_print_pdf_graphic_mode_set (pdf, PDF_GRAPHIC_MODE_GRAPHICS);
	gnome_print_pdf_graphic_state_set (pdf);
	
	ret = gnome_print_pdf_path_print (pdf, bpath);
	g_return_val_if_fail (ret == GNOME_PRINT_OK, ret);

	ret = gnome_print_pdf_write_content (pdf, "S" EOL);

	return ret;
}

static gint
gnome_print_pdf_glyphlist (GnomePrintContext *pc, const gdouble *a, GnomeGlyphList *gl)
{
	GnomePrintPdf *pdf;
	GnomePosGlyphList *pgl;
	gdouble f[6], t[6];
	gint ret, s;

	pdf = GNOME_PRINT_PDF (pc);

	/* Set text mode */
	gnome_print_pdf_graphic_mode_set (pdf, PDF_GRAPHIC_MODE_TEXT);

	pgl = gnome_pgl_from_gl (gl, (gdouble *) a, GNOME_PGL_RENDER_DEFAULT);

	for (s = 0; s < pgl->num_strings; s++) {
		GnomePosString * ps;
		GnomeFont *font;
		gint i;

		ps = pgl->strings + s;

		/* Set font */
		font = (GnomeFont *) gnome_rfont_get_font (ps->rfont);
		ret = gnome_print_pdf_set_font_private (pdf, font);
		g_return_val_if_fail (ret == GNOME_PRINT_OK, ret);
		/* Set matrix */
		art_affine_scale (f, gnome_font_get_size (font), gnome_font_get_size (font));
		art_affine_multiply (t, f, a);
		gnome_print_pdf_write_content (pdf, "%g %g %g %g %g %g Tm" EOL, t[0], t[1], t[2], t[3], t[4], t[5]);
		ret = gnome_print_pdf_set_color_private (pdf,
							 ((ps->color >> 24) & 0xff) / 255.0,
							 ((ps->color >> 16) & 0xff) / 255.0,
							 ((ps->color >>  8) & 0xff) / 255.0);
		g_return_val_if_fail (ret == GNOME_PRINT_OK, ret);
		/* Build string */
		/* fixme: This is eeeevvvviiiiilllll - but is there xyshow in pdf? - otherwise we need lot of intelligence */
		/* fixme: 16 bit fonts will be fucked up - yooooo */
		for (i = ps->start; i < ps->start + ps->length; i++) {
			gint glyph;
			glyph = pgl->glyphs[i].glyph & 0xff;
			ret = gnome_print_pdf_write_content (pdf, "%g %g m (\\%03o)Tj" EOL, pgl->glyphs[i].x, pgl->glyphs[i].y, glyph);
			g_return_val_if_fail (ret == GNOME_PRINT_OK, ret);
		}
	}

	gnome_pgl_destroy (pgl);

	pdf->active_color_flag = GP_GC_FLAG_UNSET;

	return GNOME_PRINT_OK;
}

static gint
gnome_print_pdf_image (GnomePrintContext *ctx, const gdouble *ctm, const gchar *px, gint w, gint h, gint rowstride, gint ch)
{
	GnomePrintPdf *pdf;
	gint ret;

	pdf = GNOME_PRINT_PDF (ctx);

	if (ch == 4) {
		/* RGBA image - convert to RGB */
		gint x, y;
		gchar *b, *d;
		const gchar *s;
		b = g_new (guchar, w * h * 3);
		for (y = 0; y < h; y++) {
			d = b + 3 * w * y;
			s = px + rowstride * y;
			for (x = 0; x < w; x++) {
				/* fixme: Think about signedness (Lauris) */
				*d++ = 0xff + ((((s[0] - 0xff) * s[3]) + 0x80) >> 8);
				*d++ = 0xff + ((((s[1] - 0xff) * s[3]) + 0x80) >> 8);
				*d++ = 0xff + ((((s[2] - 0xff) * s[3]) + 0x80) >> 8);
				s += 4;
			}
		}
		px = b;
		rowstride = 3 * w;
	}

	if (ch == 1) {
		pdf->pages->used_grayscale_images = TRUE;
	} else {
		pdf->pages->used_color_images = TRUE;
	}

	ret = gnome_print_pdf_image_compressed (ctx, ctm, px, w, h, rowstride, ch);

	if (ch == 4) g_free ((gpointer) px);

	return ret;
}

static gint
gnome_print_pdf_close (GnomePrintContext *pc)
{
	GnomePrintPdf *pdf;
	gint ret = 0;

	pdf = GNOME_PRINT_PDF (pc);

	ret += gnome_print_pdf_halftone_default (pc);
	ret += gnome_print_pdf_default_GS       (pc);
	ret += gnome_print_pdf_fonts            (pc);
	ret += gnome_print_pdf_pages            (pc);
	ret += gnome_print_pdf_catalog          (pc);
	ret += gnome_print_pdf_info             (pc);
	ret += gnome_print_pdf_xref             (pc);
	ret += gnome_print_pdf_trailer          (pc);
	
	gnome_print_pdf_write (pc,
			       "startxref" EOL
			       "%i" EOL
			       "%c%cEOF" EOL,
			       pdf->xref_location,
			       '%',
			       '%');

 	gnome_print_transport_close (pc->transport);
	pc->transport = NULL;

	return GNOME_PRINT_OK;
}

GnomePrintContext *
gnome_print_pdf_new (GPANode *config)
{
	GnomePrintContext *ctx;
	gint ret;

	g_return_val_if_fail (config != NULL, NULL);
	g_return_val_if_fail (GPA_IS_NODE (config), NULL);

	ctx = gtk_type_new (GNOME_TYPE_PRINT_PDF);

	ret = gnome_print_context_construct (ctx, config);

	if (ret != GNOME_PRINT_OK) {
		gtk_object_unref (GTK_OBJECT (ctx));
		ctx = NULL;
	}

	return ctx;
}

gint
gnome_print_pdf_write (GnomePrintContext *pc, const char *format, ...)
{
	GnomePrintPdf *pdf;
	va_list arguments;
	gchar *text;
	gchar *oldlocale;
	gint len, ret;
	
	pdf = GNOME_PRINT_PDF (pc);

	oldlocale = setlocale (LC_NUMERIC, NULL);
	setlocale (LC_NUMERIC, "C");
		
	va_start (arguments, format);
	text = g_strdup_vprintf (format, arguments);
	va_end (arguments);

	len = strlen (text);
	ret = gnome_print_transport_write (pc->transport, text, len);
	g_free (text);
	setlocale (LC_NUMERIC, oldlocale);

	if (ret > 0) {
		pdf->offset += ret;
		ret = GNOME_PRINT_OK;
	}
	
	return ret;
}

gint
gnome_print_pdf_object_start (GnomePrintContext *pc, guint object_number)
{
	GnomePrintPdf *pdf;
	GnomePrintPdfObject *object;
	gint ret;
	gint temp_num;

	pdf = GNOME_PRINT_PDF (pc);

	temp_num = pdf->object_number_last_used - object_number;
	object = g_list_nth_data (pdf->objects, temp_num);
	
	g_return_val_if_fail (object != NULL, -1);

	pdf->object_number_current = object_number;

	object->number = object_number;
	object->offset = pdf->offset;

	ret = gnome_print_pdf_write   (pc, "%d 0 obj" EOL "<<" EOL, object_number);

	return ret;
}

gint
gnome_print_pdf_object_end (GnomePrintContext *pc, guint object_number, guint dont_print)
{
	gint ret = 0;
	GnomePrintPdf *pdf;
	
	pdf = GNOME_PRINT_PDF (pc);

	pdf->object_number_current = 0;
	
	if (!dont_print) {
		ret += gnome_print_pdf_write (pc, ">>" EOL "endobj" EOL, object_number);
	}

	return ret;
}

/* -------------------------- END: PUBLIC FUNCTIONS ------------------------ */
static guint
gnome_print_pdf_object_number (GnomePrintContext *pc)
{
	GnomePrintPdf *pdf;
	GnomePrintPdfObject *object;

	pdf = GNOME_PRINT_PDF (pc);
	
	object = g_new (GnomePrintPdfObject, 1);

	pdf->objects = g_list_prepend (pdf->objects, object);

	return ++pdf->object_number_last_used;
}

#if 0
static GnomePrintPdfGraphicState *
gnome_print_pdf_graphic_state_current (GnomePrintPdf *pdf, gint dirtyfy)
{
	GnomePrintPdfGraphicState *gs;

	gs = pdf->graphic_state;

	if (dirtyfy) gs->dirty = TRUE;
	
	return gs;
}
#endif
	
/**
 * gnome_print_pdf_compr_from_string:
 * @str: 
 * 
 * Given the string of a compression method, it returns it's
 * enum value.
 * 
 * Return Value: Enum value of the compression. PDF_COMPRESSION_NONE on error;
 *
 **/

static PdfCompressionType
gnome_print_pdf_compr_from_string (const gchar *str)
{
	g_return_val_if_fail (str != NULL, PDF_COMPRESSION_NONE);

	if (strcmp ("NoCompression", str) == 0)
		return PDF_COMPRESSION_NONE;
	else if (strcmp ("FlateCompression", str) == 0)
		return PDF_COMPRESSION_FLATE;
	else if (strcmp ("HexEncoded", str) == 0)
		return PDF_COMPRESSION_HEX;
	else
		g_warning ("Could not determine compression from %s\n", str);

	return 	PDF_COMPRESSION_NONE;
}

static gint
gnome_print_pdf_write_compression_filters (GnomePrintContext *pc, PdfCompressionType compr_type)
{
	GnomePrintPdf *pdf;
	gint ret = 0;

	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT(pc), -1);
	pdf = GNOME_PRINT_PDF (pc);
	g_return_val_if_fail (GNOME_IS_PRINT_PDF (pdf), -1);
	
	if (compr_type == PDF_COMPRESSION_NONE &&	!pdf->ascii_format)
		return ret;
	
	ret += gnome_print_pdf_write  (pc, "/Filter [");

	if (pdf->ascii_format && compr_type != PDF_COMPRESSION_HEX)
		ret += gnome_print_pdf_write  (pc,"/ASCII85Decode ");
	
	if (compr_type == PDF_COMPRESSION_FLATE) 
		ret += gnome_print_pdf_write  (pc, "/FlateDecode ");

	if (compr_type == PDF_COMPRESSION_HEX)
		ret += gnome_print_pdf_write  (pc,"/ASCIIHexDecode ");

	ret += gnome_print_pdf_write  (pc,"]" EOL);

	return ret;
}

static gint
gnome_print_pdf_write_stream (GnomePrintContext *pc,
			      gchar *stream,
			      gint length,
			      PdfCompressionType compr_type)
{
	gint ret = 0;

	/* fixme: Compression goes here (Lauris) */

	ret += gnome_print_pdf_write  (pc, "/Length %i" EOL, length);
	ret += gnome_print_pdf_write_compression_filters (pc, compr_type);
	ret += gnome_print_pdf_write  (pc, ">>" EOL);
	ret += gnome_print_pdf_write  (pc, "stream" EOL);

	ret += gnome_print_transport_write (pc->transport, stream, length);
	
	return ret;
}


static gint
gnome_print_pdf_page_write_contents (GnomePrintContext *pc, GPPDFPage *page)
{
	GnomePrintPdf *pdf;
	PdfCompressionType compr_type;
#ifdef ENABLE_LIBGPA
	const gchar *compr_string;
#endif
	gchar *compressed_stream = NULL;
	gint ret = 0;
	gint compressed_stream_length;
	gint stream_length;
	gint real_length = 0;
	
	pdf = GNOME_PRINT_PDF (pc);

#ifdef ENABLE_LIBGPA
	compr_string = gpa_settings_query_options (pdf->gpa_settings, "TextCompression");
	compr_type = gnome_print_pdf_compr_from_string (compr_string);
#else
	compr_type = PDF_COMPRESSION_NONE;
#endif

	switch (compr_type) {
	case PDF_COMPRESSION_FLATE:
		stream_length =  page->stream_used;
		compressed_stream_length = gnome_print_encode_deflate_wcs (stream_length);
		compressed_stream = g_malloc (compressed_stream_length);
		real_length = gnome_print_encode_deflate (page->stream, compressed_stream,
							  stream_length, compressed_stream_length);
		break;
	case PDF_COMPRESSION_NONE:
	default:
		real_length = page->stream_used; 
	}
	

	ret += gnome_print_pdf_object_start (pc, page->object_number_contents);

	if (compr_type != PDF_COMPRESSION_NONE) {
		pdf->offset += gnome_print_pdf_write_stream (pc,
							     compressed_stream,
							     real_length,
							     compr_type);
#if 0
		ret += gnome_print_pdf_write  (pc, EOL);
#endif
	} else {
#if 0
		ret += gnome_print_pdf_write  (pc, "%s", page->stream);
#else
		pdf->offset += gnome_print_pdf_write_stream (pc,
							     page->stream,
							     real_length,
							     compr_type);
#if 0
		ret += gnome_print_pdf_write  (pc, EOL);
#endif
#endif
	}
	
	ret += gnome_print_pdf_write  (pc, "endstream" EOL);
	ret += gnome_print_pdf_write  (pc, "endobj" EOL);
	ret += gnome_print_pdf_object_end (pc, page->object_number_contents, TRUE);

	if (compr_type != PDF_COMPRESSION_NONE)
		g_free (compressed_stream);

	return ret;
}

static gint
gnome_print_pdf_encoding (GnomePrintContext *pc, GnomePrintPdfFont *font_in)
{
	GnomePrintPdf *pdf;
	const GnomeFontFace *face;
	GnomeFont *font;
	gint nglyphs, nfonts;
	gint i, j;
	gint ret = 0;

	pdf = GNOME_PRINT_PDF (pc);

	font = font_in->gnome_font;
	face = (GnomeFontFace *) gnome_font_get_face (font);

	gnome_print_pdf_object_start (pc, font_in->object_number_encoding);
	ret += gnome_print_pdf_write (pc,"/Type /Encoding" EOL);
	ret += gnome_print_pdf_write (pc,"/Differences [0" EOL);
	
	nglyphs = gnome_font_face_get_num_glyphs (face);
	nfonts = (nglyphs + 255) >> 8;

	for (i = 0; i < nfonts; i++) {
		gint col = 0;
		for (j = 0; j < 256; j++) {
			gint glyph;
			const gchar *name;

			glyph = 256 * i + j;
			if (glyph >= nglyphs)
				glyph = 0;

			name = gnome_font_face_get_glyph_ps_name (face, glyph);
			gnome_print_pdf_write (pc, "/%s", name);
			col += strlen (name) + 1;
			if (col > 70) {
				gnome_print_pdf_write (pc, EOL);
				col = 0;
			}
		}

	}
	
	ret += gnome_print_pdf_write (pc,"]" EOL);

	gnome_print_pdf_object_end   (pc, font_in->object_number_encoding, FALSE);

	return ret;
}


/* FIXME : fix the '-' character issue. Chema */
static gint
gnome_print_pdf_font_print_metrics (GnomePrintContext *pc, GnomePrintPdfFont *font_in)
{
	GnomePrintPdf *pdf;
	const GnomeFontFace *face;
	const GnomeFont *font;
	gint nglyphs, nfonts;
	gint i, j;
	gint ret = 0;

	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (pc), -1);
	pdf = GNOME_PRINT_PDF (pc);
	g_return_val_if_fail (GNOME_IS_PRINT_PDF (pdf), -1);
	g_return_val_if_fail (font_in != NULL, -1);
	font = font_in->gnome_font;
	g_return_val_if_fail (GNOME_IS_FONT (font), -1);
	face = (GnomeFontFace *) gnome_font_get_face (font);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), -1);

	nglyphs = gnome_font_face_get_num_glyphs (face);
	nfonts = (nglyphs + 255) >> 8;
	if (nfonts > 1) {
		static gboolean warned = FALSE;
		if (!warned || TRUE)
			g_warning ("\nCan't encode fonts with more than 1 page of glyphs for now.\n"
				   "Encoding only the first page of glyphs (256). The font in \n"
				   "question is :%s, which has %i glyphs. \n",
				   gnome_font_face_get_name (face),
				   gnome_font_face_get_num_glyphs (face));
		warned = TRUE;
		nfonts = 1;
	}

	if (nglyphs > 256)
		nglyphs = 256;
	
	ret += gnome_print_pdf_write  (pc,
				       "/FirstChar %i" EOL
				       "/LastChar %i" EOL
				       "/Widths [",
				       1,
				       nglyphs - 1);

	
	for (i = 0; i < nfonts; i++) 
	{
		gint col = 0;
		for (j = 1; j < nglyphs; j++) {
			gint glyph;
			ArtPoint point;

			glyph = 256 * i + j;
			if (glyph >= nglyphs)
				glyph = 0;

			gnome_font_face_get_glyph_stdadvance (face, glyph, &point);

			ret += gnome_print_pdf_write  (pc, "%g ",point.x);
			
			col += 1;
			if (col > 15) {
				gnome_print_pdf_write (pc, EOL);
				col = 0;
			}
		}
	}
	
	ret += gnome_print_pdf_write  (pc, "]" EOL);

	return ret;
}


static gint32
gnome_font_face_get_pdf_flags (const GnomeFontFace *face)
{
	/* See the PDF 1.3 standard, section 7.11.2, page 225 */
	gboolean fixed_width;
	gboolean serif;
	gboolean symbolic;
	gboolean script;
	gboolean italic;
	gboolean all_cap;
	gboolean small_cap;
	gboolean force_bold;
	gboolean adobe_roman_set;
	gint32 flags;

	fixed_width = gnome_font_face_is_fixed_width (face);
	serif = TRUE;
	symbolic = FALSE; /* FIXME */
	script = FALSE;  /* FIXME */
	italic = gnome_font_face_is_italic (face);
	all_cap = FALSE;
	small_cap = FALSE;
	force_bold = FALSE;
	adobe_roman_set = TRUE;

	flags = 0;
	flags =
		fixed_width     |
		(serif      << 1) |
		(symbolic   << 2) |
		(script     << 3) |
		/* 5th bit is reserved */
		(adobe_roman_set << 5) |
		(italic     << 6) |
		/* 8th - 16 are reserved */
		(all_cap    << 16) |
		(small_cap  << 17) |
		(force_bold << 18);
	/* 20-32 reserved */

	return flags;
}

static gint
gnome_font_face_get_stemv (const GnomeFontFace *face)
{
	gint stemv;
	gint stemh;

	gnome_print_pdf_type1_get_stems (face, &stemv, &stemh);

	return stemv;
}


static gint
gnome_print_pdf_font_print_descriptor (GnomePrintContext *pc,
				       GnomePrintPdfFont *font)
{
	const GnomeFontFace *face;
	guint object_number;
	guint pfb_object_number;
	gint ret = 0;
	gint ascent;
	gint descent;
	gint flags;
	gint stemv;
	gint italic_angle;
	gint capheight;
	gint xheight;
	const ArtDRect *rect;
	ArtDRect sillybox = {0.0, -100.0, 600.0, 900.0};

	g_return_val_if_fail (font != NULL, -1);
	g_return_val_if_fail (font->gnome_font != NULL, -1);
	g_return_val_if_fail (GNOME_IS_FONT (font->gnome_font), -1);
	face = gnome_font_get_face (font->gnome_font);

	object_number     = gnome_print_pdf_object_number (pc);
	pfb_object_number = gnome_print_pdf_object_number (pc);
	font->object_number_descriptor = object_number;
	font->object_number_pfb = pfb_object_number;
	
	ret += gnome_print_pdf_object_start (pc, object_number);

	ascent = (gint) gnome_font_face_get_ascender (face);
	descent = (gint) gnome_font_face_get_descender (face);
	flags = gnome_font_face_get_pdf_flags (face);
	stemv = gnome_font_face_get_stemv (face);

	/* fixme: */
#if 0
	/* GnomeFontFace arguments */
	gtk_object_get (GTK_OBJECT (face), "ItalicAngle", &val, NULL);
	italic_angle = (gint) val;
	gtk_object_get (GTK_OBJECT (face), "CapHeight", &val, NULL);
	capheight = (gint) val;
	gtk_object_get (GTK_OBJECT (face), "XHeight", &val, NULL);
	xheight = (gint) val;
	gtk_object_get (GTK_OBJECT (face), "FontBBox", &rect, NULL);
#else
	italic_angle = 0;
	capheight = 900;
	xheight = 500;
	rect = &sillybox;
#endif

	/* fixme: Why is descent negative here? */
	ret += gnome_print_pdf_write  (pc,
				       "/Type /FontDescriptor" EOL
				       "/Ascent %i" EOL
				       "/CapHeight %i" EOL
				       "/Descent %i" EOL
				       "/Flags %i" EOL
				       "/FontBBox [%g %g %g %g]" EOL
				       "/FontName /%s" EOL
				       "/ItalicAngle %i" EOL
				       "/StemV %i" EOL
				       "/XHeight %i" EOL
				       "/FontFile %i 0 R" EOL,
				       ascent,
				       capheight,
				       -descent,
				       flags,
				       rect->x0, rect->y0, rect->x1, rect->y1,
				       font->font_name,
				       italic_angle,
				       stemv,
				       xheight,
				       pfb_object_number);

	ret += gnome_print_pdf_object_end   (pc, object_number, FALSE);

	return ret;
}

	
static gint
gnome_print_pdf_fonts (GnomePrintContext *pc)
{
	GnomePrintPdf *pdf;
	GnomePrintPdfFont *font;
	gint ret = 0;
	gint n;

	pdf = GNOME_PRINT_PDF (pc);

	if (pdf->fonts_number < 1)
		return 0;

	for (n=0; n < pdf->fonts_number; n++)	{
		font = &pdf->fonts [n];

		if (!font->is_basic_14) {
			ret += gnome_print_pdf_font_print_descriptor (pc, font);
			/* We are initialy implementing only type1 embeding */
			ret += gnome_print_pdf_font_type1_embed (pc, font);
		}

		ret += gnome_print_pdf_encoding (pc, font);
		
		ret += gnome_print_pdf_object_start (pc, font->object_number);
		ret += gnome_print_pdf_write  (pc,
					       "/Type /Font" EOL
					       "/Subtype /Type1" EOL /* Hardcode tyep1 for now*/
					       "/Name /F%i" EOL,
					       font->font_number);
		if (!font->is_basic_14)
			ret += gnome_print_pdf_font_print_metrics (pc, font);

		ret += gnome_print_pdf_write  (pc,
					       "/Encoding %i 0 R" EOL
					       "/BaseFont /%s" EOL,
					       font->object_number_encoding,
					       font->font_name);
		if (!font->is_basic_14)
			ret += gnome_print_pdf_write  (pc,
						       "/FontDescriptor %i 0 R" EOL,
						       font->object_number_descriptor);
		
		ret += gnome_print_pdf_object_end   (pc, font->object_number, FALSE);
	}
	
	return ret;
}

static gint
gnome_print_pdf_error (gint fatal, const char *format, ...)
{
	va_list arguments;
	gchar *text;

	debug (FALSE, "");

	va_start (arguments, format);
	text = g_strdup_vprintf (format, arguments);
	va_end (arguments);
	
	g_warning ("Offending command [[%s]]", text);

	g_free (text);
	
	return -1;
}


static gint
gnome_print_pdf_images (GnomePrintContext *pc, GPPDFPage *page)
{
	GPPDFImage *image;
	GnomePrintPdf *pdf;
	const gchar *image_type_text;
	gint ret = 0;
	GSList *l;
	
	pdf = GNOME_PRINT_PDF (pc);

	if (!page->images) return GNOME_PRINT_OK;

	/* We reverse images list to be really nice (Lauris) */
	l = NULL;
	for (image = page->images; image != NULL; image = image->next) {
		l = g_slist_prepend (l, image);
	}

	image_type_text = NULL;
	/* Now the code itself */
	while (l) {
		image = (GPPDFImage *) l->data;
		l = g_slist_remove (l, image);

		switch (image->image_type){
		case PDF_IMAGE_RGB:
			image_type_text = "DeviceRGB";
			break;
		case PDF_IMAGE_GRAYSCALE:
			image_type_text = "DeviceGray";
			break;
		default:
			g_assert_not_reached ();
			break;
		}

		ret += gnome_print_pdf_object_start (pc, image->object_number);
		ret += gnome_print_pdf_write  (pc,
					       "/Type /XObject" EOL
					       "/Subtype /Image" EOL
					       "/Name /Im%d" EOL
					       "/Width %d" EOL
					       "/Height %d" EOL
					       "/BitsPerComponent 8" EOL
					       "/ColorSpace /%s" EOL,
					       image->image_number,
					       image->width,
					       image->height,
					       image_type_text);

		pdf->offset += gnome_print_pdf_write_stream (pc,
							     image->data,
							     image->data_length,
							     image->compr_type);

		ret += gnome_print_pdf_write  (pc, EOL);
		
		ret += gnome_print_pdf_write  (pc,
					       "endstream" EOL
					       "endobj" EOL);

		ret += gnome_print_pdf_object_end (pc, image->object_number, TRUE);
	}

	return ret;
}

static gint
gnome_print_pdf_get_fonts_object_numbers (GnomePrintContext *pc)
{
	GnomePrintPdf *pdf;
	GnomePrintPdfFont *font;
	gint n;
	gint ret = 0;

	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT(pc), -1);
	pdf = GNOME_PRINT_PDF (pc);
	g_return_val_if_fail (pdf != NULL, -1);

	if (pdf->fonts_number < 1)
		return 0;

	ret += gnome_print_pdf_write  (pc, "/Font <<" EOL);
	
	for (n=0; n < pdf->fonts_number; n++)	{
		font = &pdf->fonts [n];
		if (font->object_number == 0) {
			font->object_number = gnome_print_pdf_object_number (pc);
			font->object_number_encoding = gnome_print_pdf_object_number (pc);
		}
		
		ret += gnome_print_pdf_write  (pc, "/F%i %i 0 R" EOL,
					       font->font_number,
					       font->object_number);
	}

	ret += gnome_print_pdf_write  (pc, ">>" EOL);

	return ret;
}

static gint
gnome_print_pdf_get_images_object_numbers (GnomePrintContext *pc, GPPDFPage *page)
{
	GnomePrintPdf *pdf;
	GPPDFImage *image;
	gint ret = 0;
	GSList *l;

	pdf = GNOME_PRINT_PDF (pc);

	if (!page->images) return GNOME_PRINT_OK;

	/* We reverse images list to be really nice (Lauris) */
	l = NULL;
	for (image = page->images; image != NULL; image = image->next) {
		l = g_slist_prepend (l, image);
	}

	ret += gnome_print_pdf_write  (pc, "/XObject <<" EOL);
	
	while (l) {
		image = (GPPDFImage *) l->data;
		l = g_slist_remove (l, image);
		ret += gnome_print_pdf_write  (pc, "/Im%i %i 0 R" EOL,
					       image->image_number,
					       image->object_number);
	}

	ret += gnome_print_pdf_write  (pc, ">>" EOL);

	return ret;
}

static gint
gnome_print_pdf_page_write_resources (GnomePrintContext *pc, GPPDFPage *page)
{
	GnomePrintPdf *pdf;
	gint ret = 0;

	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (pc), -1);
	g_return_val_if_fail (page != NULL, -1);
	pdf = GNOME_PRINT_PDF (pc);
	g_return_val_if_fail (pdf  != NULL, -1);


	ret += gnome_print_pdf_object_start (pc, page->object_number_resources);
	ret += gnome_print_pdf_write  (pc,"/ProcSet [/PDF ");
	if (pdf->fonts_number > 0)
		ret += gnome_print_pdf_write  (pc,"/Text ");
	if (page->used_grayscale_images)
		ret += gnome_print_pdf_write  (pc,"/ImageB ");
	if (page->used_color_images)
		ret += gnome_print_pdf_write  (pc,"/ImageC ");
	ret += gnome_print_pdf_write  (pc,"]" EOL);

	ret += gnome_print_pdf_get_fonts_object_numbers  (pc);
	ret += gnome_print_pdf_get_images_object_numbers (pc, page);
	
	ret += gnome_print_pdf_write  (pc,"/ExtGState <<" EOL);
	if (pdf->object_number_gstate == 0)
		pdf->object_number_gstate = gnome_print_pdf_object_number (pc);
	ret += gnome_print_pdf_write  (pc,"/GS1 %i 0 R" EOL, pdf->object_number_gstate);
	ret += gnome_print_pdf_write  (pc,">>" EOL);
	ret += gnome_print_pdf_object_end   (pc, page->object_number_resources, FALSE);
	
	return ret;
}

static void
gnome_print_pdf_graphic_state_free (GnomePrintPdfGraphicState *gs)
{
	debug (FALSE, "");

#if 0
	gp_path_unref (gs->current_path);
#endif
	g_free (gs);
}
	
static GnomePrintPdfGraphicState *
gnome_print_pdf_graphic_state_new (gint undefined)
{
	GnomePrintPdfGraphicState * state;
	gint def;

	debug (FALSE, "");

	if (undefined)
		def = 1;
	else
		def = 0;
	
	state = g_new (GnomePrintPdfGraphicState, 1);

	state->dirty   = TRUE;
	state->written = FALSE;

	/* Font stuff */
	state->font_size = def;
	state->font_character_spacing = 0;
	state->font_word_spacing = 0;

	state->pdf_font_number = GNOME_PRINT_PDF_FONT_UNDEFINED;
	state->text_flag = FALSE;

	return state;
}

static gint
gnome_print_pdf_xref (GnomePrintContext *pc)
{
	GnomePrintPdf *pdf;
	GnomePrintPdfObject *nth_object;
	GList *list;
	gint   ret = 0;

	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (pc), -1);
	pdf = GNOME_PRINT_PDF (pc);
	g_return_val_if_fail (pdf != NULL, -1);
	g_return_val_if_fail (pdf->object_number_current == 0, -1);

	pdf->xref_location = pdf->offset;
	
	ret += gnome_print_pdf_write (pc,
				      "xref" EOL
				      "0 %i" EOL
				      "%010i %05i f" EOL,
				      pdf->object_number_last_used+1,
				      0,
				      65535);
	
	pdf->objects = g_list_reverse (pdf->objects);
	list = pdf->objects;

	for (; list; list = list->next)	{
		nth_object = (GnomePrintPdfObject *) list->data;
		ret +=   gnome_print_pdf_write (pc,
						"%010i %05i n" EOL,
						nth_object->offset,
						0);
	}
	
	return ret;
}

static gchar*
gnome_print_pdf_id_new (GnomePrintPdf *pdf)
{
	gchar *a;
	
	debug (FALSE, "");

	/* FIXME: Improve ID generation */
	a = g_strdup_printf ("%.12d%.2d%.12d%.6ld",
			     (gint) time(NULL),
			     95,
			     pdf->offset,
			     (long) getpid());

	if (strlen (a) != 32) {
		g_warning ("Error while creating pdf_id. [%s]\n", a);
		if (a != NULL)
			g_free (a);
		a = g_strdup ("00ff00ff00ff00ff00ff00ff00ff00ff");
	}

	return a;
}

static gint
gnome_print_pdf_trailer (GnomePrintContext *pc)
{
	GnomePrintPdf *pdf;
	gint ret = 0;
	gchar *id;

	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT(pc), -1);
	pdf = GNOME_PRINT_PDF (pc);
	g_return_val_if_fail (pdf != NULL, -1);

	id = gnome_print_pdf_id_new (pdf);

	g_return_val_if_fail (id != NULL, -1);
	
	ret += gnome_print_pdf_write (pc,
				      "trailer\r\n"
				      "<<\r\n"
				      "/Size %i\r\n"
				      "/Root %i 0 R\r\n"
				      "/Info %i 0 R\r\n"
				      "/ID [<%s><%s>]\r\n"
				      ">>\r\n",
				      pdf->object_number_last_used+1,
				      pdf->object_number_catalog,
				      pdf->object_number_info,
				      id,
				      id);

	g_free (id);
	
	return ret;
}


static gint
gnome_print_pdf_page (GnomePrintContext *pc, GPPDFPage *page)
{
	GnomePrintPdf *pdf;
	gint ret = 0;
	
	debug (FALSE, "");

	g_return_val_if_fail (page != NULL, -1);
	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT(pc), -1);
	pdf = GNOME_PRINT_PDF (pc);
	g_return_val_if_fail (pdf  != NULL, -1);

	ret += gnome_print_pdf_object_start (pc, page->object_number_page);
	ret += gnome_print_pdf_write (pc,
				      "/Type /Page" EOL
				      "/Parent %i 0 R" EOL
				      "/Resources %i 0 R" EOL
				      "/Contents %i 0 R" EOL,
				      pdf->object_number_pages,
				      page->object_number_resources,
				      page->object_number_contents);
	ret += gnome_print_pdf_object_end   (pc, page->object_number_page, FALSE);

	return ret;
	
}


static gint
gnome_print_pdf_pages (GnomePrintContext *pc)
{
	GnomePrintPdf *pdf;
	GPPDFPage *page;
	GSList *pagelist, *l;
	gint ret = 0;
	gint col;

	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT(pc), -1);
	pdf = GNOME_PRINT_PDF (pc);
	g_return_val_if_fail (pdf  != NULL, -1);

	pagelist = NULL;
	for (page = pdf->pages; page != NULL; page = page->next) {
		pagelist = g_slist_prepend (pagelist, page);
	}
	for (l = pagelist; l != NULL; l = l->next)	{
		page = (GPPDFPage *) l->data;
		ret += gnome_print_pdf_page (pc, page);
	}

	ret += gnome_print_pdf_object_start (pc, pdf->object_number_pages);
	ret += gnome_print_pdf_write (pc,
				      "/Type /Pages" EOL
				      "/Kids [");

	col = 0;
	for (l = pagelist; l != NULL; l = l->next)	{
		page = (GPPDFPage *) l->data;
		ret += gnome_print_pdf_write (pc,"%i 0 R ", page->object_number_page);
		col++;
		if (col == 10) {
			ret += gnome_print_pdf_write (pc, EOL);
			col = 0;
		}
	}

	ret += gnome_print_pdf_write (pc,
				      "]" EOL
				      "/Count %i" EOL
				      "/MediaBox [%g %g %g %g]" EOL,
				      g_slist_length (pagelist),
				      pdf->bbox.x0, pdf->bbox.y0, pdf->bbox.x1, pdf->bbox.y1);

	ret += gnome_print_pdf_object_end (pc, pdf->object_number_pages, FALSE);
	
	g_slist_free (pagelist);

	return ret;
}

static guint
gnome_print_pdf_catalog (GnomePrintContext *pc)
{
	GnomePrintPdf *pdf;
	gint ret = 0;

	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (pc), -1);
	pdf = GNOME_PRINT_PDF (pc);
	g_return_val_if_fail (pdf != NULL, -1);
	
	pdf->object_number_catalog = gnome_print_pdf_object_number (pc);

	ret += gnome_print_pdf_object_start (pc, pdf->object_number_catalog);
	ret += gnome_print_pdf_write (pc,
				      "/Type /Catalog" EOL
				      "/Pages %i 0 R" EOL,
				      pdf->object_number_pages);
	ret += gnome_print_pdf_object_end (pc, pdf->object_number_catalog, FALSE);
	
	return ret;
}

static gchar*
gnome_print_pdf_get_date (void)
{
	time_t clock;
	struct tm *now;
	gchar *date;

#ifdef ADD_TIMEZONE_STAMP
  extern char * tzname[];
	/* TODO : Add :
		 "[+-]"
		 "HH'" Offset from gmt in hours
		 "OO'" Offset from gmt in minutes
	   we need to use tz_time. but I don't
	   know how protable this is. Chema */
	gprint ("Timezone %s\n", tzname[0]);
	gprint ("Timezone *%s*%s*%li*\n", tzname[1], timezone);
#endif	

	debug (FALSE, "");

	clock = time (NULL);
	now = localtime (&clock);

	date = g_strdup_printf ("D:%04d%02d%02d%02d%02d%02d",
				now->tm_year + 1900,
				now->tm_mon + 1,
				now->tm_mday,
				now->tm_hour,
				now->tm_min,
				now->tm_sec);

	return date;
}
	

static guint
gnome_print_pdf_info (GnomePrintContext *pc)
{
	GnomePrintPdf *pdf;
	gchar *date = NULL;
	gchar *producer = NULL;
	gint ret = 0;

	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (pc), -1);
	pdf = GNOME_PRINT_PDF (pc);
	g_return_val_if_fail (pdf != NULL, -1);

	pdf->object_number_info = gnome_print_pdf_object_number (pc);

	date     = gnome_print_pdf_get_date ();
	producer = g_strdup_printf ("Gnome Print Ver: %s", VERSION);

	ret += gnome_print_pdf_object_start (pc, pdf->object_number_info);
	ret += gnome_print_pdf_write  (pc,
				       "/CreationDate (%s)" EOL
				       "/Producer (%s)" EOL,
				       date,
				       producer);
	ret += gnome_print_pdf_object_end   (pc, pdf->object_number_info, FALSE);

	g_free (producer);
	g_free (date);
	
	return ret;
}
	
static gint
gnome_print_pdf_halftone_default (GnomePrintContext *pc)
{
	guint object_number_halftone;
	gint ret = 0;

	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT(pc), -1);
	
	object_number_halftone = gnome_print_pdf_object_number (pc);

	ret += gnome_print_pdf_object_start (pc, object_number_halftone);
	ret += gnome_print_pdf_write  (pc,
				       "/Type /Halftone" EOL
				       "/HalftoneType 1" EOL
				       "/HalftoneName (Default)" EOL
				       "/Frequency 60" EOL
				       "/Angle 45" EOL
				       "/SpotFunction /Round" EOL);
	ret += gnome_print_pdf_object_end   (pc, object_number_halftone, FALSE);

	return ret;
}

static gint
gnome_print_pdf_default_GS (GnomePrintContext *pc)
{
	gint ret = 0;
	GnomePrintPdf *pdf;

	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (pc), -1);
	pdf = GNOME_PRINT_PDF (pc);
	g_return_val_if_fail (pdf != NULL, -1);

	ret += gnome_print_pdf_object_start (pc, pdf->object_number_gstate);
	ret += gnome_print_pdf_write (pc,
				      "/Type /ExtGState" EOL
				      "/SA false" EOL
				      "/OP false" EOL
				      "/HT /Default" EOL);
	ret += gnome_print_pdf_object_end   (pc, pdf->object_number_gstate, FALSE);
	
	return ret;
}


static gint
gnome_print_pdf_write_content (GnomePrintPdf *pdf, const char *format, ...)
{
	GnomePrintContext *pc;
	GPPDFPage *page;
	va_list arguments;
	gchar *text;
	gint text_length;
	gchar *oldlocale;	

	pc = GNOME_PRINT_CONTEXT (pdf);

	oldlocale = g_strdup (setlocale (LC_NUMERIC, NULL));
	setlocale (LC_NUMERIC, "C");

	va_start (arguments, format);
	text = g_strdup_vprintf (format, arguments);
	va_end (arguments);

	setlocale (LC_NUMERIC, oldlocale);
	g_free (oldlocale);

	page = pdf->pages;

	text_length = strlen (text);

	if (page->stream_used + text_length + 2 > page->stream_allocated ) {
		page->stream_allocated += GNOME_PRINT_PDF_BUFFER_SIZE;
		page->stream = g_realloc (page->stream, page->stream_allocated);
	}

	memcpy (page->stream + page->stream_used,
		text,
		text_length * sizeof (gchar));

	page->stream_used += text_length;
	page->stream [page->stream_used] = 0;

	g_free (text);
	
	return 0;
}

static gint
gnome_print_pdf_graphic_mode_set (GnomePrintPdf *pdf, gint mode)
{
	gint ret = 0;

	debug (FALSE, "");

	if (pdf->graphics_mode == mode)
		return 0;

	switch (mode) {
	case PDF_GRAPHIC_MODE_GRAPHICS:
		if (pdf->graphics_mode == PDF_GRAPHIC_MODE_TEXT)
			ret += gnome_print_pdf_write_content (pdf, "ET" EOL);
		break;
	case PDF_GRAPHIC_MODE_TEXT:
		ret += gnome_print_pdf_write_content (pdf, "BT" EOL);
		break;
	case PDF_GRAPHIC_MODE_UNDEFINED:
		gnome_print_pdf_error (FALSE, "GRAPHIC_MODE undefined\n");
		ret = -1;
		break;
	default:
		gnome_print_pdf_error (FALSE, "mem-problems\n");
		ret = -1;
		g_assert_not_reached ();
	}

	pdf->graphics_mode = mode;

	return ret;
}			

static gint
gnome_print_pdf_graphic_state_set_color (GnomePrintPdf *pdf, gint color_group)
{
	GnomePrintContext *ctx;
	gdouble r, g, b;
	gint ret;

	ctx = GNOME_PRINT_CONTEXT (pdf);

	r = gp_gc_get_red (ctx->gc);
	g = gp_gc_get_green (ctx->gc);
	b = gp_gc_get_blue (ctx->gc);

	if ((pdf->active_color_flag == GP_GC_FLAG_CLEAR) && (r == pdf->r) && (g == pdf->g) && (b == pdf->b)) return GNOME_PRINT_OK;

	/* fixme: It is good idea to split stroke/fill styles, but that should be handled by main ctx/gc */
	ret = gnome_print_pdf_write_content (pdf, "%.3g %.3g %.3g rg" EOL, r, g, b);
	ret = gnome_print_pdf_write_content (pdf, "%.3g %.3g %.3g RG" EOL, r, g, b);

	pdf->r = r;
	pdf->g = g;
	pdf->b = b;
	pdf->active_color_flag = GP_GC_FLAG_CLEAR;

	return ret;
}

static gint
gnome_print_pdf_write_gs (GnomePrintPdf *pdf)
{
	static gint printed = FALSE;
	gint ret = 0;

	debug (FALSE, "");

	if (printed)
		return 0;

	printed = TRUE;

	ret += gnome_print_pdf_write_content (pdf,"/GS1 gs" EOL);

	return ret;
}

#if 0
static GnomePrintPdfGraphicState *
gnome_print_pdf_graphic_state_text_set (GnomePrintPdf *pdf)
{
	GnomePrintPdfGraphicState *gs;
	GnomePrintPdfGraphicState *gs_set;

	debug (FALSE, "");

	g_return_val_if_fail (pdf != NULL, NULL);

	gs     = pdf->graphic_state;
	gs_set = pdf->graphic_state_set;

#ifdef FIX_ME_FIX_ME_FIX_ME
	if (!gs->dirty)
		return 0;
#endif

	gnome_print_pdf_graphic_state_set_color (pdf, PDF_COLOR_GROUP_FILL);

	gnome_print_pdf_write_gs (pdf);

	/* Linecap */
	if (gs->font_character_spacing != gs_set->font_character_spacing) {
		gnome_print_pdf_write_content (pdf, "%g Tc" EOL, gs->font_character_spacing);
		gs_set->font_character_spacing = gs->font_character_spacing;
	}
	
	return gs;
}
#endif

static gint
gnome_print_pdf_graphic_state_set_font (GnomePrintPdf *pdf)
{
	GnomePrintPdfGraphicState *gs;
	GnomePrintPdfGraphicState *gs_set;
	gint ret = 0;

	debug (FALSE, "");
	
	g_return_val_if_fail (GNOME_IS_PRINT_PDF(pdf), -1);

	gs     = pdf->graphic_state;
	gs_set = pdf->graphic_state_set;

	if (gs->pdf_font_number != gs_set->pdf_font_number) {
		ret += gnome_print_pdf_write_content (pdf,
																					"/F%i 1 Tf" EOL,
																					pdf->fonts[gs->pdf_font_number].font_number);
		gs_set->pdf_font_number = gs->pdf_font_number;
	}
	
	return ret;
}


static GnomePrintPdfGraphicState *
gnome_print_pdf_graphic_state_set (GnomePrintPdf *pdf)
{
	GnomePrintContext *ctx;
	GnomePrintPdfGraphicState *gs;
	GnomePrintPdfGraphicState *gs_set;

	ctx = GNOME_PRINT_CONTEXT (pdf);

	gs     = pdf->graphic_state;
	gs_set = pdf->graphic_state_set;

	if (!gs->dirty)
		return 0;

	gnome_print_pdf_graphic_state_set_color (pdf, PDF_COLOR_GROUP_BOTH);
	
	if (gp_gc_get_line_flag (ctx->gc) != GP_GC_FLAG_CLEAR) {
		/* Set current line state */
		gnome_print_pdf_write_content (pdf, "%d J %d j %g w %g M " EOL,
					       gp_gc_get_linecap (ctx->gc),
					       gp_gc_get_linejoin (ctx->gc),
					       gp_gc_get_linewidth (ctx->gc),
					       gp_gc_get_miterlimit (ctx->gc));
		gp_gc_set_line_flag (ctx->gc, GP_GC_FLAG_CLEAR);
	}

	if (gp_gc_get_dash_flag (ctx->gc) != GP_GC_FLAG_CLEAR) {
		/* Set current line dash */
		const ArtVpathDash *dash;
		gint i;
		dash = gp_gc_get_dash (ctx->gc);
		gnome_print_pdf_write_content (pdf, "[");
		for (i = 0; i < dash->n_dash; i++) gnome_print_pdf_write_content (pdf, " %g", dash->dash[i]);
		gnome_print_pdf_write_content (pdf, "] %g d" EOL, dash->n_dash > 0 ? dash->offset : 0.0);
		gp_gc_set_dash_flag (ctx->gc, GP_GC_FLAG_CLEAR);
	}

	if (!gs->written)
		gnome_print_pdf_write_gs (pdf);
	
	if (!gs->written)
		gnome_print_pdf_write_content (pdf, "1 i " EOL);
	
	gs->written = TRUE;

	return gs;
}

static gint
gnome_print_pdf_path_print (GnomePrintPdf *pdf, const ArtBpath *path)
{
	for (; path->code != ART_END; path++)
		switch (path->code) {
		case ART_MOVETO_OPEN:
			gnome_print_pdf_write_content (pdf, "%g %g m" EOL, path->x3, path->y3);
			break;
		case ART_MOVETO:
			gnome_print_pdf_write_content (pdf, "%g %g m" EOL, path->x3, path->y3);
			break;
		case ART_LINETO:
			gnome_print_pdf_write_content (pdf, "%g %g l" EOL, path->x3, path->y3);
			break;
		case ART_CURVETO:
			gnome_print_pdf_write_content (pdf, "%g %g %g %g %g %g c" EOL,
						       path->x1, path->y1,
						       path->x2, path->y2,
						       path->x3, path->y3);
			break;
		default:
			gnome_print_pdf_error (TRUE, "the path contains an unknown type point");
			return -1;
		}
	
	return 0;
}

#if 0
static gint
gnome_print_pdf_dump_gmode (GnomePrintPdf *pdf)
{
	gint ret = 0;

	debug (FALSE, "");

	switch (pdf->graphics_mode){
	case PDF_GRAPHIC_MODE_GRAPHICS:
		gprint ("Graphics\n");
		ret += gnome_print_pdf_write_content (pdf,"Graphics" EOL);
		break;
	case PDF_GRAPHIC_MODE_TEXT:
		gprint ("Text\n");
		ret += gnome_print_pdf_write_content (pdf,"Text" EOL);
		break;
	case PDF_GRAPHIC_MODE_UNDEFINED:
		gprint ("Undefined\n");
		ret += gnome_print_pdf_write_content (pdf,"Undefined" EOL);
		break;
	default:
		gprint ("Other\n");
		ret += gnome_print_pdf_write_content (pdf,"Other" EOL);
		break;
	}
	return ret;
}
#endif

static GnomePrintPdfGraphicState *
gnome_print_pdf_graphic_state_duplicate (GnomePrintPdfGraphicState *gs_in)
{
	GnomePrintPdfGraphicState *gs_out;

	g_return_val_if_fail (gs_in != NULL, NULL);
	
	debug (FALSE, "");
	
	gs_out = g_new (GnomePrintPdfGraphicState, 1);
	
	memcpy (gs_out,
		gs_in,
		sizeof (GnomePrintPdfGraphicState));

#if 0
	gs_out->current_path = gp_path_duplicate (gs_in->current_path);
#endif

	return gs_out;
}

#if 0
static gdouble
gp_zero_clean (gdouble cleanme)
{
	if (abs(cleanme) > 0.00000001) {
		return cleanme;
	}
	return 0.0;
}
#endif

#if 0
static gboolean
gnome_print_pdf_add_glyph_to_font (GnomePrintPdfFont *pdf_font, const gchar *glyph_name)
{
	gint n;
	
	g_return_val_if_fail (pdf_font != NULL, FALSE);
	g_return_val_if_fail (GNOME_IS_FONT (pdf_font->gnome_font), FALSE);
	g_return_val_if_fail (glyph_name != NULL, FALSE);

	for (n = 0; n < pdf_font->glyphs_num; n++) {
		if (!strcmp (glyph_name, pdf_font->glyphs[n].name))
			break;
	}

	if (n == pdf_font->glyphs_num) {
		if (pdf_font->glyphs_num == pdf_font->glyphs_max)
			pdf_font->glyphs = g_realloc (pdf_font->glyphs, sizeof (T1Glyph) *
						      (pdf_font->glyphs_max +=
						       GNOME_PRINT_NUMBER_OF_ELEMENTS_GROW));
		pdf_font->glyphs[pdf_font->glyphs_num++].name = g_strdup (glyph_name);
	}
	
	return TRUE;
}
#endif

static gboolean
gnome_print_pdf_font_insert (GnomePrintPdf *pdf,
			     GnomeFont *gnome_font,
			     const gchar *font_name,
			     gboolean is_basic_14)
{
	GnomePrintPdfFont *font;

	if (pdf->fonts_number == pdf->fonts_max)
		pdf->fonts = g_realloc (pdf->fonts, sizeof (GnomePrintPdfFont) *
					(pdf->fonts_max += GNOME_PRINT_NUMBER_OF_ELEMENTS_GROW));

	font = &pdf->fonts[pdf->fonts_number++];
	font->font_number   = pdf->fonts_number;
	font->gnome_font    = gnome_font;
	font->font_name     = g_strdup (font_name);
	font->object_number = 0;
	font->object_number_descriptor = 5890990;
	font->object_number_pfb        = 2931915;
	font->is_basic_14   = is_basic_14;

	font->glyphs_max    = GNOME_PRINT_NUMBER_OF_ELEMENTS;
	font->glyphs_num    = 0;
	font->glyphs        = g_new (T1Glyph, font->glyphs_max);

	gtk_object_ref (GTK_OBJECT(gnome_font));
	
	return TRUE;
}

static gint
gnome_print_pdf_get_font_number (GnomePrintContext *pc,
				 GnomeFont *gnome_font,
				 gboolean is_basic_14)
{
	GnomePrintPdf *pdf;
	gint n;
	const gchar *font_name;

	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (pc), -1);
	pdf = GNOME_PRINT_PDF (pc);
	g_return_val_if_fail (pdf != NULL, -1);
	g_return_val_if_fail (GNOME_IS_FONT (gnome_font), -1);

	font_name = gnome_font_get_ps_name (gnome_font);

	for (n = 0; n < pdf->fonts_number; n++)
		if (!strcmp (font_name, pdf->fonts[n].font_name))
			break;

	if (n != pdf->fonts_number)
		return n;

	gnome_print_pdf_font_insert (pdf, gnome_font, font_name, is_basic_14);

	return pdf->fonts_number-1;
}

/* fixme: This is strictly for glyphlists (Lauris) */
static gint
gnome_print_pdf_set_color_private (GnomePrintPdf *pdf, gdouble r, gdouble g, gdouble b)
{
	gint ret;

	ret = gnome_print_pdf_write_content (pdf, "%.3g %.3g %.3g rg" EOL, r, g, b);

	pdf->active_color_flag = GP_GC_FLAG_UNSET;

	return GNOME_PRINT_OK;
}

static gint
gnome_print_pdf_set_font_private (GnomePrintPdf *pdf, GnomeFont *font)
{
	GnomePrintPdfGraphicState *gs;
	const char *fontname;
	gint n;
	gint is_basic_14;

	fontname = gnome_font_get_ps_name (font);

	for (n = 0; n < pdf->fonts_internal_number; n++)
		if (!strcmp (fontname, pdf->fonts_internal[n].font_name))
			break;

	if (n == pdf->fonts_internal_number) {
		is_basic_14 = FALSE;
	}	else {
		is_basic_14 = pdf->fonts_internal[n].is_basic_14;
	}

	gs = pdf->graphic_state;
	gs->font_size = gnome_font_get_size (font);
	gs->pdf_font_number = gnome_print_pdf_get_font_number ((GnomePrintContext *) pdf, font, is_basic_14);

	gnome_print_pdf_graphic_state_set_font (pdf);

	return 0;
}

static gint
gnome_print_pdf_image_load (GnomePrintPdf *pdf, const guchar *data, gint w, gint h, gint rowstride,
			    gint bytes_per_pixel, gint image_type,
			    gint data_length, PdfCompressionType compr_type)
{
	GPPDFImage *image;
	GPPDFPage *page;

	page = pdf->pages;
	
	image = g_new (GPPDFImage, 1);

	image->next = page->images;
	page->images = image;

	image->data = g_malloc (data_length + 1);
	memcpy (image->data, data, data_length);
	image->data_length = data_length;
	image->width = w;
	image->height = h;
	image->rowstride = rowstride;
	image->bytes_per_pixel = bytes_per_pixel;
	image->object_number = gnome_print_pdf_object_number (GNOME_PRINT_CONTEXT (pdf));
	image->image_type = image_type;
	image->compr_type = compr_type;
	image->image_number = (image->next) ? image->next->image_number + 1 : 1;

	return image->image_number;
}

/* fixme: Image type is The Right Way, but it will be encoded in main API (Lauris) */

static gint
gnome_print_pdf_image_compressed (GnomePrintContext *ctx, const gdouble *ctm, const guchar *px, gint w, gint h, gint rowstride, gint ch)
{
	PdfCompressionType compr_type;
	guchar *value;
	GnomePrintPdfGraphicState *gs;
	GnomePrintPdf *pdf;
	gchar *image_stream = NULL;
	gint ret = 0;
	gint image_number;
	gint image_stream_size;

	pdf = GNOME_PRINT_PDF (ctx);
	
	/* Read preferred compression type */
	if (ch == 1) {
		value = gpa_node_get_path_value (ctx->config, "Settings.Output.Imaging.GrayscaleImageCompression");
	} else {
		value = gpa_node_get_path_value (ctx->config, "Settings.Output.Imaging.ColorImageCompression");
	}
	if (value) {
		compr_type = gnome_print_pdf_compr_from_string (value);
		g_free (value);
	} else {
		compr_type = PDF_COMPRESSION_HEX;
	}
	
	gs = pdf->graphic_state;

	gnome_print_pdf_graphic_mode_set (pdf, PDF_GRAPHIC_MODE_GRAPHICS);
	
	ret = gnome_print_pdf_write_content (pdf, "%g %g %g %g %g %g cm" EOL, ctm[0], ctm[1], ctm[2], ctm[3], ctm[4], ctm[5]);
	g_return_val_if_fail (ret >= 0, ret);
	ret = gnome_print_pdf_write_content (pdf, "0 0 m" EOL);
	g_return_val_if_fail (ret >= 0, ret);

	/* 1. Get the required size (after compression) */
	switch (compr_type) {
	case PDF_COMPRESSION_FLATE:
		image_stream_size = gnome_print_encode_deflate_wcs (w * h * ch);
		break;
	case PDF_COMPRESSION_HEX:
		image_stream_size = gnome_print_encode_hex_wcs (w * h * ch);
		break;
	case PDF_COMPRESSION_NONE:
		image_stream_size = w * h * ch;
		break;
	default:
		g_warning ("file %s: line %d: Illegal compression method [%d]", __FILE__, __LINE__, compr_type);
		return GNOME_PRINT_ERROR_UNKNOWN;
		break;
	}

	/* fixme: This seems suspicious (rowstride!!!) (Lauris) */
	if (compr_type != PDF_COMPRESSION_NONE) {
		/* 2. Allocate the buffer */
		image_stream = g_new (gchar, image_stream_size);
		/* 3. Compress the image */
		switch (compr_type) {
		case PDF_COMPRESSION_FLATE:
			image_stream_size = gnome_print_encode_deflate (px, image_stream,
									w * h * ch,
									image_stream_size);
			break;
		case PDF_COMPRESSION_HEX:
			image_stream_size = gnome_print_encode_hex (px, image_stream,
								    w * h * ch);
			break;
		case PDF_COMPRESSION_NONE:
		default:
			g_assert_not_reached ();
			break;
		}
	} else {
		image_stream = (gchar *) px;
	}

	/* 4. Load it */
	image_number = gnome_print_pdf_image_load (pdf, image_stream,
						   w, h, rowstride, ch,
						   (ch == 1) ? PDF_IMAGE_GRAYSCALE : PDF_IMAGE_RGB, image_stream_size,
						   compr_type);

	if (compr_type != PDF_COMPRESSION_NONE) {
		/* fixme: This seems The Right Thing To Do (Lauris) */
		g_free (image_stream);
	}

	ret = gnome_print_pdf_write_content (pdf, "/Im%i Do" EOL, image_number);
	
	return ret;
}

static gint
gnome_print_pdf_page_free (GPPDFPage *page)
{
	debug (FALSE, "");

	g_return_val_if_fail (page != NULL, -1);
		
	while (page->images) {
		GPPDFImage *image;
		image = page->images;
		page->images = image->next;
		if (image->data) g_free (image->data);
		g_free (image);
	}
		
	if (page->stream) g_free (page->stream);
	g_free (page->page_name);
	g_free (page);

	return 0;
}

static gboolean
gnome_print_pdf_free_fonts (GnomePrintPdf *pdf)
{
	GnomePrintPdfFont *font;
	gint n,i;
	debug (FALSE, "");

	g_return_val_if_fail (pdf != NULL, FALSE);
	
	for (n = 0; n < pdf->fonts_number; n++) {
		font = (GnomePrintPdfFont *) &pdf->fonts[n];
		g_free (font->font_name);
		for (i = 0; i < font->glyphs_num; i++) {
			gchar *name;
			name = font->glyphs[i].name;
			g_free (name);
		}
		g_free (font->glyphs);
		gtk_object_unref (GTK_OBJECT (font->gnome_font));
	}

	return TRUE;
}
					 

static gboolean
gnome_print_pdf_free_objects (GnomePrintPdf *pdf)
{
	GList *list;
	
	debug (FALSE, "");

	g_return_val_if_fail (pdf != NULL, FALSE);
		
	for (list = pdf->objects; list != NULL; list = list->next) {
		GnomePrintPdfObject *object;
		object = (GnomePrintPdfObject *) list->data;
		g_free (object);
	}

	return TRUE;
}

gint
gnome_print_pdf_add_bytes_written (GnomePrintPdf *pdf, gint bytes)
{
	g_return_val_if_fail (GNOME_IS_PRINT_PDF (pdf), -1);
	
	pdf->offset += bytes;

	return 0;
}

#if 0
static gboolean
gnome_print_dash_init (GnomePrintDash *dash)
{
	debug (FALSE, "");

	dash->number_values = 0;
	dash->phase = 0;
	dash->values = 0;
	
	return TRUE;
}
#endif

#if 0
static gboolean
gnome_print_dash_compare (GnomePrintDash *dash1, GnomePrintDash *dash2)
{
	gint n;
	
	if (dash1->number_values != dash2->number_values)
		return FALSE;
	
	if (dash1->phase != dash2->phase)
		return FALSE;

	for (n=0; n < dash1->number_values; n++)
		if (dash1->values[n] != dash2->values[n])
			return FALSE;

	return TRUE;
}

static void
gnome_print_dash_copy (GnomePrintDash *dash_from, GnomePrintDash *dash_to)
{
	dash_to->number_values = dash_from->number_values;
	dash_to->phase         = dash_from->phase;

	if (dash_to->values == NULL)
		dash_to->values = g_new (gdouble, dash_from->number_values);
	
if(FALSE)	memcpy (dash_to->values,
					dash_from->values,
					dash_to->number_values * sizeof (gdouble));
}
#endif

#if 0
static gint
gnome_print_pdf_setdash (GnomePrintContext *pc, gint number_values, const double *values, double offset)
{
	GnomePrintPdf *pdf;
	GnomePrintDash *dash;

	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT(pc), -1);
	pdf = GNOME_PRINT_PDF (pc);
	g_return_val_if_fail (pdf != NULL, -1);

	dash = &pdf->graphic_state->dash;

	g_free (dash->values);

	dash->phase = offset;
	dash->values = g_new (gdouble, number_values);
	dash->number_values = number_values;

	memcpy (dash->values, values,
					number_values * sizeof (gdouble));

	return 0;
}
#endif

#if 0
static gint
gnome_print_pdf_strokepath (GnomePrintContext *pc)
{
  static gboolean warned = FALSE;

	debug (FALSE, "");

	if (warned)
		return 0;
	
	g_warning ("Strokepath is not supported. It migth get deprecated in the future\n");
	warned = TRUE;
	
  return 0;
}
#endif

#if 0
static gint
gnome_print_pdf_moveto (GnomePrintContext *pc, double x, double y)
{
	GnomePrintPdf *pdf;
	GnomePrintPdfGraphicState *gs;
	ArtPoint point;
	
	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (pc), -1);
	pdf = GNOME_PRINT_PDF (pc);
	g_return_val_if_fail (pdf != NULL, -1);

	gs = gnome_print_pdf_graphic_state_current (pdf, FALSE);

	point.x = x;
	point.y = y;
	art_affine_point (&point, &point, gs->ctm);
	gp_path_moveto (gs->current_path, point.x, point.y);

	return 0;
}
#endif

#if 0
static gint
gnome_print_pdf_lineto (GnomePrintContext *pc, double x, double y)
{
	GnomePrintPdf *pdf;
	GnomePrintPdfGraphicState *gs;
	ArtPoint point;
	
	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (pc), -1);
	pdf = GNOME_PRINT_PDF (pc);
	g_return_val_if_fail (pdf != NULL, -1);

	gs = gnome_print_pdf_graphic_state_current (pdf, FALSE);

	point.x = x;
	point.y = y;
	art_affine_point (&point, &point, gs->ctm);

	gp_path_lineto (gs->current_path, point.x, point.y);
	
	return 0;
}

static gint
gnome_print_pdf_curveto (GnomePrintContext *pc,
			 double x0, double y0,
			 double x1, double y1,
			 double x2, double y2)
{
	GnomePrintPdf *pdf;
	GnomePrintPdfGraphicState *gs;
	ArtPoint point_0;
	ArtPoint point_1;
	ArtPoint point_2;

	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (pc), -1);
	pdf = GNOME_PRINT_PDF (pc);
	g_return_val_if_fail (pdf != NULL, -1);

	gs = gnome_print_pdf_graphic_state_current (pdf, FALSE);

	/* IS there a libart function for this ??? Chema */
	point_0.x = x0;
	point_0.y = y0;
	point_1.x = x1;
	point_1.y = y1;
	point_2.x = x2;
	point_2.y = y2;

	art_affine_point (&point_0, &point_0, gs->ctm);
	art_affine_point (&point_1, &point_1, gs->ctm);
	art_affine_point (&point_2, &point_2, gs->ctm);

	gp_path_curveto (gs->current_path,
			 point_0.x, point_0.y,
			 point_1.x, point_1.y,
			 point_2.x, point_2.y); 
	
	return 0;	
}
#endif

#if 0
static gint
gnome_print_pdf_newpath (GnomePrintContext *pc)
{
	GnomePrintPdf *pdf;
	GnomePrintPdfGraphicState *gs;

	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT (pc), -1);
	pdf = GNOME_PRINT_PDF (pc);
	g_return_val_if_fail (pdf != NULL, -1);

	gs = gnome_print_pdf_graphic_state_current (pdf, FALSE);
	
	if (gp_path_length (gs->current_path) > 1) 
		g_warning ("Path was disposed without using it [newpath]\n");
	
	gp_path_reset (gs->current_path);

  return 0;
}
#endif

#if 0
static gint
gnome_print_pdf_closepath (GnomePrintContext *pc)
{
	GnomePrintPdf *pdf;

	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT(pc), -1);
	pdf = GNOME_PRINT_PDF (pc);
	g_return_val_if_fail (pdf != NULL, -1);

	gp_path_closepath (pdf->graphic_state->current_path);

  return 0;
}
#endif

#if 0
static gint
gnome_print_pdf_setrgbcolor (GnomePrintContext *pc,
			     double r, double g, double b)
{
	GnomePrintPdf *pdf;
	GnomePrintPdfGraphicState *gs;

	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT(pc), -1);
	pdf = GNOME_PRINT_PDF (pc);
	g_return_val_if_fail (pdf != NULL, -1);

	
	gs = gnome_print_pdf_graphic_state_current (pdf, TRUE);

	gs->color_fill_mode   = PDF_COLOR_MODE_DEVICERGB;
	gs->color_fill [0] = r;
	gs->color_fill [1] = g;
	gs->color_fill [2] = b;
	
	gs->color_stroke_mode = PDF_COLOR_MODE_DEVICERGB;
	gs->color_stroke [0] = r;
	gs->color_stroke [1] = g;
	gs->color_stroke [2] = b;

	return 0;
}
#endif

#if 0
static gint
gnome_print_pdf_graphic_state_set_color (GnomePrintPdf *pdf, gint color_group)
{
	GnomePrintPdfGraphicState *gs;
	GnomePrintPdfGraphicState *gs_set;

	debug (FALSE, "");

	g_return_val_if_fail (pdf != NULL, -1);

	gs     = pdf->graphic_state;
	gs_set = pdf->graphic_state_set;

	if ((color_group == PDF_COLOR_GROUP_STROKE ||
	     color_group == PDF_COLOR_GROUP_BOTH) &&
	    (gs->color_stroke_mode != gs_set->color_stroke_mode ||
	     gs->color_stroke [0] != gs_set->color_stroke [0]||
	     gs->color_stroke [1] != gs_set->color_stroke [1]||
	     gs->color_stroke [2] != gs_set->color_stroke [2]||
	     gs->color_stroke [3] != gs_set->color_stroke [3])) {
		switch (gs->color_stroke_mode){
		case PDF_COLOR_MODE_DEVICEGRAY:
			gnome_print_pdf_write_content (pdf, "%.3g G" EOL,
						       gs->color_stroke[0]);
			break;
		case PDF_COLOR_MODE_DEVICERGB:
			gnome_print_pdf_write_content (pdf, "%.3g %.3g %.3g RG" EOL,
						       gs->color_stroke[0],
						       gs->color_stroke[1],
						       gs->color_stroke[2]);
			break;
		case PDF_COLOR_MODE_DEVICECMYK:
			gnome_print_pdf_write_content (pdf, "%.3g %.3g %.3g %.3g K" EOL,
						       gs->color_stroke[0],
						       gs->color_stroke[1],
						       gs->color_stroke[2],
						       gs->color_stroke[3]);
			break;
		}
		gs_set->color_stroke_mode = gs->color_stroke_mode;
		gs_set->color_stroke[0]   = gs->color_stroke[0];
		gs_set->color_stroke[1]   = gs->color_stroke[1];
		gs_set->color_stroke[2]   = gs->color_stroke[2];
		gs_set->color_stroke[3]   = gs->color_stroke[3];
	}

	if ((color_group == PDF_COLOR_GROUP_FILL ||
	     color_group == PDF_COLOR_GROUP_BOTH) &&
	    (gs->color_fill_mode != gs_set->color_fill_mode ||
	     gs->color_fill [0] != gs_set->color_fill [0]||
	     gs->color_fill [1] != gs_set->color_fill [1]||
	     gs->color_fill [2] != gs_set->color_fill [2]||
	     gs->color_fill [3] != gs_set->color_fill [3])) {
		switch (gs->color_fill_mode){
		case PDF_COLOR_MODE_DEVICEGRAY:
			gnome_print_pdf_write_content (pdf, "%.3g g" EOL,
						       gs->color_fill[0]);
			break;
		case PDF_COLOR_MODE_DEVICERGB:
			gnome_print_pdf_write_content (pdf, "%.3g %.3g %.3g rg" EOL,
						       gs->color_fill[0],
						       gs->color_fill[1],
						       gs->color_fill[2]);
			break;
		case PDF_COLOR_MODE_DEVICECMYK:
			gnome_print_pdf_write_content (pdf, "%.3g %.3g %.3g %.3g K" EOL,
						       gs->color_fill[0],
						       gs->color_fill[1],
						       gs->color_fill[2],
						       gs->color_fill[3]);
			break;
		}
		gs_set->color_fill_mode  = gs->color_fill_mode;
		gs_set->color_fill [0]  = gs->color_fill [0];
		gs_set->color_fill [1]  = gs->color_fill [1];
		gs_set->color_fill [2]  = gs->color_fill [2];
		gs_set->color_fill [3]  = gs->color_fill [3];
	}

	return 0;
}
#endif

#if 0
static gint
gnome_print_pdf_ctm_is_identity (double matrix [6])
{
	double identity [6];

	art_affine_identity (identity);

	if ( (identity [0] != matrix [0]) ||
			 (identity [1] != matrix [1]) ||
			 (identity [2] != matrix [2]) ||
			 (identity [3] != matrix [3]) ||
			 (identity [4] != matrix [4]) ||
			 (identity [5] != matrix [5]))
		return FALSE;
	
	return TRUE;
}
#endif

#if 0
static gint
gnome_print_pdf_concat (GnomePrintContext *pc, const double matrix[6])
{
	GnomePrintPdf * pdf;
	GnomePrintPdfGraphicState *gs;

	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_PRINT_CONTEXT(pc), -1);
	pdf = GNOME_PRINT_PDF (pc);
	g_return_val_if_fail (pdf != NULL, -1);

	gs = gnome_print_pdf_graphic_state_current (pdf, FALSE);

	art_affine_multiply (gs->ctm, matrix, gs->ctm);

	return 0;
}
#endif

#if 0
static gint
gnome_print_pdf_setopacity (GnomePrintContext *pc, double opacity)
{
  static gboolean warned = FALSE;

	debug (FALSE, "");

	if (!warned) {
    g_warning ("Unimplemented setopacity");
    warned = TRUE;
  }
  return 0;
}
#endif

#if 0
/*
#define METHOD_ONE
*/

static gint
gnome_print_pdf_show_sized (GnomePrintContext *pc, const char *text, int bytes)
{
	GnomePrintPdfGraphicState *gs;
	GnomePrintPdfFont *pdf_font;
	GnomePrintPdf *pdf;
	const ArtPoint *cp;
	gint ret = 0;
	double val_1, val_2, val_3, val_4;
	const char *p;
	const GnomeFontFace * face;
	const gdouble *ctm;

	pdf = GNOME_PRINT_PDF (pc);

	gs = pdf->graphic_state;
	
	if (!gp_gc_has_currentpoint (pc->gc)) {
		gnome_print_pdf_error (FALSE, "show, currentpoint not defined.");
		return -1;
	}

	if (gs->pdf_font_number == GNOME_PRINT_PDF_FONT_UNDEFINED ||
			gs->font_size == 0) {
		gnome_print_pdf_error (FALSE, "show, fontname or fontsize not defined.");
		return -1;
	}

	ctm = gp_gc_get_ctm (pc->gc);
	val_1 = gp_zero_clean (ctm[0]);
	val_2 = gp_zero_clean (ctm[1]);
	val_3 = gp_zero_clean (ctm[2]);
	val_4 = gp_zero_clean (ctm[3]);

	cp = gp_gc_get_currentpoint (pc->gc);

	ret += gnome_print_pdf_graphic_mode_set (pdf, PDF_GRAPHIC_MODE_TEXT);

	ret += gnome_print_pdf_graphic_state_set_font (pdf);

	pdf_font = &pdf->fonts[gs->pdf_font_number];
	g_return_val_if_fail (GNOME_IS_FONT (pdf_font->gnome_font), -1);
	face = gnome_font_get_face (pdf_font->gnome_font);
	g_return_val_if_fail (GNOME_IS_FONT_FACE (face), -1);

	ret += gnome_print_pdf_write_content (pdf,
					      "%f %f %f %f %f %f Tm" EOL,
					      val_1,
					      val_2,
					      val_3,
					      val_4,
					      cp->x,
					      cp->y);
	gnome_print_pdf_graphic_state_text_set (pdf);

	/* I don't like the fact that we are writing one letter at time */
  if (gnome_print_pdf_write_content (pdf, "(") < 0)
    return -1;

#ifdef METHOD_ONE
  if (gnome_print_pdf_write_content (pdf, "\\376\\377") < 0)
    return -1;
#endif
	
  for (p = text; p && p < (text + bytes); p = g_utf8_next_char (p))
	{
		const gchar *glyph_name;
		gunichar u;
		gint g, glyph, page;

    u = g_utf8_get_char (p);
		g = gnome_font_face_lookup_default (face, u);
		glyph_name = gnome_font_face_get_glyph_ps_name (face, g);
		gnome_print_pdf_add_glyph_to_font (pdf_font, glyph_name);
		
		glyph = g & 0xff;
		page = (g >> 8) & 0xff;

#ifdef METHOD_ONE
		if (gnome_print_pdf_write_content (pdf,"\\%03o\\%03o", page, glyph) < 0)
			return -1;
#else
		if (page != 0) {
			static gboolean warned = FALSE;
			if (!warned)
				g_warning ("The Gnome PDF writer can't print characters outside "
									 "the main page, (page 0).");
			warned = TRUE;
		}
		if (gnome_print_pdf_write_content (pdf,"\\%03o", glyph) < 0)
			return -1;
#endif	
	}

	gnome_print_pdf_write_content (pdf, ")Tj" EOL);

  return ret;
}
#endif




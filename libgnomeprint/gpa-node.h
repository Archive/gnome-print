#ifndef __GPA_NODE_H__
#define __GPA_NODE_H__

/*
 * GPANode
 *
 * Opaque handle to gnome-print configuration tree
 *
 * Authors:
 *   Lauris Kaplinski <lauris@ximian.com>
 *
 * Copyright (C) 2001 Ximian, Inc.
 *
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#define GPA_NODE(n) gpa_node_check_cast (n)
#define GPA_IS_NODE(n) gpa_node_check_type (n)

#include <gtk/gtktypeutils.h>

typedef struct _GPANode GPANode;

void gpa_node_ref (GPANode *node);
void gpa_node_unref (GPANode *node);

/* Gtk+ replacement to simplify migration */
GPANode *gpa_node_check_cast (gpointer node);
gboolean gpa_node_check_type (gpointer node);

guchar *gpa_node_id (GPANode *node);

/* These return referenced node or NULL */
GPANode *gpa_node_get_parent (GPANode *node);
GPANode *gpa_node_get_child (GPANode *node, GPANode *ref);
GPANode *gpa_node_get_path_node (GPANode *node, const guchar *path);

/* Basic value manipulation */
guchar *gpa_node_get_value (GPANode *node);
gboolean gpa_node_set_value (GPANode *node, const guchar *value);
guchar *gpa_node_get_path_value (GPANode *node, const guchar *path);
gboolean gpa_node_set_path_value (GPANode *node, const guchar *path, const guchar *value);

/* Convenience stuff */
gboolean gpa_node_get_bool_path_value (GPANode *node, const guchar *path, gint *value);
gboolean gpa_node_get_int_path_value (GPANode *node, const guchar *path, gint *value);
gboolean gpa_node_get_double_path_value (GPANode *node, const guchar *path, gdouble *value);
gboolean gpa_node_set_bool_path_value (GPANode *node, const guchar *path, gint value);
gboolean gpa_node_set_int_path_value (GPANode *node, const guchar *path, gint value);
gboolean gpa_node_set_double_path_value (GPANode *node, const guchar *path, gdouble value);

/* Gives you default Settings node */

GPANode *gpa_defaults (void);

END_GNOME_DECLS

#endif


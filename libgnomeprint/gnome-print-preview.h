#ifndef __GNOME_PRINT_PREVIEW_H__
#define __GNOME_PRINT_PREVIEW_H__

/*
 *  Copyright (C) 1999-2001 Ximian Inc. and authors
 *
 *  Authors:
 *    Miguel de Icaza (miguel@gnu.org)
 *    Lauris Kaplinski <lauris@ximian.com>
 *
 *  Preview driver
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

/*
 * GnomePrintPreview printer context.
 */
#define GNOME_TYPE_PRINT_PREVIEW	 (gnome_print_preview_get_type ())
#define GNOME_PRINT_PREVIEW(obj)	 (GTK_CHECK_CAST ((obj), GNOME_TYPE_PRINT_PREVIEW, GnomePrintPreview))
#define GNOME_PRINT_PREVIEW_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_PRINT_PREVIEW, GnomePrintPreviewClass))
#define GNOME_IS_PRINT_PREVIEW(obj)	    (GTK_CHECK_TYPE ((obj), GNOME_TYPE_PRINT_PREVIEW))
#define GNOME_IS_PRINT_PREVIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_PRINT_PREVIEW))

typedef struct _GnomePrintPreview        GnomePrintPreview;
typedef struct _GnomePrintPreviewClass   GnomePrintPreviewClass;

#include <libgnome/gnome-paper.h>
#include <libgnomeui/gnome-canvas.h>
#include <libgnomeprint/gnome-print.h>

GtkType            gnome_print_preview_get_type  (void);
GnomePrintContext *gnome_print_preview_new       (GnomeCanvas *canvas,
						  const char *paper_size);
void               gnome_print_preview_construct (GnomePrintPreview *preview,
						  GnomeCanvas *canvas,
						  const GnomePaper *paper_info);

END_GNOME_DECLS

#endif /* __GNOME_PRINT_PREVIEW_H__ */


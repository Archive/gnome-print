#!/usr/bin/perl

# A script to automatically generate Gtk code for the PostScript print driver

# Return a variable name with the type stripped off
sub striptype {
    my $var = shift;

    $var =~ s/\[.*\]$//;
    $var =~ s/^.*\s\**//;
    return $var;
}

while (<>) {
    if (/^printer->(\w+)\s*\((.*)\)/) {
	push @methods, $1;
	$args{$1} = $2;
    }
}

print "/* The method defs, autogenned */
";
print "struct _GnomePrintContextClass
{
  GtkObjectClass parent_class;

";
foreach $method (@methods) {
$args = $args{$method};
if ($args ne '') { $args = ', '.$args; }
print "  int (* ".$method.") ".(' ' x (16 - length $method))."(GnomePrintContext *pc".$args.");

";
}
print "  int (* close)            (GnomePrintContext *pc);
};


";

print "/* The declarations of the virtual paint methods, autogenned */
";
foreach $method (@methods) {
$args = $args{$method};
if ($args ne '') { $args = ', '.$args; }
print "int
gnome_print_".$method." ".(' ' x (16 - length $method))."(GnomePrintContext *pc".$args.");

";
}

print "\n\n";

print "/* initialization code, autogenned */
";
foreach $method (@methods) {
$args = $args{$method};
if ($args ne '') { $args = ', '.$args; }
print "  class->".$method." = NULL;
";
}

print "\n\n";

print "/* The implementations of the virtual paint methods, autogenned */
";
foreach $method (@methods) {
$args = $args{$method};
$stripargs = join (', ', map { striptype ($_) } split (/,\s+/, $args));
if ($args ne '') { $args = ', '.$args; $stripargs = ', '.$stripargs; }
print "int
gnome_print_".$method." ".(' ' x (16 - length $method))."(GnomePrintContext *pc".$args.")
{
  return PRINT_CLASS(pc)->".$method. " (pc".$stripargs.");
}

";
}

print "\n\n";

print "/* The implementations of the PostScript paint methods, autogenned */

";
foreach $method (@methods) {
$args = $args{$method};
@args = split (/,\s+/, $args);
$stripargs = join (', ', map { striptype ($_) } @args);
if ($args ne '') { $args = ', '.$args; $stripargs = ', '.$stripargs; }
print "int
gnome_print_ps_".$method." (GnomePrintContext *pc".$args.")
{
  return gnome_print_context_fprintf (pc,
                                      \"";
foreach $arg (@args) {
if ($arg =~ /^(\w+)\s+(.*)\[(\d+)\]$/) {
$var = $2;
$last = $3 - 1;
print "[ ";
for $i (0..$last) {
print "%g ";
}
print "] ";
} elsif ($arg =~ /^(\w+)\s+(.*)$/) {
if ($1 eq 'double') { print "%g "; }
elsif ($1 eq 'int') { print "%d "; }
else { print "<$1> "; }
}

}
print $method."\\n\"";
foreach $arg (@args) {
if ($arg =~ /^\w+\s+(.*)\[(\d+)\]$/) {
$var = $1;
$last = $2 - 1;
for $i (0..$last) {
print ",
                                      ".$1."[".$i."]";
}
} else {
print ",
                                      ".striptype ($arg);
}
}
print ");
}

";

}

print "\n\n";

print "  /* initialization code, autogenned */
";
foreach $method (@methods) {
$args = $args{$method};
if ($args ne '') { $args = ', '.$args; }
print "  pc_class->".$method." = gnome_print_ps_".$method.";
";
}


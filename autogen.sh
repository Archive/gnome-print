#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="Gnome Print"

(test -f $srcdir/configure.in \
  && test -f $srcdir/libgnomeprint/gnome-print.h) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level gnome-print directory"
    exit 1
}

. $srcdir/macros/autogen.sh
